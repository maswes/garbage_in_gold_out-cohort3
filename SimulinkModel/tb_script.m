% Couldn't get the Simulink model to run

clear all;

test_msg = unicode2native('Just right!!'); % Must be of length 12
%test_msg = repmat(uint8(233), 1, MyConstants.RS_K-6);
% Pad message to Reed-Solomon message size
msgLength = length(test_msg);
maxPayload = MyConstants.RS_K - 6; % 3 size, 1 id, 2 crc
if msgLength < maxPayload
    pad_msg = [test_msg repmat(uint8(1), 1, maxPayload - msgLength)];
else
    pad_msg = test_msg;
end

% Can we pass in the message as an array?
tx = qpsk_tb_tx(pad_msg);

% 
[numRecBytes, rx, rssi] = qpsk_tb_rx(tx)

if MyConstants.ADD_ERRORS
    err_msg = test_msg;
    err_msg(6) = 1;
    err_msg(7) = 1;
    fprintf('Changed "%s" to "%s" with errors\n', test_msg, err_msg);
end

fprintf('Received %d bytes as "%s" with rssi %d\n', numRecBytes, rx, rssi)
assert(isequal(rx, test_msg))
