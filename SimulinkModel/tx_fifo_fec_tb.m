clear all;

% Need to push enough to the FIFO to complete encoding
%test_msg = unicode2native('abcdabcdabcdabcd');
%test_msg = repmat(uint8('a'), 1, MyConstants.RS_K);
test_msg =  ... 
    [41    0    0    1   74  117  115  116   32  114  105  103  104 ...
     116   33   33    0    0    0    0    0    0    0    0 0    0 ...
     0    0    0    0    0    0    0    0    0    0    0    0    0 ...
     0    0    0    0    0    0  187  216    0  0    0    0    0    ...
     0    0    0    0    0    0    0    0    0    0    0];
data_in = [test_msg zeros(1,MyConstants.RS_N - length(test_msg) + 1,'uint8')];
data_out = [];

% For comparison
hHDLEnc = comm.HDLRSEncoder(MyConstants.RS_N, MyConstants.RS_K, 'BSource', 'Property', 'B', 0);
for ii = 1:MyConstants.RS_N+1
    messageStart = (ii==1);
    messageEnd   = (ii==MyConstants.RS_K);
    validIn      = (ii<=MyConstants.RS_N);
    if validIn
        byte_in = data_in(ii);
    else
        byte_in = uint8(0);
    end;

    [encOut(ii), startOut(ii), endOut(ii), validOut(ii)] = ...
        step(hHDLEnc, byte_in, messageStart, messageEnd, validIn);
    
    fprintf('[%3d] %3d %3d %3d %3d\n', ii-1, encOut(ii), startOut(ii), endOut(ii), validOut(ii));
end
data_check = encOut(find(startOut):find(endOut));

