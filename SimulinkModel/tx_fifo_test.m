function [dout, empty, byte_recieved, full, bytes_available] = ...
    tx_fifo_test(get_byte)
%
%  First In First Out (FIFO) structure.
%  This FIFO stores integers.
%  The FIFO is actually a circular buffer.
%
persistent head tail fifo byte_out messageWithCRC ml

if (isempty(head))
    head = 1;
    tail = 2;
    byte_out = 0;
end

if isempty(fifo)
    fifo = zeros(1,1024);
end

if isempty(messageWithCRC)
%Initialization from qpsk_tb
    messageASCII = 'hello world!';
    message = double(unicode2native(messageASCII));
    % add on length of message to the front with four bytes
    msgLength = length(message);
    messageWithNumBytes =[ ...
        mod(msgLength,2^8) ...
        mod(floor(msgLength/2^8),2^8) ...
        mod(floor(msgLength/2^16),2^8) ...
        1 ... % message ID
        message];
    % add two bytes at the end, which is a CRC
    messageWithCRC = CreateAppend16BitCRC(messageWithNumBytes);
    ml = length(messageWithCRC);
end
%End initialization from qpsk_tb

full = 0;
empty = 0;
byte_recieved = 0;

%Empty so need to refill buffer
if ((tail == 1 && head == 1024) || ((head + 1) == tail))
    for(x=1:ml)
        fifo(tail) = messageWithCRC(x);
        tail = tail + 1;
        if tail == 1025
           tail = 1;
        end
    end
    byte_recieved = 1;
end

if ((head == 1 && tail == 1024) || ((tail + 1) == head))
    full = 1;
end

%%%%%%%%%%%%%%get%%%%%%%%%%%%%%%%%%%%%
if (get_byte && empty == 0)
    head = head + 1;
    if head == 1025
        head = 1;
    end
    byte_out = fifo(head);
end


% Section for calculating num bytes in FIFO
if (head < tail)
    bytes_available = (tail - head) - 1;
else
    bytes_available = (1024 - head) + tail - 1;
end

dout = byte_out;

end