function [ out_i, out_q, eq_train ] = equalizer(in_i, in_q, start_training, quad)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    persistent in_training;
    persistent i_buffer;
    persistent q_buffer;
    persistent buffer_pointer;
    persistent i_coefficients;
    persistent q_coefficients;
    persistent training_counter;
    persistent train_eq_i;
    persistent train_eq_q;
    persistent test_i;
    persistent debounce;
    
    
    if isempty(i_buffer)
        in_training = 0;
        i_buffer = zeros(1,25);
        q_buffer = zeros(1,25);
        buffer_pointer = 1;
        i_coefficients = zeros(1,25);
        i_coefficients(11) = 0.15;
        i_coefficients(12) = 0.2;
        i_coefficients(13) = 0.3;
        i_coefficients(14) = 0.2;
        i_coefficients(15) = 0.15;
        q_coefficients = zeros(1,25);
        q_coefficients(11) = 0.15;
        q_coefficients(12) = 0.2;
        q_coefficients(13) = 0.3;
        q_coefficients(14) = 0.2;
        q_coefficients(15) = 0.15;
        train_eq_i = equal_i_exp();
        train_eq_q = equal_q_exp();
        training_counter = 0;
        test_i = zeros(1,505);
        debounce = 0;
    end
    i_buffer(buffer_pointer) = in_i;
    q_buffer(buffer_pointer) = in_q;
    %Make sure this doesn't screw up anything down below
    
    if(start_training == 1 && in_training == 0 && debounce == 0)
        in_training = 1;
        training_counter = 1;
        debounce = 1;
    end
    
    if(start_training == 0)
        debounce = 0;
        i_coefficients = zeros(1,25);
        i_coefficients(11) = 0.15;
        i_coefficients(12) = 0.2;
        i_coefficients(13) = 0.3;
        i_coefficients(14) = 0.2;
        i_coefficients(15) = 0.15;
        q_coefficients = zeros(1,25);
        q_coefficients(11) = 0.15;
        q_coefficients(12) = 0.2;
        q_coefficients(13) = 0.3;
        q_coefficients(14) = 0.2;
        q_coefficients(15) = 0.15;
    end
    
    temp_i = 0;
    temp_q = 0;
    offset = 0;
    for(i = 1:25)
        if(buffer_pointer+offset+i >= 26)
            offset = offset - 25;
        end
        temp_i = temp_i + i_buffer(i+buffer_pointer+offset)*i_coefficients(i);
        temp_q = temp_q + q_buffer(i+buffer_pointer+offset)*q_coefficients(i);
    end
    out_i = temp_i;
    out_q = temp_q;
    
    if(in_training == 1 && training_counter > 0)
        test_i(training_counter) = in_q;
        if(training_counter >= 488) % Need to fill in this value
            % This is the last value to train on
            in_training = 0;
        end
        if quad == 0
            expected_i = train_eq_i(training_counter+16);
            expected_q = train_eq_q(training_counter+16);
        elseif quad==1
            expected_i = -train_eq_q(training_counter+16);
            expected_q = train_eq_i(training_counter+16);
        elseif quad==2
            expected_i = -train_eq_i(training_counter+16);
            expected_q = -train_eq_q(training_counter+16);
        else
            expected_i = train_eq_q(training_counter+16);
            expected_q = -train_eq_i(training_counter+16);
        end
        error_i = (expected_i - out_i)/8;
        error_q = (expected_q - out_q)/8;
        offset = 0;  % Probably?
        for iter = 1:25
            if(buffer_pointer+offset+iter >= 26)
                offset = offset - 25;
            end
            i_coefficients(iter) = i_buffer(buffer_pointer+offset+iter) * error_i + i_coefficients(iter);
            q_coefficients(iter) = q_buffer(buffer_pointer+offset+iter) * error_q + q_coefficients(iter);
        end
    end
    if(in_training == 1)
        training_counter = training_counter+1;
    end
    buffer_pointer = buffer_pointer + 1;
    if(buffer_pointer >= 26)
        buffer_pointer = 1;
    end
    eq_train = in_training;
end

