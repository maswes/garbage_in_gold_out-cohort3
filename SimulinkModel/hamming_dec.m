function [ output ] = hamming_dec( input )
    input_vec = [bitget(input, 1) bitget(input, 2) bitget(input, 3) bitget(input, 4) bitget(input, 5) bitget(input, 6) bitget(input, 7)];
    err_loc = bitxor(bitxor(bitxor(input_vec(1),input_vec(3)),input_vec(5)),input_vec(7));
    err_loc = err_loc + 2*bitxor(bitxor(bitxor(input_vec(2),input_vec(3)),input_vec(6)),input_vec(7));
    err_loc = err_loc + 4*bitxor(bitxor(bitxor(input_vec(4),input_vec(5)),input_vec(6)),input_vec(7));
    
    if(err_loc >0)
        input_vec(err_loc) = ~input_vec(err_loc);
    end
    
    output = input_vec(3) + input_vec(5) * 2 + input_vec(6) * 4 + input_vec(7) * 8;
    
end
