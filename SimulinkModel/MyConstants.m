classdef MyConstants
    properties (Constant = true)
        FIFO_DEPTH = 2048;
        RS_N = 255;
        RS_K = 181;
        HEADER_LEN = 3;
        ADD_ERRORS = 0;
        USE_SCRAMBLER = 1;
        hpn = comm.PNSequence('Polynomial', [11 9 0], ...
            'SamplesPerFrame', 2048, ...
            'InitialConditions', [0 0 0 0 0 0 0 0 0 0 1]);
        pn_seq = step(MyConstants.hpn);
        SCRAMBLER = bi2de(reshape(MyConstants.pn_seq', [], 8));
    end
end
