function [dout, empty, byte_recieved, full, bytes_available] = ...
    tx_fifo_fec(get_byte, store_byte, byte_in, reset_fifo)
%
%  First In First Out (FIFO) structure.
%  This FIFO stores integers.
%  The FIFO is actually a circular buffer.
%
persistent head tail fifo byte_out debounce hHDLEnc message_start count scram_count

if (isempty(head) || reset_fifo == 1)
    head = 1;
    tail = 1;
    byte_out = 0;
    debounce = 0;
    count = 1;
    scram_count = 1;
    message_start = true;
end

if isempty(fifo)
    fifo = zeros(1,2048);
    hHDLEnc = comm.HDLRSEncoder(MyConstants.RS_N, MyConstants.RS_K, 'BSource', 'Property', 'B', 0);
end

full = 0;
empty = 0;
byte_recieved = 0;

if ((tail == 1 && head == MyConstants.FIFO_DEPTH) || (head == tail))
    empty = 1;
end
if ((head == 1 && tail == MyConstants.FIFO_DEPTH) || ((tail + 1) == head))
    full = 1;
end
if store_byte == 0
    debounce = 0;
end
if debounce == 1
    byte_recieved = 1;
end

%%%%%%%%%%%%%%get%%%%%%%%%%%%%%%%%%%%%
if (get_byte && empty == 0)
    byte_out = fifo(head);
    head = head + 1;
    if head == MyConstants.FIFO_DEPTH + 1
        head = 1;
    end
end

%%%%%%%%%%%%%put%%%%%%%%%%%%%%%%%%%%%
if (store_byte && full == 0 && debounce == 0)
    if(count==MyConstants.RS_N + 2) % Should be 1, but this is a total hack because an extra byte gets stored than ever retrieved
        count = 1;
    end
    message_start = (count == 1);
    message_end = (count == MyConstants.RS_K);
    valid_in = (count <= MyConstants.RS_K);

    [enc_out, start_out, end_out, valid_out] = ...
        step(hHDLEnc, byte_in, message_start, message_end, valid_in);
    
    count = count + 1;
    
    if valid_out
        % scramble output
        orig_out = enc_out;
        if MyConstants.USE_SCRAMBLER
            if scram_count == 256
                scram_count = 1;
            end
            enc_out = bitxor(enc_out, MyConstants.SCRAMBLER(scram_count));
            scram_count = scram_count + 1;
        end
        
        %fprintf('[%3d] %3d %3d %3d %3d %3d; %3d %3d %3d %3d\n', scram_count, orig_out, enc_out, start_out, end_out, valid_out, byte_in, message_start, message_end, valid_in);
        if MyConstants.ADD_ERRORS && ((count == 6) || (count == 7))
            %fprintf('Changing %d to 1\n', enc_out);
            enc_out = 1;
        end
        fifo(tail) = enc_out;
        tail = tail + 1;
        if tail == MyConstants.FIFO_DEPTH + 1
            tail = 1;
        end
    end
    
    byte_recieved = 1;    
    debounce = 1;
end

% Section for calculating num bytes in FIFO
if (head < tail)
    bytes_available = (tail - head);
elseif (head == tail)
    bytes_available = 0;
else
    bytes_available = (MyConstants.FIFO_DEPTH - head) + tail;
end

dout = byte_out;

end