function [dout, bytes_available, byte_ready, empty, full, d_1] = ...
    rx_fifo_fec(reset_fifo, store_byte, byte_in, get_byte)

%
%  First In First Out (FIFO) structure.
%  This FIFO stores integers.
%  The FIFO is actually a circular buffer.
%

persistent head tail fifo byte_out handshake sb_handshake hHDLDec message_start count scram_count

if (reset_fifo || isempty(head))
    head = 1;
    tail = 2;
    byte_out = 0;
    handshake = 0;
    sb_handshake = 0;
    count = 1;
    scram_count = 1;
    message_start = false;
end

if isempty(fifo)
    fifo = zeros(1,MyConstants.FIFO_DEPTH);
    hHDLDec = comm.HDLRSDecoder(MyConstants.RS_N, MyConstants.RS_K,'BSource','Property','B',0);
end

full = 0;
empty = 0;

% handshaking logic
if (handshake == 1 && get_byte == 0)   % reset for next request
    byte_ready = 0;
    handshake = 0;                  
elseif (handshake == 1)                 % keep byte ready until users flags they are done
    byte_ready = 1;                   
else
    byte_ready = 0;                     % no requests, no byte ready
end

if store_byte == 0;
    sb_handshake = 0;
end

if ((tail == 1 && head == MyConstants.FIFO_DEPTH) || ((head + 1) == tail))
    empty = 1;
end
if ((head == 1 && tail == MyConstants.FIFO_DEPTH) || ((tail + 1) == head))
    full = 1;
end

%%%%%%%%%%%%%%get%%%%%%%%%%%%%%%%%%%%%
if (get_byte && handshake == 0 && ~empty)
    head = head + 1;
    if head == MyConstants.FIFO_DEPTH + 1
        head = 1;
    end
    byte_ready = 1;
    handshake = 1;
    byte_out = fifo(head);
end
%%%%%%%%%%%%%put%%%%%%%%%%%%%%%%%%%%%
if (store_byte && ~full && sb_handshake == 0)
    
    if count <= MyConstants.HEADER_LEN
        fifo(tail) = byte_in;
        sb_handshake = 1;
        tail = tail + 1;
        if tail == MyConstants.FIFO_DEPTH + 1
            tail = 1;
        end
        
        count = count + 1;
    else
        
        message_start = (count == MyConstants.HEADER_LEN + 1);
        message_end = (count == MyConstants.RS_N + MyConstants.HEADER_LEN);
        valid_in = (count > MyConstants.HEADER_LEN) && (count <= MyConstants.RS_N + MyConstants.HEADER_LEN);
        loops = 1;
        
        if message_end
            loops = 1024;
        end
        
        for ii = 1:loops
            % Ugly code here; was late.
            message_start = (count == MyConstants.HEADER_LEN + 1);
            message_end = (count == MyConstants.RS_N + MyConstants.HEADER_LEN);
            valid_in = (count > MyConstants.HEADER_LEN) && (count <= MyConstants.RS_N + MyConstants.HEADER_LEN);
            
            orig_in = byte_in;
            
            % unscramble output
            if MyConstants.USE_SCRAMBLER
                if (count >= MyConstants.HEADER_LEN + 1) && (count <= MyConstants.RS_N + MyConstants.HEADER_LEN)
                    byte_in = bitxor(byte_in, MyConstants.SCRAMBLER(scram_count));
                    scram_count = scram_count + 1;
                end
            end

            [dec_out, start_out, end_out, valid_out, dec_err_out] =  ...
                step(hHDLDec, byte_in, message_start, message_end, valid_in);
                        
            if valid_out
                fifo(tail) = dec_out;
                sb_handshake = 1;
                tail = tail + 1;
                if tail == MyConstants.FIFO_DEPTH + 1
                    tail = 1;
                end
            end
            
            %fprintf('[%3d] %3d %3d %3d %3d %3d; %3d %3d %3d %3d %3d\n', count, dec_out, start_out, end_out, valid_out, dec_err_out, orig_in, byte_in, message_start, message_end, valid_in);
            count = count + 1;
            
            if end_out
                break;
            end
        end
    end
end

% Section for calculating num bytes in FIFO
if (head < tail)
    bytes_available = (tail - head) - 1;
else
    bytes_available = (MyConstants.FIFO_DEPTH - head) + tail - 1;
end

dout = byte_out;
d_1 = byte_out;
end