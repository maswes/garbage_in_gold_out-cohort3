function [numRecBytes, msgBytes, rssi] = qpsk_tb_rx(r)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Main receiver core
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r_out = zeros(1,length(r)+200);
bytes = zeros(1,1000); byte_count = 0; next_byte = 0; percent_full = 0;
rx_fifo_byte_out = zeros(1,length(r)+200);
byte_ready = zeros(1,length(r)+200);
bytes_available = zeros(1,length(r)+200);
for i1 = 1:length(r)+200
    if i1 == 1
        mcu_rdy = 0;
    else
        mcu_rdy = 1;
    end
    if i1 > length(r)
        r_in = 0;
    else
        r_in = r(i1);
    end
    i_in = round(real(r_in)*128)/128;
    q_in = round(imag(r_in)*128)/128;
    r_out(i1) = real(complex(i_in,q_in));
    
    % Disabling DC Offset until we add a scrambler
    
    [dc_i_out, dc_q_out, rssi_out, rssi_en_out, dir_out, dir_en_out, ~] = ...
        dc_offset_correction(i_in, q_in, mod(i1,2), 500, 1500, +(i1>3000), 1);
    
    [store_byte, byte, num_bytes_ready, clear_fifo_out, blinky, rssi] =...
        qpsk_rx(dc_i_out, dc_q_out, mcu_rdy);

    
    
      
    %[store_byte, byte, num_bytes_ready, clear_fifo_out, blinky, rssi] =...
    %    qpsk_rx(i_in, q_in, mcu_rdy);
    

    % To FIFO
    [rx_fifo_byte_out(i1), bytes_available(i1), byte_ready(i1)] = ...
        rx_fifo(clear_fifo_out, store_byte, byte, next_byte);
    if (i1>1)
        if (byte_ready(i1) == 1 && byte_ready(i1-1) == 0)
            byte_count = byte_count + 1;
            bytes(byte_count) = rx_fifo_byte_out(i1);
            next_byte=0;
        else
            next_byte=1;
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

numRecBytes = 255;
assert(numRecBytes<1000);
msgBytes = zeros(1,numRecBytes);
msgBytes = bytes((1+3):(numRecBytes+3));
end