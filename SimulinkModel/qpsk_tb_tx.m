function [x] = qpsk_tb_tx(message)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Emulate microprocessor packet creation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% add on length of message to the front with four bytes
msgLength = length(message);
% add two bytes at the end, which is a CRC
messageWithCRC = CreateAppend16BitCRC(message);
ml = length(messageWithCRC);
% Put this
% Concerned about the '+1'
messageWithCRC = [7 7 7 messageWithCRC zeros(1, MyConstants.RS_N - ml)];
%Very Complete test
for x = 4:258
    messageWithCRC(x) = x-4;
end
%pad = repmat([0 1 0 0 1 1], 38);
%messageWithCRC = [messageWithCRC pad(1,1:228) zeros(1, MyConstants.RS_N - ml + 1 - 228)];
ml = length(messageWithCRC);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

all_data_in = [];
all_data_out = [];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FPGA radio transmit core
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data_in = 0;
empty_in = 1;
tx_en_in = 0;
store_byte = 0;
numBytesFromFifo = 0;
num_samp = 11640;
x = complex(zeros(1,num_samp),zeros(1,num_samp));
CORE_LATENCY = 4;
data_buf = zeros(1,CORE_LATENCY);
store_byte_buf = zeros(1,CORE_LATENCY);
clear_buf = zeros(1,CORE_LATENCY);
tx_en_buf = zeros(1,CORE_LATENCY);
reset_fifo = 0;
byte_request = 0;
for i1 = 1:num_samp
    % first thing the processor does is clear the internal tx fifo
    if i1 == 1
        clear_fifo_in = 1;
    else
        clear_fifo_in = 0;
    end
    
    data_buf = [data_buf(2:end) data_in];
    store_byte_buf = [store_byte_buf(2:end) store_byte];
    clear_buf = [clear_buf(2:end) clear_fifo_in];
    tx_en_buf = [tx_en_buf(2:end) tx_en_in];

    [new_data_in, empty_in, byte_recieved, full, percent_full] = ...
        tx_fifo(byte_request, store_byte_buf(1), data_buf(1), reset_fifo);

    [i_out, q_out, tx_done_out, request_byte, clear_fifo_in_done] = ...
        qpsk_tx(new_data_in,empty_in,clear_buf(1),tx_en_buf(1));

    x_out = complex(i_out,q_out)/2^11;
    x(i1) = x_out;
    byte_request = request_byte;
    
    %%% Emulate write to FIFO interface
    if mod(i1,8) == 1 && numBytesFromFifo == length(messageWithCRC)
        numBytesFromFifo = numBytesFromFifo + 1;
    end
    if mod(i1,8) == 1 && numBytesFromFifo < length(messageWithCRC)
        data_in = messageWithCRC(numBytesFromFifo+1);
        numBytesFromFifo = numBytesFromFifo + 1;
    end
     
    %%% Software lags a but on the handshaking signals %%%
    if (0 < mod(i1,8) && mod(i1,8) < 5) && tx_en_in == 0
        store_byte = 1;
    else
        store_byte = 0;
    end
    % processor loaded all bytes into FIFO so begin transmitting
    if (numBytesFromFifo == (length(messageWithCRC) + 1) && mod(i1,8) > 5)
        empty_in = 1;
        tx_en_in = 1;
    end
end
end
