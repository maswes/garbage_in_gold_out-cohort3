function [ output ] = triple_hamming_dec( input1, input2, input3 )
    hamming_val = bitor(bitand(input1,input2), bitor(bitand(input1, input3),bitand(input2, input3)));
    output = hamming_dec(hamming_val);
end
