%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% QPSK demonstration packet-based transceiver for Chilipepper
% Toyon Research Corp.
% http://www.toyon.com/chilipepper.php
% Created 10/17/2012
% embedded@toyon.com
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This file is the top level entry function for the receiver portion of the
% example. The entire receiver is designed to run at Rate=1 (one clock
% cycle per iteration of the core. 
% We follow standard receive practice with frequency offset estimation,
% pulse-shape filtering, time estimateion, and correlation to determine
% tart of packet.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%#codegen
function [store_byte, byte, num_bytes_ready, clear_fifo_out, blinky, rssi] =...
    qpsk_rx(i_in, q_in, mcu_rx_ready_in)

persistent finish_rx_latch
persistent blinky_cnt
persistent store_sfi
persistent store_sfq
persistent counter
persistent counter2
persistent store_sci
persistent store_scq
persistent store_sei
persistent store_seq
persistent store_i_in
persistent store_q_in
persistent packet_det packet_quad
persistent debounce
persistent store_sti store_stq

if isempty(finish_rx_latch)
    finish_rx_latch = 0; % feedback once packet is received to rest
    blinky_cnt = 0;
    store_i_in = zeros(3,2000);
    store_q_in = zeros(3,2000);
    store_sfi = zeros(3,2000);
    store_sfq = zeros(3,2000);
    store_sci = zeros(3,2000);
    store_scq = zeros(3,2000);
    store_sei = zeros(3,2000);
    store_seq = zeros(3,2000);
    store_sti = zeros(3,2000);
    store_stq = zeros(3,2000);
    counter = 0;
    counter2 = 0;
    packet_det = 0;
    packet_quad = 0;
    debounce = 0;
end
    store_i_in(counter2+1,1:2000) = [store_i_in(counter2+1,2:2000) i_in];
    store_q_in(counter2+1,1:2000) = [store_q_in(counter2+1,2:2000) q_in];



% frequency offset estimation. Note that time constant is input as integer
[s_f_i, s_f_q] = ...
    qpsk_rx_foc(i_in, q_in, finish_rx_latch);

% Square-root raised-cosine band-limited filtering
[s_c_i, s_c_q] = qpsk_rx_srrc(s_f_i, s_f_q);

% Pulled out equalizer :(. Maybe some other capstone group can step up.
%[s_e_i, s_e_q, eq_train] = ...
%    equalizer(s_c_i, s_c_q, packet_det, packet_quad);

% Time offset estimation. Output data changes at the symbol rate.
[s_t_i, s_t_q] = ...
    qpsk_rx_toc(s_c_i, s_c_q, finish_rx_latch);

if(packet_det == 0)
    store_i_in(counter2+1,1:2000) = [store_i_in(counter2+1,2:2000) i_in];
    store_q_in(counter2+1,1:2000) = [store_q_in(counter2+1,2:2000) q_in];
    store_sfi(counter2+1,1:2000) = [store_sfi(counter2+1,2:2000) s_f_i];
    store_sfq(counter2+1,1:2000) = [store_sfq(counter2+1,2:2000) s_f_q];
    store_sci(counter2+1,1:2000) = [store_sci(counter2+1,2:2000) s_c_i];
    store_scq(counter2+1,1:2000) = [store_scq(counter2+1,2:2000) s_c_q];
    store_sei(counter2+1,1:2000) = [store_sei(counter2+1,2:2000) s_e_i];
    store_seq(counter2+1,1:2000) = [store_seq(counter2+1,2:2000) s_e_q];
    store_sti(counter2+1,1:2000) = [store_sti(counter2+1,2:2000) s_t_i];
    store_stq(counter2+1,1:2000) = [store_stq(counter2+1,2:2000) s_t_q];
end

% Determine start of packet using front-loaded training sequence
[byte, store_byte, finish_rx, num_bytes_ready, clear_fifo_out, rssi, packet_det, packet_quad] = ...
    qpsk_rx_correlator(s_t_i, s_t_q, mcu_rx_ready_in);

if debounce==0 && packet_det==1
    counter2 = counter2 + 1;
    if counter2 >= 3
        counter2 = 0;
    end
    debounce = 1;
end

if packet_det == 0
    debounce = 0;
end

blinky_cnt = blinky_cnt + 1;
if blinky_cnt == 20000000
    blinky_cnt = 0;
end
blinky = floor(blinky_cnt/10000000);
finish_rx_latch = finish_rx;

counter = counter + 1;
if counter >= 8
    counter = 0;
end