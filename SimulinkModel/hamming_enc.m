function [ output ] = hamming_enc( input )
    input1 = bitget(input, 1);
    input2 = bitget(input, 2);
    input3 = bitget(input, 3);
    input4 = bitget(input, 4);
    output1 = bitxor(bitxor(input1, input2), input4);
    output2 = bitxor(bitxor(input1, input3), input4);
    output3 = input1;
    output4 = bitxor(bitxor(input2, input3), input4);
    output5 = input2;
    output6 = input3;
    output7 = input4;
    output = output1 + output2 * 2 + output3 * 4 + output4 * 8 + output5 * 16 + output6 * 32 + output7 * 64;
end

