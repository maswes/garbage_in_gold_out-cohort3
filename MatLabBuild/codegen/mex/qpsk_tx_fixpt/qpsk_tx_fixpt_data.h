/*
 * qpsk_tx_fixpt_data.h
 *
 * Code generation for function 'qpsk_tx_fixpt_data'
 *
 */

#ifndef __QPSK_TX_FIXPT_DATA_H__
#define __QPSK_TX_FIXPT_DATA_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "qpsk_tx_fixpt_types.h"

/* Variable Declarations */
extern const mxArray *eml_mx;
extern const mxArray *b_eml_mx;
extern const mxArray *c_eml_mx;
extern const mxArray *d_eml_mx;
extern emlrtRSInfo c_emlrtRSI;
extern emlrtRSInfo d_emlrtRSI;
extern emlrtRSInfo e_emlrtRSI;
extern emlrtRSInfo f_emlrtRSI;
extern emlrtRSInfo g_emlrtRSI;
extern emlrtRSInfo h_emlrtRSI;
extern emlrtRSInfo i_emlrtRSI;
extern emlrtRSInfo j_emlrtRSI;
extern emlrtRSInfo k_emlrtRSI;
extern emlrtRSInfo l_emlrtRSI;
extern emlrtRSInfo m_emlrtRSI;
extern emlrtRSInfo n_emlrtRSI;
extern emlrtRSInfo o_emlrtRSI;
extern emlrtRSInfo p_emlrtRSI;

#endif

/* End of code generation (qpsk_tx_fixpt_data.h) */
