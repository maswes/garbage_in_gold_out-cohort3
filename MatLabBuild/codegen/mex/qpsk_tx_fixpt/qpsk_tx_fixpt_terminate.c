/*
 * qpsk_tx_fixpt_terminate.c
 *
 * Code generation for function 'qpsk_tx_fixpt_terminate'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "qpsk_tx_fixpt.h"
#include "qpsk_tx_fixpt_terminate.h"
#include "qpsk_tx_fixpt_data.h"

/* Function Definitions */
void qpsk_tx_fixpt_atexit(void)
{
  emlrtStack st = { NULL, NULL, NULL };

  emlrtCreateRootTLS(&emlrtRootTLSGlobal, &emlrtContextGlobal, NULL, 1);
  st.tls = emlrtRootTLSGlobal;
  emlrtEnterRtStackR2012b(&st);
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
  emlrtDestroyArray(&eml_mx);
  emlrtDestroyArray(&b_eml_mx);
  emlrtDestroyArray(&c_eml_mx);
  emlrtDestroyArray(&d_eml_mx);
}

void qpsk_tx_fixpt_terminate(void)
{
  emlrtStack st = { NULL, NULL, NULL };

  st.tls = emlrtRootTLSGlobal;
  emlrtLeaveRtStackR2012b(&st);
  emlrtDestroyRootTLS(&emlrtRootTLSGlobal);
}

/* End of code generation (qpsk_tx_fixpt_terminate.c) */
