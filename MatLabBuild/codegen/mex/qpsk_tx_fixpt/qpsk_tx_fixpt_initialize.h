/*
 * qpsk_tx_fixpt_initialize.h
 *
 * Code generation for function 'qpsk_tx_fixpt_initialize'
 *
 */

#ifndef __QPSK_TX_FIXPT_INITIALIZE_H__
#define __QPSK_TX_FIXPT_INITIALIZE_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "qpsk_tx_fixpt_types.h"

/* Function Declarations */
extern void qpsk_tx_fixpt_initialize(emlrtContext *aContext);

#endif

/* End of code generation (qpsk_tx_fixpt_initialize.h) */
