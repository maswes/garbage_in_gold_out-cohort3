@echo off
set MATLAB=C:\PROGRA~1\MATLAB\2014a
set MATLAB_ARCH=win64
set MATLAB_BIN="C:\Program Files\MATLAB\2014a\bin"
set ENTRYPOINT=mexFunction
set OUTDIR=.\
set LIB_NAME=qpsk_tx_fixpt_mex
set MEX_NAME=qpsk_tx_fixpt_mex
set MEX_EXT=.mexw64
call "C:\PROGRA~1\MATLAB\2014a\sys\lcc64\lcc64\mex\lcc64opts.bat"
echo # Make settings for qpsk_tx_fixpt > qpsk_tx_fixpt_mex.mki
echo COMPILER=%COMPILER%>> qpsk_tx_fixpt_mex.mki
echo COMPFLAGS=%COMPFLAGS%>> qpsk_tx_fixpt_mex.mki
echo OPTIMFLAGS=%OPTIMFLAGS%>> qpsk_tx_fixpt_mex.mki
echo DEBUGFLAGS=%DEBUGFLAGS%>> qpsk_tx_fixpt_mex.mki
echo LINKER=%LINKER%>> qpsk_tx_fixpt_mex.mki
echo LINKFLAGS=%LINKFLAGS%>> qpsk_tx_fixpt_mex.mki
echo LINKOPTIMFLAGS=%LINKOPTIMFLAGS%>> qpsk_tx_fixpt_mex.mki
echo LINKDEBUGFLAGS=%LINKDEBUGFLAGS%>> qpsk_tx_fixpt_mex.mki
echo MATLAB_ARCH=%MATLAB_ARCH%>> qpsk_tx_fixpt_mex.mki
echo BORLAND=%BORLAND%>> qpsk_tx_fixpt_mex.mki
echo OMPFLAGS= >> qpsk_tx_fixpt_mex.mki
echo OMPLINKFLAGS= >> qpsk_tx_fixpt_mex.mki
echo EMC_COMPILER=lcc64>> qpsk_tx_fixpt_mex.mki
echo EMC_CONFIG=optim>> qpsk_tx_fixpt_mex.mki
"C:\Program Files\MATLAB\2014a\bin\win64\gmake" -B -f qpsk_tx_fixpt_mex.mk
