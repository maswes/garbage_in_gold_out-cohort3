/*
 * qpsk_tx_fixpt_data.c
 *
 * Code generation for function 'qpsk_tx_fixpt_data'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "qpsk_tx_fixpt.h"
#include "qpsk_tx_fixpt_data.h"

/* Variable Definitions */
const mxArray *eml_mx;
const mxArray *b_eml_mx;
const mxArray *c_eml_mx;
const mxArray *d_eml_mx;
emlrtRSInfo c_emlrtRSI = { 150, "qpsk_tx_fixpt",
  "C:\\Users\\Charlie\\Documents\\capstone\\MatLabBuild\\codegen\\qpsk_tx\\fixpt\\qpsk_tx_fixpt.m"
};

emlrtRSInfo d_emlrtRSI = { 151, "qpsk_tx_fixpt",
  "C:\\Users\\Charlie\\Documents\\capstone\\MatLabBuild\\codegen\\qpsk_tx\\fixpt\\qpsk_tx_fixpt.m"
};

emlrtRSInfo e_emlrtRSI = { 63, "qpsk_tx_fixpt",
  "C:\\Users\\Charlie\\Documents\\capstone\\MatLabBuild\\codegen\\qpsk_tx\\fixpt\\qpsk_tx_fixpt.m"
};

emlrtRSInfo f_emlrtRSI = { 61, "qpsk_tx_fixpt",
  "C:\\Users\\Charlie\\Documents\\capstone\\MatLabBuild\\codegen\\qpsk_tx\\fixpt\\qpsk_tx_fixpt.m"
};

emlrtRSInfo g_emlrtRSI = { 59, "qpsk_tx_fixpt",
  "C:\\Users\\Charlie\\Documents\\capstone\\MatLabBuild\\codegen\\qpsk_tx\\fixpt\\qpsk_tx_fixpt.m"
};

emlrtRSInfo h_emlrtRSI = { 57, "qpsk_tx_fixpt",
  "C:\\Users\\Charlie\\Documents\\capstone\\MatLabBuild\\codegen\\qpsk_tx\\fixpt\\qpsk_tx_fixpt.m"
};

emlrtRSInfo i_emlrtRSI = { 55, "qpsk_tx_fixpt",
  "C:\\Users\\Charlie\\Documents\\capstone\\MatLabBuild\\codegen\\qpsk_tx\\fixpt\\qpsk_tx_fixpt.m"
};

emlrtRSInfo j_emlrtRSI = { 53, "qpsk_tx_fixpt",
  "C:\\Users\\Charlie\\Documents\\capstone\\MatLabBuild\\codegen\\qpsk_tx\\fixpt\\qpsk_tx_fixpt.m"
};

emlrtRSInfo k_emlrtRSI = { 51, "qpsk_tx_fixpt",
  "C:\\Users\\Charlie\\Documents\\capstone\\MatLabBuild\\codegen\\qpsk_tx\\fixpt\\qpsk_tx_fixpt.m"
};

emlrtRSInfo l_emlrtRSI = { 49, "qpsk_tx_fixpt",
  "C:\\Users\\Charlie\\Documents\\capstone\\MatLabBuild\\codegen\\qpsk_tx\\fixpt\\qpsk_tx_fixpt.m"
};

emlrtRSInfo m_emlrtRSI = { 220, "qpsk_tx_fixpt",
  "C:\\Users\\Charlie\\Documents\\capstone\\MatLabBuild\\codegen\\qpsk_tx\\fixpt\\qpsk_tx_fixpt.m"
};

emlrtRSInfo n_emlrtRSI = { 14, "bitsrl",
  "C:\\Program Files\\MATLAB\\2014a\\toolbox\\eml\\lib\\fixedpoint\\@embedded\\@fi\\bitsrl.m"
};

emlrtRSInfo o_emlrtRSI = { 75, "eml_fi_bitshift",
  "C:\\Program Files\\MATLAB\\2014a\\toolbox\\eml\\lib\\fixedpoint\\@embedded\\@fi\\eml_fi_bitshift.m"
};

emlrtRSInfo p_emlrtRSI = { 236, "eml_fi_bitshift",
  "C:\\Program Files\\MATLAB\\2014a\\toolbox\\eml\\lib\\fixedpoint\\@embedded\\@fi\\eml_fi_bitshift.m"
};

/* End of code generation (qpsk_tx_fixpt_data.c) */
