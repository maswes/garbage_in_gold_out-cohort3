/*
 * qpsk_tx_fixpt.h
 *
 * Code generation for function 'qpsk_tx_fixpt'
 *
 */

#ifndef __QPSK_TX_FIXPT_H__
#define __QPSK_TX_FIXPT_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "qpsk_tx_fixpt_types.h"

/* Function Declarations */
extern void f65_qpsk_tx_byte2sym_init(void);
extern void f72_qpsk_srrc_init(void);
extern void qpsk_tx_fixpt(const emlrtStack *sp, uint8_T data_in, uint8_T
  empty_in, uint8_T clear_fifo_in, uint8_T tx_en_in, int16_T *i_out, int16_T
  *q_out, uint8_T *tx_done_out, uint8_T *request_byte, uint8_T *blinky);
extern void qpsk_tx_fixpt_init(void);

#endif

/* End of code generation (qpsk_tx_fixpt.h) */
