/*
 * _coder_qpsk_tx_fixpt_info.h
 *
 * Code generation for function 'qpsk_tx_fixpt'
 *
 */

#ifndef ___CODER_QPSK_TX_FIXPT_INFO_H__
#define ___CODER_QPSK_TX_FIXPT_INFO_H__
/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"


/* Function Declarations */
extern const mxArray *emlrtMexFcnResolvedFunctionsInfo(void);

#endif
/* End of code generation (_coder_qpsk_tx_fixpt_info.h) */
