/*
 * _coder_qpsk_tx_fixpt_api.h
 *
 * Code generation for function '_coder_qpsk_tx_fixpt_api'
 *
 */

#ifndef ___CODER_QPSK_TX_FIXPT_API_H__
#define ___CODER_QPSK_TX_FIXPT_API_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "qpsk_tx_fixpt_types.h"

/* Function Declarations */
extern void qpsk_tx_fixpt_api(const mxArray * const prhs[4], const mxArray *
  plhs[5]);

#endif

/* End of code generation (_coder_qpsk_tx_fixpt_api.h) */
