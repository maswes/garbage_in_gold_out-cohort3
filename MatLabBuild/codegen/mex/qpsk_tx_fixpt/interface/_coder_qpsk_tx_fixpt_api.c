/*
 * _coder_qpsk_tx_fixpt_api.c
 *
 * Code generation for function '_coder_qpsk_tx_fixpt_api'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "qpsk_tx_fixpt.h"
#include "_coder_qpsk_tx_fixpt_api.h"
#include "qpsk_tx_fixpt_data.h"

/* Function Declarations */
static uint8_T b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static const mxArray *b_emlrt_marshallOut(const emlrtStack *sp, const uint8_T u);
static uint8_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *empty_in,
  const char_T *identifier);
static uint8_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static uint8_T e_emlrt_marshallIn(const mxArray *src);
static uint8_T emlrt_marshallIn(const emlrtStack *sp, const mxArray *data_in,
  const char_T *identifier);
static const mxArray *emlrt_marshallOut(const emlrtStack *sp, const int16_T u);
static uint8_T f_emlrt_marshallIn(const mxArray *src);

/* Function Definitions */
static uint8_T b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  uint8_T y;
  emlrtCheckFiR2012b(sp, parentId, u, false, 0U, 0, eml_mx, b_eml_mx);
  y = e_emlrt_marshallIn(emlrtAlias(u));
  emlrtDestroyArray(&u);
  return y;
}

static const mxArray *b_emlrt_marshallOut(const emlrtStack *sp, const uint8_T u)
{
  const mxArray *y;
  const mxArray *b_y;
  const mxArray *m1;
  y = NULL;
  b_y = NULL;
  m1 = emlrtCreateNumericMatrix(1, 1, mxUINT8_CLASS, mxREAL);
  *(uint8_T *)mxGetData(m1) = u;
  emlrtAssign(&b_y, m1);
  emlrtAssign(&y, emlrtCreateFIR2013b(sp, eml_mx, c_eml_mx, "simulinkarray", b_y,
    true, false));
  return y;
}

static uint8_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *empty_in,
  const char_T *identifier)
{
  uint8_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = d_emlrt_marshallIn(sp, emlrtAlias(empty_in), &thisId);
  emlrtDestroyArray(&empty_in);
  return y;
}

static uint8_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  uint8_T y;
  emlrtCheckFiR2012b(sp, parentId, u, false, 0U, 0, eml_mx, c_eml_mx);
  y = f_emlrt_marshallIn(emlrtAlias(u));
  emlrtDestroyArray(&u);
  return y;
}

static uint8_T e_emlrt_marshallIn(const mxArray *src)
{
  uint8_T ret;
  const mxArray *mxInt;
  mxInt = emlrtImportFiIntArrayR2008b(src);
  ret = *(uint8_T *)mxGetData(mxInt);
  emlrtDestroyArray(&mxInt);
  emlrtDestroyArray(&src);
  return ret;
}

static uint8_T emlrt_marshallIn(const emlrtStack *sp, const mxArray *data_in,
  const char_T *identifier)
{
  uint8_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = b_emlrt_marshallIn(sp, emlrtAlias(data_in), &thisId);
  emlrtDestroyArray(&data_in);
  return y;
}

static const mxArray *emlrt_marshallOut(const emlrtStack *sp, const int16_T u)
{
  const mxArray *y;
  const mxArray *b_y;
  const mxArray *m0;
  y = NULL;
  b_y = NULL;
  m0 = emlrtCreateNumericMatrix(1, 1, mxINT16_CLASS, mxREAL);
  *(int16_T *)mxGetData(m0) = u;
  emlrtAssign(&b_y, m0);
  emlrtAssign(&y, emlrtCreateFIR2013b(sp, eml_mx, d_eml_mx, "simulinkarray", b_y,
    true, false));
  return y;
}

static uint8_T f_emlrt_marshallIn(const mxArray *src)
{
  uint8_T ret;
  const mxArray *mxInt;
  mxInt = emlrtImportFiIntArrayR2008b(src);
  ret = *(uint8_T *)mxGetData(mxInt);
  emlrtDestroyArray(&mxInt);
  emlrtDestroyArray(&src);
  return ret;
}

void qpsk_tx_fixpt_api(const mxArray * const prhs[4], const mxArray *plhs[5])
{
  uint8_T data_in;
  uint8_T empty_in;
  uint8_T clear_fifo_in;
  uint8_T tx_en_in;
  uint8_T blinky;
  uint8_T request_byte;
  uint8_T tx_done_out;
  int16_T q_out;
  int16_T i_out;
  emlrtStack st = { NULL, NULL, NULL };

  st.tls = emlrtRootTLSGlobal;

  /* Marshall function inputs */
  data_in = emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "data_in");
  empty_in = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[1]), "empty_in");
  clear_fifo_in = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[2]), "clear_fifo_in");
  tx_en_in = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[3]), "tx_en_in");

  /* Invoke the target function */
  qpsk_tx_fixpt(&st, data_in, empty_in, clear_fifo_in, tx_en_in, &i_out, &q_out,
                &tx_done_out, &request_byte, &blinky);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(&st, i_out);
  plhs[1] = emlrt_marshallOut(&st, q_out);
  plhs[2] = b_emlrt_marshallOut(&st, tx_done_out);
  plhs[3] = b_emlrt_marshallOut(&st, request_byte);
  plhs[4] = b_emlrt_marshallOut(&st, blinky);
}

/* End of code generation (_coder_qpsk_tx_fixpt_api.c) */
