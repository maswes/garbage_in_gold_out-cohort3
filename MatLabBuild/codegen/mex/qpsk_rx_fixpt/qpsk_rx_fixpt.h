/*
 * qpsk_rx_fixpt.h
 *
 * Code generation for function 'qpsk_rx_fixpt'
 *
 */

#ifndef __QPSK_RX_FIXPT_H__
#define __QPSK_RX_FIXPT_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "qpsk_rx_fixpt_types.h"

/* Function Declarations */
extern void f108_qpsk_rx_correlator_init(void);
extern void f23_qpsk_rx_foc_init(void);
extern void f55_qpsk_rx_srrc_init(void);
extern void f58_qpsk_rx_toc_init(void);
extern void qpsk_rx_fixpt(const emlrtStack *sp, int16_T i_in, int16_T q_in,
  uint8_T mcu_rx_ready_in, uint8_T *store_byte, uint8_T *byte, uint16_T
  *num_bytes_ready, uint8_T *clear_fifo_out, uint8_T *blinky);
extern void qpsk_rx_fixpt_init(void);

#endif

/* End of code generation (qpsk_rx_fixpt.h) */
