/*
 * _coder_qpsk_rx_fixpt_api.c
 *
 * Code generation for function '_coder_qpsk_rx_fixpt_api'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "qpsk_rx_fixpt.h"
#include "_coder_qpsk_rx_fixpt_api.h"
#include "qpsk_rx_fixpt_data.h"

/* Function Declarations */
static int16_T b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static const mxArray *b_emlrt_marshallOut(const emlrtStack *sp, const uint8_T u);
static uint8_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *mcu_rx_ready_in, const char_T *identifier);
static const mxArray *c_emlrt_marshallOut(const emlrtStack *sp, const uint16_T u);
static uint8_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static int16_T e_emlrt_marshallIn(const mxArray *src);
static int16_T emlrt_marshallIn(const emlrtStack *sp, const mxArray *i_in, const
  char_T *identifier);
static const mxArray *emlrt_marshallOut(const emlrtStack *sp, const uint8_T u);
static uint8_T f_emlrt_marshallIn(const mxArray *src);

/* Function Definitions */
static int16_T b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  int16_T y;
  emlrtCheckFiR2012b(sp, parentId, u, false, 0U, 0, eml_mx, b_eml_mx);
  y = e_emlrt_marshallIn(emlrtAlias(u));
  emlrtDestroyArray(&u);
  return y;
}

static const mxArray *b_emlrt_marshallOut(const emlrtStack *sp, const uint8_T u)
{
  const mxArray *y;
  const mxArray *b_y;
  const mxArray *m1;
  y = NULL;
  b_y = NULL;
  m1 = emlrtCreateNumericMatrix(1, 1, mxUINT8_CLASS, mxREAL);
  *(uint8_T *)mxGetData(m1) = u;
  emlrtAssign(&b_y, m1);
  emlrtAssign(&y, emlrtCreateFIR2013b(sp, eml_mx, d_eml_mx, "simulinkarray", b_y,
    true, false));
  return y;
}

static uint8_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray
  *mcu_rx_ready_in, const char_T *identifier)
{
  uint8_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = d_emlrt_marshallIn(sp, emlrtAlias(mcu_rx_ready_in), &thisId);
  emlrtDestroyArray(&mcu_rx_ready_in);
  return y;
}

static const mxArray *c_emlrt_marshallOut(const emlrtStack *sp, const uint16_T u)
{
  const mxArray *y;
  const mxArray *b_y;
  const mxArray *m2;
  y = NULL;
  b_y = NULL;
  m2 = emlrtCreateNumericMatrix(1, 1, mxUINT16_CLASS, mxREAL);
  *(uint16_T *)mxGetData(m2) = u;
  emlrtAssign(&b_y, m2);
  emlrtAssign(&y, emlrtCreateFIR2013b(sp, eml_mx, e_eml_mx, "simulinkarray", b_y,
    true, false));
  return y;
}

static uint8_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  uint8_T y;
  emlrtCheckFiR2012b(sp, parentId, u, false, 0U, 0, eml_mx, c_eml_mx);
  y = f_emlrt_marshallIn(emlrtAlias(u));
  emlrtDestroyArray(&u);
  return y;
}

static int16_T e_emlrt_marshallIn(const mxArray *src)
{
  int16_T ret;
  const mxArray *mxInt;
  mxInt = emlrtImportFiIntArrayR2008b(src);
  ret = *(int16_T *)mxGetData(mxInt);
  emlrtDestroyArray(&mxInt);
  emlrtDestroyArray(&src);
  return ret;
}

static int16_T emlrt_marshallIn(const emlrtStack *sp, const mxArray *i_in, const
  char_T *identifier)
{
  int16_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = b_emlrt_marshallIn(sp, emlrtAlias(i_in), &thisId);
  emlrtDestroyArray(&i_in);
  return y;
}

static const mxArray *emlrt_marshallOut(const emlrtStack *sp, const uint8_T u)
{
  const mxArray *y;
  const mxArray *b_y;
  const mxArray *m0;
  y = NULL;
  b_y = NULL;
  m0 = emlrtCreateNumericMatrix(1, 1, mxUINT8_CLASS, mxREAL);
  *(uint8_T *)mxGetData(m0) = u;
  emlrtAssign(&b_y, m0);
  emlrtAssign(&y, emlrtCreateFIR2013b(sp, eml_mx, c_eml_mx, "simulinkarray", b_y,
    true, false));
  return y;
}

static uint8_T f_emlrt_marshallIn(const mxArray *src)
{
  uint8_T ret;
  const mxArray *mxInt;
  mxInt = emlrtImportFiIntArrayR2008b(src);
  ret = *(uint8_T *)mxGetData(mxInt);
  emlrtDestroyArray(&mxInt);
  emlrtDestroyArray(&src);
  return ret;
}

void qpsk_rx_fixpt_api(const mxArray * const prhs[3], const mxArray *plhs[5])
{
  int16_T i_in;
  int16_T q_in;
  uint8_T mcu_rx_ready_in;
  uint8_T blinky;
  uint8_T clear_fifo_out;
  uint16_T num_bytes_ready;
  uint8_T byte;
  uint8_T store_byte;
  emlrtStack st = { NULL, NULL, NULL };

  st.tls = emlrtRootTLSGlobal;

  /* Marshall function inputs */
  i_in = emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "i_in");
  q_in = emlrt_marshallIn(&st, emlrtAliasP(prhs[1]), "q_in");
  mcu_rx_ready_in = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[2]),
    "mcu_rx_ready_in");

  /* Invoke the target function */
  qpsk_rx_fixpt(&st, i_in, q_in, mcu_rx_ready_in, &store_byte, &byte,
                &num_bytes_ready, &clear_fifo_out, &blinky);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(&st, store_byte);
  plhs[1] = b_emlrt_marshallOut(&st, byte);
  plhs[2] = c_emlrt_marshallOut(&st, num_bytes_ready);
  plhs[3] = emlrt_marshallOut(&st, clear_fifo_out);
  plhs[4] = emlrt_marshallOut(&st, blinky);
}

/* End of code generation (_coder_qpsk_rx_fixpt_api.c) */
