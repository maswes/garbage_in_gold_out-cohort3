/*
 * _coder_qpsk_rx_fixpt_mex.c
 *
 * Code generation for function 'qpsk_rx_fixpt'
 *
 */

/* Include files */
#include "mex.h"
#include "_coder_qpsk_rx_fixpt_api.h"
#include "qpsk_rx_fixpt_initialize.h"
#include "qpsk_rx_fixpt_terminate.h"

/* Function Declarations */
static void qpsk_rx_fixpt_mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);

/* Variable Definitions */
emlrtContext emlrtContextGlobal = { true, false, EMLRT_VERSION_INFO, NULL, "qpsk_rx_fixpt", NULL, false, {2045744189U,2170104910U,2743257031U,4284093946U}, NULL };
void *emlrtRootTLSGlobal = NULL;

/* Function Definitions */
static void qpsk_rx_fixpt_mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  const mxArray *outputs[5];
  const mxArray *inputs[3];
  int n = 0;
  int nOutputs = (nlhs < 1 ? 1 : nlhs);
  int nInputs = nrhs;
  emlrtStack st = { NULL, NULL, NULL };
  /* Module initialization. */
  qpsk_rx_fixpt_initialize(&emlrtContextGlobal);
  st.tls = emlrtRootTLSGlobal;
  /* Check for proper number of arguments. */
  if (nrhs != 3) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:WrongNumberOfInputs", 5, mxINT32_CLASS, 3, mxCHAR_CLASS, 13, "qpsk_rx_fixpt");
  } else if (nlhs > 5) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:TooManyOutputArguments", 3, mxCHAR_CLASS, 13, "qpsk_rx_fixpt");
  }
  /* Temporary copy for mex inputs. */
  for (n = 0; n < nInputs; ++n) {
    inputs[n] = prhs[n];
  }
  /* Call the function. */
  qpsk_rx_fixpt_api(inputs, outputs);
  /* Copy over outputs to the caller. */
  for (n = 0; n < nOutputs; ++n) {
    plhs[n] = emlrtReturnArrayR2009a(outputs[n]);
  }
  /* Module finalization. */
  qpsk_rx_fixpt_terminate();
}

void qpsk_rx_fixpt_atexit_wrapper(void)
{
   qpsk_rx_fixpt_atexit();
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Initialize the memory manager. */
  mexAtExit(qpsk_rx_fixpt_atexit_wrapper);
  /* Dispatch the entry-point. */
  qpsk_rx_fixpt_mexFunction(nlhs, plhs, nrhs, prhs);
}
/* End of code generation (_coder_qpsk_rx_fixpt_mex.c) */
