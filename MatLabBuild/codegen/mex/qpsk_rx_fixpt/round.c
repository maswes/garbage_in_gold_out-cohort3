/*
 * round.c
 *
 * Code generation for function 'round'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "qpsk_rx_fixpt.h"
#include "round.h"

/* Function Definitions */
int16_T b_round(int32_T a)
{
  int16_T y;
  int32_T i55;
  int32_T i56;
  int32_T i57;
  int32_T i58;
  i55 = a + 2047;
  if (i55 >= 0) {
    i55 = (int32_T)((uint32_T)i55 >> 11);
  } else {
    i55 = ~(int32_T)((uint32_T)~i55 >> 11);
  }

  if ((i55 & 65536) != 0) {
    i56 = i55 | -65536;
  } else {
    i56 = i55 & 65535;
  }

  i55 = i56 - 1;
  if ((i55 & 65536) != 0) {
    i55 |= -65536;
  } else {
    i55 &= 65535;
  }

  i55++;
  if (i55 >= 0) {
    i55 = (int32_T)((uint32_T)i55 >> 1);
  } else {
    i55 = ~(int32_T)((uint32_T)~i55 >> 1);
  }

  if (a >= 0) {
    i57 = (int32_T)((uint32_T)a >> 11);
  } else {
    i57 = ~(int32_T)((uint32_T)~a >> 11);
  }

  if ((i57 & 65536) != 0) {
    i58 = i57 | -65536;
  } else {
    i58 = i57 & 65535;
  }

  i57 = i58 + 1;
  if ((i57 & 65536) != 0) {
    i57 |= -65536;
  } else {
    i57 &= 65535;
  }

  if (a < 0) {
    if ((i55 & 65536) != 0) {
      y = (int16_T)(i55 | -65536);
    } else {
      y = (int16_T)(i55 & 65535);
    }
  } else if (i57 >= 0) {
    y = (int16_T)((uint32_T)i57 >> 1);
  } else {
    y = (int16_T)~(int32_T)((uint32_T)~i57 >> 1);
  }

  return y;
}

/* End of code generation (round.c) */
