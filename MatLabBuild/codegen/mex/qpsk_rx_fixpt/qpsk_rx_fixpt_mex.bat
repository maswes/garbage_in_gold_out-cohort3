@echo off
set MATLAB=C:\PROGRA~1\MATLAB\2014a
set MATLAB_ARCH=win64
set MATLAB_BIN="C:\Program Files\MATLAB\2014a\bin"
set ENTRYPOINT=mexFunction
set OUTDIR=.\
set LIB_NAME=qpsk_rx_fixpt_mex
set MEX_NAME=qpsk_rx_fixpt_mex
set MEX_EXT=.mexw64
call "C:\PROGRA~1\MATLAB\2014a\sys\lcc64\lcc64\mex\lcc64opts.bat"
echo # Make settings for qpsk_rx_fixpt > qpsk_rx_fixpt_mex.mki
echo COMPILER=%COMPILER%>> qpsk_rx_fixpt_mex.mki
echo COMPFLAGS=%COMPFLAGS%>> qpsk_rx_fixpt_mex.mki
echo OPTIMFLAGS=%OPTIMFLAGS%>> qpsk_rx_fixpt_mex.mki
echo DEBUGFLAGS=%DEBUGFLAGS%>> qpsk_rx_fixpt_mex.mki
echo LINKER=%LINKER%>> qpsk_rx_fixpt_mex.mki
echo LINKFLAGS=%LINKFLAGS%>> qpsk_rx_fixpt_mex.mki
echo LINKOPTIMFLAGS=%LINKOPTIMFLAGS%>> qpsk_rx_fixpt_mex.mki
echo LINKDEBUGFLAGS=%LINKDEBUGFLAGS%>> qpsk_rx_fixpt_mex.mki
echo MATLAB_ARCH=%MATLAB_ARCH%>> qpsk_rx_fixpt_mex.mki
echo BORLAND=%BORLAND%>> qpsk_rx_fixpt_mex.mki
echo OMPFLAGS= >> qpsk_rx_fixpt_mex.mki
echo OMPLINKFLAGS= >> qpsk_rx_fixpt_mex.mki
echo EMC_COMPILER=lcc64>> qpsk_rx_fixpt_mex.mki
echo EMC_CONFIG=optim>> qpsk_rx_fixpt_mex.mki
"C:\Program Files\MATLAB\2014a\bin\win64\gmake" -B -f qpsk_rx_fixpt_mex.mk
