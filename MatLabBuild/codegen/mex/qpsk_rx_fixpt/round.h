/*
 * round.h
 *
 * Code generation for function 'round'
 *
 */

#ifndef __ROUND_H__
#define __ROUND_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "qpsk_rx_fixpt_types.h"

/* Function Declarations */
extern int16_T b_round(int32_T a);

#endif

/* End of code generation (round.h) */
