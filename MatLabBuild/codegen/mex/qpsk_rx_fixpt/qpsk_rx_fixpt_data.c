/*
 * qpsk_rx_fixpt_data.c
 *
 * Code generation for function 'qpsk_rx_fixpt_data'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "qpsk_rx_fixpt.h"
#include "qpsk_rx_fixpt_data.h"

/* Variable Definitions */
const mxArray *eml_mx;
const mxArray *b_eml_mx;
const mxArray *c_eml_mx;
const mxArray *d_eml_mx;
const mxArray *e_eml_mx;
emlrtRSInfo e_emlrtRSI = { 141, "qpsk_rx_fixpt",
  "C:\\Users\\Charlie\\Documents\\capstone\\MatLabBuild\\codegen\\qpsk_rx\\fixpt\\qpsk_rx_fixpt.m"
};

emlrtRSInfo f_emlrtRSI = { 142, "qpsk_rx_fixpt",
  "C:\\Users\\Charlie\\Documents\\capstone\\MatLabBuild\\codegen\\qpsk_rx\\fixpt\\qpsk_rx_fixpt.m"
};

emlrtRSInfo g_emlrtRSI = { 420, "qpsk_rx_fixpt",
  "C:\\Users\\Charlie\\Documents\\capstone\\MatLabBuild\\codegen\\qpsk_rx\\fixpt\\qpsk_rx_fixpt.m"
};

emlrtRSInfo h_emlrtRSI = { 14, "bitsrl",
  "C:\\Program Files\\MATLAB\\2014a\\toolbox\\eml\\lib\\fixedpoint\\@embedded\\@fi\\bitsrl.m"
};

emlrtRSInfo i_emlrtRSI = { 75, "eml_fi_bitshift",
  "C:\\Program Files\\MATLAB\\2014a\\toolbox\\eml\\lib\\fixedpoint\\@embedded\\@fi\\eml_fi_bitshift.m"
};

emlrtRSInfo j_emlrtRSI = { 236, "eml_fi_bitshift",
  "C:\\Program Files\\MATLAB\\2014a\\toolbox\\eml\\lib\\fixedpoint\\@embedded\\@fi\\eml_fi_bitshift.m"
};

/* End of code generation (qpsk_rx_fixpt_data.c) */
