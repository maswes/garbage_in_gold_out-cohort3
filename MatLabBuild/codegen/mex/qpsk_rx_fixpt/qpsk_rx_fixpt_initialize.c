/*
 * qpsk_rx_fixpt_initialize.c
 *
 * Code generation for function 'qpsk_rx_fixpt_initialize'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "qpsk_rx_fixpt.h"
#include "qpsk_rx_fixpt_initialize.h"
#include "qpsk_rx_fixpt_data.h"

/* Variable Definitions */
static const volatile char_T *emlrtBreakCheckR2012bFlagVar;
static emlrtMCInfo emlrtMCI = { -1, -1, "", "" };

/* Function Declarations */
static const mxArray *b_numerictype(const emlrtStack *sp, const char * b,
  boolean_T c, const char * d, const char * e, const char * f, real_T g, const
  char * h, real_T i, const char * j, real_T k, const char * l, real_T m, const
  char * n, real_T o, emlrtMCInfo *location);
static const mxArray *fimath(const emlrtStack *sp, const char * b, const char
  * c, const char * d, const char * e, const char * f, const char * g, const
  char * h, const char * i, const char * j, const char * k, const char * l,
  real_T m, const char * n, real_T o, const char * p, real_T r, const char * s,
  real_T t, const char * u, real_T v, const char * w, real_T x, const char * y,
  real_T ab, const char * bb, const char * cb, const char * db, real_T eb, const
  char * fb, real_T gb, const char * hb, real_T ib, const char * jb, real_T kb,
  const char * lb, real_T mb, const char * nb, real_T ob, const char * pb,
  real_T qb, const char * rb, boolean_T sb, emlrtMCInfo *location);
static const mxArray *numerictype(const emlrtStack *sp, const char * b, real_T c,
  const char * d, real_T e, const char * f, real_T g, const char * h, real_T i,
  const char * j, real_T k, emlrtMCInfo *location);
static void qpsk_rx_fixpt_once(const emlrtStack *sp);

/* Function Definitions */
static const mxArray *b_numerictype(const emlrtStack *sp, const char * b,
  boolean_T c, const char * d, const char * e, const char * f, real_T g, const
  char * h, real_T i, const char * j, real_T k, const char * l, real_T m, const
  char * n, real_T o, emlrtMCInfo *location)
{
  const mxArray *pArrays[14];
  const mxArray *m5;
  pArrays[0] = emlrtCreateString(b);
  pArrays[1] = emlrtCreateLogicalScalar(c);
  pArrays[2] = emlrtCreateString(d);
  pArrays[3] = emlrtCreateString(e);
  pArrays[4] = emlrtCreateString(f);
  pArrays[5] = emlrtCreateDoubleScalar(g);
  pArrays[6] = emlrtCreateString(h);
  pArrays[7] = emlrtCreateDoubleScalar(i);
  pArrays[8] = emlrtCreateString(j);
  pArrays[9] = emlrtCreateDoubleScalar(k);
  pArrays[10] = emlrtCreateString(l);
  pArrays[11] = emlrtCreateDoubleScalar(m);
  pArrays[12] = emlrtCreateString(n);
  pArrays[13] = emlrtCreateDoubleScalar(o);
  return emlrtCallMATLABR2012b(sp, 1, &m5, 14, pArrays, "numerictype", true,
    location);
}

static const mxArray *fimath(const emlrtStack *sp, const char * b, const char
  * c, const char * d, const char * e, const char * f, const char * g, const
  char * h, const char * i, const char * j, const char * k, const char * l,
  real_T m, const char * n, real_T o, const char * p, real_T r, const char * s,
  real_T t, const char * u, real_T v, const char * w, real_T x, const char * y,
  real_T ab, const char * bb, const char * cb, const char * db, real_T eb, const
  char * fb, real_T gb, const char * hb, real_T ib, const char * jb, real_T kb,
  const char * lb, real_T mb, const char * nb, real_T ob, const char * pb,
  real_T qb, const char * rb, boolean_T sb, emlrtMCInfo *location)
{
  const mxArray *pArrays[42];
  const mxArray *m3;
  pArrays[0] = emlrtCreateString(b);
  pArrays[1] = emlrtCreateString(c);
  pArrays[2] = emlrtCreateString(d);
  pArrays[3] = emlrtCreateString(e);
  pArrays[4] = emlrtCreateString(f);
  pArrays[5] = emlrtCreateString(g);
  pArrays[6] = emlrtCreateString(h);
  pArrays[7] = emlrtCreateString(i);
  pArrays[8] = emlrtCreateString(j);
  pArrays[9] = emlrtCreateString(k);
  pArrays[10] = emlrtCreateString(l);
  pArrays[11] = emlrtCreateDoubleScalar(m);
  pArrays[12] = emlrtCreateString(n);
  pArrays[13] = emlrtCreateDoubleScalar(o);
  pArrays[14] = emlrtCreateString(p);
  pArrays[15] = emlrtCreateDoubleScalar(r);
  pArrays[16] = emlrtCreateString(s);
  pArrays[17] = emlrtCreateDoubleScalar(t);
  pArrays[18] = emlrtCreateString(u);
  pArrays[19] = emlrtCreateDoubleScalar(v);
  pArrays[20] = emlrtCreateString(w);
  pArrays[21] = emlrtCreateDoubleScalar(x);
  pArrays[22] = emlrtCreateString(y);
  pArrays[23] = emlrtCreateDoubleScalar(ab);
  pArrays[24] = emlrtCreateString(bb);
  pArrays[25] = emlrtCreateString(cb);
  pArrays[26] = emlrtCreateString(db);
  pArrays[27] = emlrtCreateDoubleScalar(eb);
  pArrays[28] = emlrtCreateString(fb);
  pArrays[29] = emlrtCreateDoubleScalar(gb);
  pArrays[30] = emlrtCreateString(hb);
  pArrays[31] = emlrtCreateDoubleScalar(ib);
  pArrays[32] = emlrtCreateString(jb);
  pArrays[33] = emlrtCreateDoubleScalar(kb);
  pArrays[34] = emlrtCreateString(lb);
  pArrays[35] = emlrtCreateDoubleScalar(mb);
  pArrays[36] = emlrtCreateString(nb);
  pArrays[37] = emlrtCreateDoubleScalar(ob);
  pArrays[38] = emlrtCreateString(pb);
  pArrays[39] = emlrtCreateDoubleScalar(qb);
  pArrays[40] = emlrtCreateString(rb);
  pArrays[41] = emlrtCreateLogicalScalar(sb);
  return emlrtCallMATLABR2012b(sp, 1, &m3, 42, pArrays, "fimath", true, location);
}

static const mxArray *numerictype(const emlrtStack *sp, const char * b, real_T c,
  const char * d, real_T e, const char * f, real_T g, const char * h, real_T i,
  const char * j, real_T k, emlrtMCInfo *location)
{
  const mxArray *pArrays[10];
  const mxArray *m4;
  pArrays[0] = emlrtCreateString(b);
  pArrays[1] = emlrtCreateDoubleScalar(c);
  pArrays[2] = emlrtCreateString(d);
  pArrays[3] = emlrtCreateDoubleScalar(e);
  pArrays[4] = emlrtCreateString(f);
  pArrays[5] = emlrtCreateDoubleScalar(g);
  pArrays[6] = emlrtCreateString(h);
  pArrays[7] = emlrtCreateDoubleScalar(i);
  pArrays[8] = emlrtCreateString(j);
  pArrays[9] = emlrtCreateDoubleScalar(k);
  return emlrtCallMATLABR2012b(sp, 1, &m4, 10, pArrays, "numerictype", true,
    location);
}

static void qpsk_rx_fixpt_once(const emlrtStack *sp)
{
  emlrtAssignP(&e_eml_mx, NULL);
  emlrtAssignP(&d_eml_mx, NULL);
  emlrtAssignP(&c_eml_mx, NULL);
  emlrtAssignP(&b_eml_mx, NULL);
  emlrtAssignP(&eml_mx, NULL);
  emlrtAssignP(&e_eml_mx, b_numerictype(sp, "SignednessBool", false,
    "Signedness", "Unsigned", "WordLength", 9.0, "FractionLength", 0.0,
    "BinaryPoint", 0.0, "Slope", 1.0, "FixedExponent", 0.0, &emlrtMCI));
  emlrtAssignP(&d_eml_mx, b_numerictype(sp, "SignednessBool", false,
    "Signedness", "Unsigned", "WordLength", 8.0, "FractionLength", 0.0,
    "BinaryPoint", 0.0, "Slope", 1.0, "FixedExponent", 0.0, &emlrtMCI));
  emlrtAssignP(&c_eml_mx, b_numerictype(sp, "SignednessBool", false,
    "Signedness", "Unsigned", "WordLength", 1.0, "FractionLength", 0.0,
    "BinaryPoint", 0.0, "Slope", 1.0, "FixedExponent", 0.0, &emlrtMCI));
  emlrtAssignP(&b_eml_mx, numerictype(sp, "WordLength", 12.0, "FractionLength",
    0.0, "BinaryPoint", 0.0, "Slope", 1.0, "FixedExponent", 0.0, &emlrtMCI));
  emlrtAssignP(&eml_mx, fimath(sp, "RoundMode", "floor", "RoundingMethod",
    "Floor", "OverflowMode", "wrap", "OverflowAction", "Wrap", "ProductMode",
    "FullPrecision", "ProductWordLength", 32.0, "MaxProductWordLength", 128.0,
    "ProductFractionLength", 30.0, "ProductFixedExponent", -30.0, "ProductSlope",
    9.3132257461547852E-10, "ProductSlopeAdjustmentFactor", 1.0, "ProductBias",
    0.0, "SumMode", "FullPrecision", "SumWordLength", 32.0, "MaxSumWordLength",
    128.0, "SumFractionLength", 30.0, "SumFixedExponent", -30.0, "SumSlope",
    9.3132257461547852E-10, "SumSlopeAdjustmentFactor", 1.0, "SumBias", 0.0,
    "CastBeforeSum", true, &emlrtMCI));
  qpsk_rx_fixpt_init();
  f108_qpsk_rx_correlator_init();
  f23_qpsk_rx_foc_init();
  f55_qpsk_rx_srrc_init();
  f58_qpsk_rx_toc_init();
}

void qpsk_rx_fixpt_initialize(emlrtContext *aContext)
{
  emlrtStack st = { NULL, NULL, NULL };

  emlrtBreakCheckR2012bFlagVar = emlrtGetBreakCheckFlagAddressR2012b();
  emlrtCreateRootTLS(&emlrtRootTLSGlobal, aContext, NULL, 1);
  st.tls = emlrtRootTLSGlobal;
  emlrtClearAllocCountR2012b(&st, false, 3U, "ForceOff");
  emlrtEnterRtStackR2012b(&st);
  if (emlrtFirstTimeR2012b(emlrtRootTLSGlobal)) {
    qpsk_rx_fixpt_once(&st);
  }
}

/* End of code generation (qpsk_rx_fixpt_initialize.c) */
