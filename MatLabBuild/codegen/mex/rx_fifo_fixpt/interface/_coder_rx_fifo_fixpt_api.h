/*
 * _coder_rx_fifo_fixpt_api.h
 *
 * Code generation for function '_coder_rx_fifo_fixpt_api'
 *
 */

#ifndef ___CODER_RX_FIFO_FIXPT_API_H__
#define ___CODER_RX_FIFO_FIXPT_API_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "rx_fifo_fixpt_types.h"

/* Function Declarations */
extern void rx_fifo_fixpt_api(const mxArray * const prhs[4], const mxArray *
  plhs[6]);

#endif

/* End of code generation (_coder_rx_fifo_fixpt_api.h) */
