/*
 * _coder_rx_fifo_fixpt_mex.c
 *
 * Code generation for function 'rx_fifo_fixpt'
 *
 */

/* Include files */
#include "mex.h"
#include "_coder_rx_fifo_fixpt_api.h"
#include "rx_fifo_fixpt_initialize.h"
#include "rx_fifo_fixpt_terminate.h"

/* Function Declarations */
static void rx_fifo_fixpt_mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);

/* Variable Definitions */
emlrtContext emlrtContextGlobal = { true, false, EMLRT_VERSION_INFO, NULL, "rx_fifo_fixpt", NULL, false, {2045744189U,2170104910U,2743257031U,4284093946U}, NULL };
void *emlrtRootTLSGlobal = NULL;

/* Function Definitions */
static void rx_fifo_fixpt_mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  const mxArray *outputs[6];
  const mxArray *inputs[4];
  int n = 0;
  int nOutputs = (nlhs < 1 ? 1 : nlhs);
  int nInputs = nrhs;
  emlrtStack st = { NULL, NULL, NULL };
  /* Module initialization. */
  rx_fifo_fixpt_initialize(&emlrtContextGlobal);
  st.tls = emlrtRootTLSGlobal;
  /* Check for proper number of arguments. */
  if (nrhs != 4) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:WrongNumberOfInputs", 5, mxINT32_CLASS, 4, mxCHAR_CLASS, 13, "rx_fifo_fixpt");
  } else if (nlhs > 6) {
    emlrtErrMsgIdAndTxt(&st, "EMLRT:runTime:TooManyOutputArguments", 3, mxCHAR_CLASS, 13, "rx_fifo_fixpt");
  }
  /* Temporary copy for mex inputs. */
  for (n = 0; n < nInputs; ++n) {
    inputs[n] = prhs[n];
  }
  /* Call the function. */
  rx_fifo_fixpt_api(inputs, outputs);
  /* Copy over outputs to the caller. */
  for (n = 0; n < nOutputs; ++n) {
    plhs[n] = emlrtReturnArrayR2009a(outputs[n]);
  }
  /* Module finalization. */
  rx_fifo_fixpt_terminate();
}

void rx_fifo_fixpt_atexit_wrapper(void)
{
   rx_fifo_fixpt_atexit();
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  /* Initialize the memory manager. */
  mexAtExit(rx_fifo_fixpt_atexit_wrapper);
  /* Dispatch the entry-point. */
  rx_fifo_fixpt_mexFunction(nlhs, plhs, nrhs, prhs);
}
/* End of code generation (_coder_rx_fifo_fixpt_mex.c) */
