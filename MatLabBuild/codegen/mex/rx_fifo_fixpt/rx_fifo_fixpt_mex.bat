@echo off
set MATLAB=C:\PROGRA~1\MATLAB\2014a
set MATLAB_ARCH=win64
set MATLAB_BIN="C:\Program Files\MATLAB\2014a\bin"
set ENTRYPOINT=mexFunction
set OUTDIR=.\
set LIB_NAME=rx_fifo_fixpt_mex
set MEX_NAME=rx_fifo_fixpt_mex
set MEX_EXT=.mexw64
call "C:\PROGRA~1\MATLAB\2014a\sys\lcc64\lcc64\mex\lcc64opts.bat"
echo # Make settings for rx_fifo_fixpt > rx_fifo_fixpt_mex.mki
echo COMPILER=%COMPILER%>> rx_fifo_fixpt_mex.mki
echo COMPFLAGS=%COMPFLAGS%>> rx_fifo_fixpt_mex.mki
echo OPTIMFLAGS=%OPTIMFLAGS%>> rx_fifo_fixpt_mex.mki
echo DEBUGFLAGS=%DEBUGFLAGS%>> rx_fifo_fixpt_mex.mki
echo LINKER=%LINKER%>> rx_fifo_fixpt_mex.mki
echo LINKFLAGS=%LINKFLAGS%>> rx_fifo_fixpt_mex.mki
echo LINKOPTIMFLAGS=%LINKOPTIMFLAGS%>> rx_fifo_fixpt_mex.mki
echo LINKDEBUGFLAGS=%LINKDEBUGFLAGS%>> rx_fifo_fixpt_mex.mki
echo MATLAB_ARCH=%MATLAB_ARCH%>> rx_fifo_fixpt_mex.mki
echo BORLAND=%BORLAND%>> rx_fifo_fixpt_mex.mki
echo OMPFLAGS= >> rx_fifo_fixpt_mex.mki
echo OMPLINKFLAGS= >> rx_fifo_fixpt_mex.mki
echo EMC_COMPILER=lcc64>> rx_fifo_fixpt_mex.mki
echo EMC_CONFIG=optim>> rx_fifo_fixpt_mex.mki
"C:\Program Files\MATLAB\2014a\bin\win64\gmake" -B -f rx_fifo_fixpt_mex.mk
