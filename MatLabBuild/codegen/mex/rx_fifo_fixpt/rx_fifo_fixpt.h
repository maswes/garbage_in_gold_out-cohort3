/*
 * rx_fifo_fixpt.h
 *
 * Code generation for function 'rx_fifo_fixpt'
 *
 */

#ifndef __RX_FIFO_FIXPT_H__
#define __RX_FIFO_FIXPT_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "rx_fifo_fixpt_types.h"

/* Function Declarations */
extern void byte_out_not_empty_init(void);
extern void handshake_not_empty_init(void);
extern void head_not_empty_init(void);
extern void rx_fifo_fixpt(const emlrtStack *sp, uint8_T reset_fifo, uint8_T
  store_byte, uint8_T byte_in, uint8_T get_byte, uint32_T *dout, uint8_T
  *bytes_available, uint8_T *byte_ready, uint8_T *empty, uint8_T *full, uint8_T *
  d_1);
extern void rx_fifo_fixpt_init(void);
extern void sb_handshake_not_empty_init(void);
extern void tail_not_empty_init(void);

#endif

/* End of code generation (rx_fifo_fixpt.h) */
