/*
 * tx_fifo_fixpt.h
 *
 * Code generation for function 'tx_fifo_fixpt'
 *
 */

#ifndef __TX_FIFO_FIXPT_H__
#define __TX_FIFO_FIXPT_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "tx_fifo_fixpt_types.h"

/* Function Declarations */
extern void byte_out_not_empty_init(void);
extern void handshake_not_empty_init(void);
extern void head_not_empty_init(void);
extern void tail_not_empty_init(void);
extern void tx_fifo_fixpt(const emlrtStack *sp, uint8_T reset_fifo, uint8_T
  store_byte, uint8_T byte_in, uint8_T get_byte, uint8_T *dout, uint16_T
  *bytes_available, uint8_T *byte_received, uint8_T *empty);
extern void tx_fifo_fixpt_init(void);

#endif

/* End of code generation (tx_fifo_fixpt.h) */
