@echo off
set MATLAB=C:\PROGRA~1\MATLAB\2014a
set MATLAB_ARCH=win64
set MATLAB_BIN="C:\Program Files\MATLAB\2014a\bin"
set ENTRYPOINT=mexFunction
set OUTDIR=.\
set LIB_NAME=tx_fifo_fixpt_mex
set MEX_NAME=tx_fifo_fixpt_mex
set MEX_EXT=.mexw64
call "C:\PROGRA~1\MATLAB\2014a\sys\lcc64\lcc64\mex\lcc64opts.bat"
echo # Make settings for tx_fifo_fixpt > tx_fifo_fixpt_mex.mki
echo COMPILER=%COMPILER%>> tx_fifo_fixpt_mex.mki
echo COMPFLAGS=%COMPFLAGS%>> tx_fifo_fixpt_mex.mki
echo OPTIMFLAGS=%OPTIMFLAGS%>> tx_fifo_fixpt_mex.mki
echo DEBUGFLAGS=%DEBUGFLAGS%>> tx_fifo_fixpt_mex.mki
echo LINKER=%LINKER%>> tx_fifo_fixpt_mex.mki
echo LINKFLAGS=%LINKFLAGS%>> tx_fifo_fixpt_mex.mki
echo LINKOPTIMFLAGS=%LINKOPTIMFLAGS%>> tx_fifo_fixpt_mex.mki
echo LINKDEBUGFLAGS=%LINKDEBUGFLAGS%>> tx_fifo_fixpt_mex.mki
echo MATLAB_ARCH=%MATLAB_ARCH%>> tx_fifo_fixpt_mex.mki
echo BORLAND=%BORLAND%>> tx_fifo_fixpt_mex.mki
echo OMPFLAGS= >> tx_fifo_fixpt_mex.mki
echo OMPLINKFLAGS= >> tx_fifo_fixpt_mex.mki
echo EMC_COMPILER=lcc64>> tx_fifo_fixpt_mex.mki
echo EMC_CONFIG=optim>> tx_fifo_fixpt_mex.mki
"C:\Program Files\MATLAB\2014a\bin\win64\gmake" -B -f tx_fifo_fixpt_mex.mk
