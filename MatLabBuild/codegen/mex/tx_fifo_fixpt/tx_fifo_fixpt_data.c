/*
 * tx_fifo_fixpt_data.c
 *
 * Code generation for function 'tx_fifo_fixpt_data'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "tx_fifo_fixpt.h"
#include "tx_fifo_fixpt_data.h"

/* Variable Definitions */
const mxArray *eml_mx;
const mxArray *b_eml_mx;
const mxArray *c_eml_mx;
const mxArray *d_eml_mx;

/* End of code generation (tx_fifo_fixpt_data.c) */
