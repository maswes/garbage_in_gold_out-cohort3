/*
 * tx_fifo_fixpt_initialize.h
 *
 * Code generation for function 'tx_fifo_fixpt_initialize'
 *
 */

#ifndef __TX_FIFO_FIXPT_INITIALIZE_H__
#define __TX_FIFO_FIXPT_INITIALIZE_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "tx_fifo_fixpt_types.h"

/* Function Declarations */
extern void tx_fifo_fixpt_initialize(emlrtContext *aContext);

#endif

/* End of code generation (tx_fifo_fixpt_initialize.h) */
