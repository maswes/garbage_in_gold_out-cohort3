/*
 * tx_fifo_fixpt_data.h
 *
 * Code generation for function 'tx_fifo_fixpt_data'
 *
 */

#ifndef __TX_FIFO_FIXPT_DATA_H__
#define __TX_FIFO_FIXPT_DATA_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "tx_fifo_fixpt_types.h"

/* Variable Declarations */
extern const mxArray *eml_mx;
extern const mxArray *b_eml_mx;
extern const mxArray *c_eml_mx;
extern const mxArray *d_eml_mx;

#endif

/* End of code generation (tx_fifo_fixpt_data.h) */
