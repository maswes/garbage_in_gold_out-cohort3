/*
 * _coder_tx_fifo_fixpt_info.h
 *
 * Code generation for function 'tx_fifo_fixpt'
 *
 */

#ifndef ___CODER_TX_FIFO_FIXPT_INFO_H__
#define ___CODER_TX_FIFO_FIXPT_INFO_H__
/* Include files */
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"


/* Function Declarations */
extern const mxArray *emlrtMexFcnResolvedFunctionsInfo(void);

#endif
/* End of code generation (_coder_tx_fifo_fixpt_info.h) */
