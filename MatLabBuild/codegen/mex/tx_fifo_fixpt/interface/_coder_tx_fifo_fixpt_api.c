/*
 * _coder_tx_fifo_fixpt_api.c
 *
 * Code generation for function '_coder_tx_fifo_fixpt_api'
 *
 */

/* Include files */
#include "rt_nonfinite.h"
#include "tx_fifo_fixpt.h"
#include "_coder_tx_fifo_fixpt_api.h"
#include "tx_fifo_fixpt_data.h"

/* Function Declarations */
static uint8_T b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static const mxArray *b_emlrt_marshallOut(const emlrtStack *sp, const uint16_T u);
static uint8_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *byte_in,
  const char_T *identifier);
static const mxArray *c_emlrt_marshallOut(const emlrtStack *sp, const uint8_T u);
static uint8_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId);
static uint8_T e_emlrt_marshallIn(const mxArray *src);
static uint8_T emlrt_marshallIn(const emlrtStack *sp, const mxArray *reset_fifo,
  const char_T *identifier);
static const mxArray *emlrt_marshallOut(const emlrtStack *sp, const uint8_T u);
static uint8_T f_emlrt_marshallIn(const mxArray *src);

/* Function Definitions */
static uint8_T b_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  uint8_T y;
  emlrtCheckFiR2012b(sp, parentId, u, false, 0U, 0, eml_mx, b_eml_mx);
  y = e_emlrt_marshallIn(emlrtAlias(u));
  emlrtDestroyArray(&u);
  return y;
}

static const mxArray *b_emlrt_marshallOut(const emlrtStack *sp, const uint16_T u)
{
  const mxArray *y;
  const mxArray *b_y;
  const mxArray *m1;
  y = NULL;
  b_y = NULL;
  m1 = emlrtCreateNumericMatrix(1, 1, mxUINT16_CLASS, mxREAL);
  *(uint16_T *)mxGetData(m1) = u;
  emlrtAssign(&b_y, m1);
  emlrtAssign(&y, emlrtCreateFIR2013b(sp, eml_mx, d_eml_mx, "simulinkarray", b_y,
    true, false));
  return y;
}

static uint8_T c_emlrt_marshallIn(const emlrtStack *sp, const mxArray *byte_in,
  const char_T *identifier)
{
  uint8_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = d_emlrt_marshallIn(sp, emlrtAlias(byte_in), &thisId);
  emlrtDestroyArray(&byte_in);
  return y;
}

static const mxArray *c_emlrt_marshallOut(const emlrtStack *sp, const uint8_T u)
{
  const mxArray *y;
  const mxArray *b_y;
  const mxArray *m2;
  y = NULL;
  b_y = NULL;
  m2 = emlrtCreateNumericMatrix(1, 1, mxUINT8_CLASS, mxREAL);
  *(uint8_T *)mxGetData(m2) = u;
  emlrtAssign(&b_y, m2);
  emlrtAssign(&y, emlrtCreateFIR2013b(sp, eml_mx, b_eml_mx, "simulinkarray", b_y,
    true, false));
  return y;
}

static uint8_T d_emlrt_marshallIn(const emlrtStack *sp, const mxArray *u, const
  emlrtMsgIdentifier *parentId)
{
  uint8_T y;
  emlrtCheckFiR2012b(sp, parentId, u, false, 0U, 0, eml_mx, c_eml_mx);
  y = f_emlrt_marshallIn(emlrtAlias(u));
  emlrtDestroyArray(&u);
  return y;
}

static uint8_T e_emlrt_marshallIn(const mxArray *src)
{
  uint8_T ret;
  const mxArray *mxInt;
  mxInt = emlrtImportFiIntArrayR2008b(src);
  ret = *(uint8_T *)mxGetData(mxInt);
  emlrtDestroyArray(&mxInt);
  emlrtDestroyArray(&src);
  return ret;
}

static uint8_T emlrt_marshallIn(const emlrtStack *sp, const mxArray *reset_fifo,
  const char_T *identifier)
{
  uint8_T y;
  emlrtMsgIdentifier thisId;
  thisId.fIdentifier = identifier;
  thisId.fParent = NULL;
  y = b_emlrt_marshallIn(sp, emlrtAlias(reset_fifo), &thisId);
  emlrtDestroyArray(&reset_fifo);
  return y;
}

static const mxArray *emlrt_marshallOut(const emlrtStack *sp, const uint8_T u)
{
  const mxArray *y;
  const mxArray *b_y;
  const mxArray *m0;
  y = NULL;
  b_y = NULL;
  m0 = emlrtCreateNumericMatrix(1, 1, mxUINT8_CLASS, mxREAL);
  *(uint8_T *)mxGetData(m0) = u;
  emlrtAssign(&b_y, m0);
  emlrtAssign(&y, emlrtCreateFIR2013b(sp, eml_mx, c_eml_mx, "simulinkarray", b_y,
    true, false));
  return y;
}

static uint8_T f_emlrt_marshallIn(const mxArray *src)
{
  uint8_T ret;
  const mxArray *mxInt;
  mxInt = emlrtImportFiIntArrayR2008b(src);
  ret = *(uint8_T *)mxGetData(mxInt);
  emlrtDestroyArray(&mxInt);
  emlrtDestroyArray(&src);
  return ret;
}

void tx_fifo_fixpt_api(const mxArray * const prhs[4], const mxArray *plhs[4])
{
  uint8_T reset_fifo;
  uint8_T store_byte;
  uint8_T byte_in;
  uint8_T get_byte;
  uint8_T empty;
  uint8_T byte_received;
  uint16_T bytes_available;
  uint8_T dout;
  emlrtStack st = { NULL, NULL, NULL };

  st.tls = emlrtRootTLSGlobal;

  /* Marshall function inputs */
  reset_fifo = emlrt_marshallIn(&st, emlrtAliasP(prhs[0]), "reset_fifo");
  store_byte = emlrt_marshallIn(&st, emlrtAliasP(prhs[1]), "store_byte");
  byte_in = c_emlrt_marshallIn(&st, emlrtAliasP(prhs[2]), "byte_in");
  get_byte = emlrt_marshallIn(&st, emlrtAliasP(prhs[3]), "get_byte");

  /* Invoke the target function */
  tx_fifo_fixpt(&st, reset_fifo, store_byte, byte_in, get_byte, &dout,
                &bytes_available, &byte_received, &empty);

  /* Marshall function outputs */
  plhs[0] = emlrt_marshallOut(&st, dout);
  plhs[1] = b_emlrt_marshallOut(&st, bytes_available);
  plhs[2] = c_emlrt_marshallOut(&st, byte_received);
  plhs[3] = c_emlrt_marshallOut(&st, empty);
}

/* End of code generation (_coder_tx_fifo_fixpt_api.c) */
