/*
 * _coder_tx_fifo_fixpt_api.h
 *
 * Code generation for function '_coder_tx_fifo_fixpt_api'
 *
 */

#ifndef ___CODER_TX_FIFO_FIXPT_API_H__
#define ___CODER_TX_FIFO_FIXPT_API_H__

/* Include files */
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "tmwtypes.h"
#include "mex.h"
#include "emlrt.h"
#include "blas.h"
#include "rtwtypes.h"
#include "tx_fifo_fixpt_types.h"

/* Function Declarations */
extern void tx_fifo_fixpt_api(const mxArray * const prhs[4], const mxArray *
  plhs[4]);

#endif

/* End of code generation (_coder_tx_fifo_fixpt_api.h) */
