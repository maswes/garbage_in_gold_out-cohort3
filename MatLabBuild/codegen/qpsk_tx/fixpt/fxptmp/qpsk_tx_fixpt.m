%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                          %
%       Generated by MATLAB 8.3, MATLAB Coder 2.6 and HDL Coder 3.4        %
%                                                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%#codegen
function [i_out,q_out,tx_done_out,request_byte,blinky] = qpsk_tx_fixpt(data_in,empty_in,clear_fifo_in,tx_en_in)

fm = fimath('RoundingMethod', 'Floor', 'OverflowAction', 'Wrap', 'ProductMode', 'FullPrecision', 'MaxProductWordLength', 128, 'SumMode', 'FullPrecision', 'MaxSumWordLength', 128);
persistent blinky_cnt
if isempty( blinky_cnt )
    blinky_cnt = fi(0, 0, 25, 0, fm);
end
[fmo_1,fmo_2,fmo_3,fmo_4] = f65_qpsk_tx_byte2sym( data_in, empty_in, clear_fifo_in, tx_en_in );
byte_i_out = fi(fmo_1, 1, 2, 0, fm);
byte_q_out = fi(fmo_2, 1, 2, 0, fm);
request_byte = fi(fmo_3, 0, 1, 0, fm);
tx_done = fi(fmo_4, 0, 1, 0, fm);
byte_out = fi(complex( byte_i_out, byte_q_out ), 1, 2, 0, fm);
[d_ssrc] = fi(f72_qpsk_srrc( byte_out ), 1, 13, 12, fm);
% make i/q discrete ports and scale to the full 12-bit range of the DAC
% (one bit is for sign)
i_out = fi(round( real( d_ssrc )*fi(2^11, 0, 12, 0, fm) ), 1, 12, 0, fm);
q_out = fi(round( imag( d_ssrc )*fi(2^11, 0, 12, 0, fm) ), 1, 12, 0, fm);
tx_done_out = fi(tx_done, 0, 1, 0, fm);
blinky_cnt(:) = blinky_cnt + fi(1, 0, 1, 0, fm);
if blinky_cnt==fi(20000000, 0, 25, 0, fm)
    blinky_cnt(:) = 0;
end
blinky = fi(floor( blinky_cnt*fi(1/10000000, 0, 14, 37, fm) ), 0, 1, 0, fm);
end
function y = f2_TB_i

fm = fimath('RoundingMethod', 'Floor', 'OverflowAction', 'Wrap', 'ProductMode', 'FullPrecision', 'MaxProductWordLength', 128, 'SumMode', 'FullPrecision', 'MaxSumWordLength', 128);
%#codegen
y = fi([ 1; 1; -1; -1; 1; 1; -1; -1; 1; -1; -1; -1; -1; 1; -1; 1; 1; -1; -1; 1; 1; 1; -1; 1; 1; 1; -1; 1; -1; 1; 1; 1; -1; 1; 1; 1; 1; -1; 1; 1; -1; -1; -1; -1; 1; 1; 1; -1; -1; -1; 1; 1; 1; 1; -1; 1; 1; 1; 1; 1; -1; -1; -1; 1; 1 ], 1, 2, 0, fm);
end
function y = f3_TB_q

fm = fimath('RoundingMethod', 'Floor', 'OverflowAction', 'Wrap', 'ProductMode', 'FullPrecision', 'MaxProductWordLength', 128, 'SumMode', 'FullPrecision', 'MaxSumWordLength', 128);
%#codegen
y = fi([ 1; -1; 1; 1; 1; -1; -1; -1; -1; 1; 1; -1; 1; 1; -1; -1; -1; 1; -1; -1; 1; 1; 1; -1; -1; 1; 1; 1; -1; -1; -1; -1; -1; -1; 1; 1; -1; 1; -1; 1; 1; -1; -1; 1; -1; -1; 1; 1; -1; -1; -1; -1; -1; 1; 1; 1; 1; -1; -1; -1; -1; 1; -1; 1; -1 ], 1, 2, 0, fm);
end
function b = f64_mybitget(by,p)

fm = fimath('RoundingMethod', 'Floor', 'OverflowAction', 'Wrap', 'ProductMode', 'FullPrecision', 'MaxProductWordLength', 128, 'SumMode', 'FullPrecision', 'MaxSumWordLength', 128);
switch p
    case fi(1, 0, 1, 0, fm)
        u = fi(floor( fi_div_by_shift(by, 0) ), 0, 8, 0, fm);
    case fi(2, 0, 2, 0, fm)
        u = fi(floor( fi_div_by_shift(by, 1) ), 0, 8, 0, fm);
    case fi(3, 0, 2, 0, fm)
        u = fi(floor( fi_div_by_shift(by, 2) ), 0, 8, 0, fm);
    case fi(4, 0, 3, 0, fm)
        u = fi(floor( fi_div_by_shift(by, 3) ), 0, 8, 0, fm);
    case fi(5, 0, 3, 0, fm)
        u = fi(floor( fi_div_by_shift(by, 4) ), 0, 8, 0, fm);
    case fi(6, 0, 3, 0, fm)
        u = fi(floor( fi_div_by_shift(by, 5) ), 0, 8, 0, fm);
    case fi(7, 0, 3, 0, fm)
        u = fi(floor( fi_div_by_shift(by, 6) ), 0, 8, 0, fm);
    case fi(8, 0, 4, 0, fm)
        u = fi(floor( fi_div_by_shift(by, 7) ), 0, 8, 0, fm);
    otherwise
        u = fi(0, 0, 8, 0, fm);
end
b = fi(mod( u, fi(2, 0, 2, 0, fm) ), 0, 1, 0, fm);
end
%#codegen
% this core runs at an oversampling rate of 8
function [d_i_out,d_q_out,re_byte_out,tx_done_out] = f65_qpsk_tx_byte2sym(data_in,empty_in,clear_fifo_in,tx_en_in)

fm = fimath('RoundingMethod', 'Floor', 'OverflowAction', 'Wrap', 'ProductMode', 'FullPrecision', 'MaxProductWordLength', 128, 'SumMode', 'FullPrecision', 'MaxSumWordLength', 128);
OS_RATE = fi(8, 0, 4, 0, fm);
SYM_PER_BYTE = fi(4, 0, 3, 0, fm);  % number of symbols per byte (QPSK 4)
tbi = fi(f2_TB_i, 1, 2, 0, fm);
tbq = fi(f3_TB_q, 1, 2, 0, fm);
CORE_LATENCY = fi(8, 0, 4, 0, fm);
persistent count
persistent symIndex
persistent diLatch dqLatch
persistent tx_fifo
persistent wrCount rdCount
persistent txDone
persistent sentTrain
persistent reBuf
if isempty( count )
    count = fi(0, 0, 6, 0, fm);
    symIndex = fi(0, 0, 3, 0, fm);
    diLatch = fi(0, 1, 2, 0, fm);
    dqLatch = fi(0, 1, 2, 0, fm);
    wrCount = fi(0, 0, 11, 0, fm);
    rdCount = fi(0, 0, 11, 0, fm);
    txDone = fi(0, 0, 1, 0, fm);
    sentTrain = fi(0, 0, 8, 0, fm);
    reBuf = fi(0, 0, 4, 0, fm);
end
if isempty( tx_fifo )
    tx_fifo = fi(zeros( 1, 1024 ), 0, 8, 0, fm);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% if want to transmit a new packet reset things
if clear_fifo_in==fi(1, 0, 1, 0, fm)
    wrCount(:) = 0;
    txDone(:) = 0;
    reBuf(:) = 0;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% we are ready to transmit some data
rdIndex = fi(wrCount - rdCount + fi(1, 0, 1, 0, fm), 0, 11, 0, fm);
if rdIndex<=fi(0, 0, 1, 0, fm)
    rdIndex(:) = 1024;
end
data = fi(tx_fifo( rdIndex ), 0, 8, 0, fm);
d_i_out = fi(0, 1, 2, 0, fm);
d_q_out = fi(0, 1, 2, 0, fm);
% if the processor says transmit, start sending what is in the buffer.
% we stop when we've written all the data out that we wrote to the fifo.
% This core doesn't care about packet length, just about how many bytes got
% written to the fifo.
PAD_BITS = fi(64, 0, 7, 0, fm);
if empty_in==fi(1, 0, 1, 0, fm) && tx_en_in==fi(1, 0, 1, 0, fm) && txDone==fi(0, 0, 1, 0, fm)
    if sentTrain<=PAD_BITS
        if count==fi(0, 0, 1, 0, fm)
            diLatch(:) = fi_signed(mod( sentTrain, fi(2, 0, 2, 0, fm) )*fi(2, 0, 2, 0, fm)) - fi(1, 0, 1, 0, fm);
            dqLatch(:) = fi_signed(mod( sentTrain, fi(2, 0, 2, 0, fm) )*fi(2, 0, 2, 0, fm)) - fi(1, 0, 1, 0, fm);
        end
        count(:) = count + fi(1, 0, 1, 0, fm);
        if count>=OS_RATE
            count(:) = 0;
            sentTrain(:) = sentTrain + fi(1, 0, 1, 0, fm);
        end
        d_i_out(:) = diLatch;
        d_q_out(:) = dqLatch;
    elseif sentTrain<=fi(65, 0, 7, 0, fm) + PAD_BITS
        if count==fi(0, 0, 1, 0, fm)
            diLatch(:) = tbi( sentTrain - PAD_BITS );
            dqLatch(:) = tbq( sentTrain - PAD_BITS );
        end
        count(:) = count + fi(1, 0, 1, 0, fm);
        if count>=OS_RATE
            count(:) = 0;
            sentTrain(:) = sentTrain + fi(1, 0, 1, 0, fm);
        end
        d_i_out(:) = diLatch;
        d_q_out(:) = dqLatch;
    else
        if mod( count, OS_RATE )==fi(0, 0, 1, 0, fm)
            sym2 = fi(symIndex*fi(2, 0, 2, 0, fm), 0, 3, 0, fm);
            diLatch(:) = fi_signed(f64_mybitget( data, sym2 + fi(1, 0, 1, 0, fm) )*fi(2, 0, 2, 0, fm)) - fi(1, 0, 1, 0, fm);
            dqLatch(:) = fi_signed(f64_mybitget( data, sym2 + fi(2, 0, 2, 0, fm) )*fi(2, 0, 2, 0, fm)) - fi(1, 0, 1, 0, fm);
            symIndex(:) = symIndex + fi(1, 0, 1, 0, fm);
        end
        d_i_out(:) = diLatch;
        d_q_out(:) = dqLatch;
        count(:) = count + fi(1, 0, 1, 0, fm);
        if count>=OS_RATE*SYM_PER_BYTE
            count(:) = 0;
            symIndex(:) = 0;
            rdCount(:) = rdCount - fi(1, 0, 1, 0, fm);
        end
        if rdCount==fi(0, 0, 1, 0, fm)
            txDone(:) = 1;
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% transfer data from processor to internal buffer
% Because the core has a non-zero throughput we need to stale a bit for the
% requested data to make it to our input. So, I'm doing that we reBuf
% counter. There are definitely more efficient ways to do this but I'm
% gonna leave that for another day.
wrIndex = fi(1024, 0, 11, 0, fm);
re_byte_out = fi(0, 0, 1, 0, fm);
if empty_in==fi(0, 0, 1, 0, fm) && reBuf==fi(0, 0, 1, 0, fm)
    reBuf(:) = CORE_LATENCY;
    txDone(:) = 0;
    re_byte_out(:) = 1;
end
if reBuf>fi(0, 0, 1, 0, fm)
    reBuf(:) = reBuf - fi(1, 0, 1, 0, fm);
end
if reBuf==fi(1, 0, 1, 0, fm)
    wrCount(:) = wrCount + fi(1, 0, 1, 0, fm);  %total number of bytes to send out
    wrIndex(:) = wrCount;
    rdCount(:) = wrCount;
    reBuf(:) = 0;
    count(:) = 0;
    sentTrain(:) = 1;
end
tx_fifo( wrIndex ) = data_in;
tx_done_out = fi(txDone, 0, 1, 0, fm);
end
function y = f66_SRRC

fm = fimath('RoundingMethod', 'Floor', 'OverflowAction', 'Wrap', 'ProductMode', 'FullPrecision', 'MaxProductWordLength', 128, 'SumMode', 'FullPrecision', 'MaxSumWordLength', 128);
%#codegen
y = fi([ 0.006690562067; -0.005768593810; -0.011827354519; 0.020586915021; 0.016726405168; -0.053692342207; -0.020253031546; 0.196043589067; 0.336822413190; 0.196043589067; -0.020253031546; -0.053692342207; 0.016726405168; 0.020586915021; -0.011827354519; -0.005768593810; 0.006690562067 ], 1, 14, 14, fm);
end
function d_out = f72_qpsk_srrc(d_in)

fm = fimath('RoundingMethod', 'Floor', 'OverflowAction', 'Wrap', 'ProductMode', 'FullPrecision', 'MaxProductWordLength', 128, 'SumMode', 'FullPrecision', 'MaxSumWordLength', 128);
persistent buf
OS_RATE = fi(8, 0, 4, 0, fm);
f = fi(f66_SRRC, 1, 14, 14, fm);
if isempty( buf )
    buf = fi(complex( zeros( 1, fi_toint(OS_RATE*fi(2, 0, 2, 0, fm) + fi(1, 0, 1, 0, fm)) ), zeros( 1, fi_toint(OS_RATE*fi(2, 0, 2, 0, fm) + fi(1, 0, 1, 0, fm)) ) ), 1, 2, 0, fm);
end
buf(:) = [ fi(buf( 2:end ), 1, 2, 0, fm), d_in ];
d_out = fi(buf*f, 1, 14, 13, fm);
end


function y = fi_div_by_shift(a,shift_len)
coder.inline( 'always' );
if isfi( a )
    nt = numerictype( a );
    fm = fimath( a );
    nt_bs = numerictype( nt.Signed, nt.WordLength + shift_len, nt.FractionLength + shift_len );
    y = bitsrl( fi( a, nt_bs, fm ), shift_len );
else
    y = a/2^shift_len;
end
end


function y = fi_signed(a)
coder.inline( 'always' );
if isfi( a ) && ~(issigned( a ))
    nt = numerictype( a );
    new_nt = numerictype( 1, nt.WordLength + 1, nt.FractionLength );
    y = fi( a, new_nt, fimath( a ) );
else
    y = a;
end
end


function y = fi_toint(u)
coder.inline( 'always' );
if isfi( u )
    nt = numerictype( u );
    s = nt.SignednessBool;
    wl = nt.WordLength;
    y = int32( fi( u, s, wl, 0, hdlfimath ) );
else
    y = int32( u );
end
end
