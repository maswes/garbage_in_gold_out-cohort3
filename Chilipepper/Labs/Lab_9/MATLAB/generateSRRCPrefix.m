tbi = TB_i;
tbq = TB_q;
res_real = [];
res_imag = [];
tmp = 0;
for iter3=1:2
    for iter=1:length(TB_i)
        for iter2=1:8
            tmp=qpsk_srrc(complex(tbi(iter),tbq(iter)));
            [res_real(1,end+1) res_imag(1,end+1)] = qpsk_rx_srrc(real(tmp), imag(tmp));
        end
    end
end