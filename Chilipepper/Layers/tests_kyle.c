/*
 * tests_kyle.c
 */

#include <string.h>
#include "debug.h"
#ifdef XILINX
#include "xil_printf.h"
#endif
#include "phy_layer.h"
#include "fec_layer.h"
#include "link_layer.h"
#include "trans_layer.h"
#include "stream_layer.h"

int rs_example (int npar);

unsigned char buff_in[1024];
unsigned char buff_out[1024];
/*
void populate_buff( unsigned char *buff, const int len )
{
   int i;
   for ( i = 0; i < len; ++i )
      buff[i] = i + 111;
}
*/

#if 0
int test_phy( int length )
{
   int i, len;
   populate_buff( buff_in, length );
   len = send_phy( buff_in, length );
   if ( len < length )
      DEBUG( "%s: only sent %d of %d.\n", __func__, len, length );

   len = recv_phy( buff_out );

   for ( i = 0; i < len; ++i )
      if ( buff_in[i] != buff_out[i] )
      {
         xil_printf( "%s: Failed for length %d\n", __func__, length );
         return -1;
      }

   return 0;
}

int test_fec( int length )
{
   int i, len;
   populate_buff( buff_in, length );

   xil_printf( "\r\n%s: Send buffer", __func__ );
   for ( i = 0; i < length; ++i )
   {
      if ( i % 16 == 0 )
         xil_printf( "\r\n\t" );
      xil_printf( "0x%02x ", buff_in[i] );
   }
   xil_printf( "\r\n" );

   len = send_fec( buff_in, length );
   if ( len < length )
      xil_printf( "%s: only sent %d of %d.\n", __func__, len, length );

   len = recv_fec( buff_out );

   xil_printf( "%s: Recv buffer", __func__ );
   for ( i = 0; i < len; ++i )
   {
      if ( i % 16 == 0 )
         xil_printf( "\r\n\t" );
      xil_printf( "0x%02x ", buff_out[i] );
   }
   xil_printf( "\r\n" );

   for ( i = 0; i < len; ++i )
      if ( buff_in[i] != buff_out[i] )
      {
         xil_printf( "%s: Failed for length %d at %d: 0x%02x != 0x%02x\n", __func__, length, i, buff_in[i], buff_out[i] );
         return -1;
      }

   return 0;
}
#endif

int tests_kyle( void )
{
#if 0
   xil_printf( "\r\n\r\nTesting PHY Loopback\r\n" );
   set_phy( PHY_LOOPBACK );
   test_phy( 0 );
   test_phy( 1 );
   test_phy( 2 );
   test_phy( 3 );
   test_phy( 512 );
   test_phy( 513 );
   test_phy( 1024 );
   test_phy( 1025 );

   xil_printf( "\r\n\r\nTesting FEC Hamming\r\n" );
   set_phy( PHY_LOOPBACK );
   set_fec( FEC_HAMMING );
   test_fec( 0 );
   test_fec( 1 );
   test_fec( 2 );
   test_fec( 3 );
   test_fec( 16 );
   test_fec( 17 );
   test_fec( 512 );
   test_fec( 529 ); /* Fix */
#endif

   rs_example(4);

   unsigned char buff[2048];

   set_debug_level( DEBUG_ALL );

   set_phy( PHY_LOOPBACK );
   set_fec( FEC_RS );
   set_link( LINK_PASS );
   set_trans( TRANS_PASS );
   set_stream( STREAM_CHUNK );

   int size = 100;

   populate_buff( buff, size );

   print_buff( DEBUG_BASIC, INDENT_MAIN, "Send", buff, size );
   g_rssi = 78;
   send_stream( buff, size );

   while ( 1 )
   {
      size = recv_stream( buff );
      print_buff( DEBUG_BASIC, INDENT_MAIN, "Recv", buff, size );
      if ( size < STREAM_BUFF_SIZE )
         break;
   }

   return 0;
}
