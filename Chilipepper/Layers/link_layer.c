/*
 * link_layer.c
 */

#include "link_layer.h"
#include "fec_layer.h"
#include "cobs.h"
#include "debug.h"


unsigned char link_buff[LINK_BUFF_SIZE];

static int send_link_pass( unsigned char *buff, const int length )
{
   DEBUG( DEBUG_FUNC, INDENT_LINK, "%s\r\n", __func__);

   if ( length > LINK_BUFF_SIZE )
      return -1;

   return send_fec( buff, length );
}

static int recv_link_pass( unsigned char *buff )
{
   DEBUG( DEBUG_FUNC, INDENT_LINK, "%s\r\n", __func__);

   return recv_fec( buff );
}

static int send_link_cobs( unsigned char *buff, const int length )
{
   DEBUG( DEBUG_FUNC, INDENT_LINK, "%s\r\n", __func__);
   int len;

   if ( length > LINK_BUFF_SIZE )
      return -1;

   len = cobs_encode( (unsigned char *)buff, length, link_buff );
   print_buff( DEBUG_VERBOSE, INDENT_LINK, "COBS Encoded", link_buff, len );

   return send_fec( link_buff, len );
}

static int recv_link_cobs( unsigned char *buff )
{
   DEBUG( DEBUG_FUNC, INDENT_LINK, "%s\r\n", __func__);
   int fec_len, len;
   fec_len = recv_fec( link_buff );
   
   if ( fec_len < 0 )
   {
      DEBUG( DEBUG_WORDY, INDENT_LINK, "%s: %d = fec_len < 0\r\n", __func__, fec_len );
      return fec_len;
   }

   len = cobs_decode( link_buff, fec_len, buff );
   print_buff( DEBUG_VERBOSE, INDENT_LINK, "COBS Decoded", buff, len );

   return len;
}

/*
 * LINK Layer Setup
 */
int set_link( link_t link )
{
   switch ( link )
   {
      case LINK_PASS:
         send_link = send_link_pass;
         recv_link = recv_link_pass;
         break;
      case LINK_COBS:
         send_link = send_link_cobs;
         recv_link = recv_link_cobs;
         break;
      default:
         break;
   }

   return 0;
}

