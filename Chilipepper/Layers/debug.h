#ifndef _DEBUG_H_
#define _DEBUG_H_

#define DEBUG_PRINT
//#define XILINX

#ifdef XILINX
#include "xil_printf.h"
#define PRINTF( ... ) xil_printf( __VA_ARGS__ )
#else
#include <stdio.h>
#define PRINTF( ... ) printf( __VA_ARGS__ )
#endif 

#ifdef DEBUG_PRINT
//#define DEBUG(level, ...) if ( debug_levels & level ) { PRINTF(__VA_ARGS__); }
#define DEBUG( level, indent, ... ) if ( debug_levels & level ) { { int i; for ( i = 0; i < indent; ++i ) PRINTF( "\t" ); } PRINTF( __VA_ARGS__ ); }
#else
#define DEBUG( ... ) 
#endif 

typedef enum
   { DEBUG_NONE    = 0x00,
     DEBUG_ASSIGN  = 0x01,
     DEBUG_FUNC    = 0x02,
     DEBUG_BASIC   = 0x04,
     DEBUG_WORDY   = 0x08,
     DEBUG_VERBOSE = 0x10,
     DEBUG_ALL     = 0xff
   } debug_level_t;

typedef enum
   { INDENT_MAIN   = 0,
     INDENT_STREAM = 1,
     INDENT_TRANS  = 2,
     INDENT_LINK   = 3,
     INDENT_FEC    = 4,
     INDENT_PHY    = 5
   } debug_indent_t;
     

extern debug_level_t debug_levels;

void populate_buff( unsigned char *buff, const int len );
void print_buff( const debug_level_t level, const debug_indent_t indent, const char *title, const unsigned char *buff, const int length );
void set_debug_level( const debug_level_t level );

int Chilipepper_CheckCrc( unsigned char *rxBuf, int numBytes );
void Chilipepper_AppendCrc( unsigned char *buf, int msgLength );

/* TEMP */
#define CHUNK_BUFF_SIZE 256

#endif /* _DEBUG_H */
