/*
 * trans_layer.h
 */

#ifndef TRANS_LAYER_H_
#define TRANS_LAYER_H_

#include "stream_layer.h"

#define TRANS_FLAG_ADDITION 1
#define TRANS_SEQ_ADDITION 2
#define TRANS_CRC_ADDITION 2
#define TRANS_BUFF_SIZE ( STREAM_BUFF_SIZE + TRANS_FLAG_ADDITION + TRANS_SEQ_ADDITION + TRANS_CRC_ADDITION )

#define TRANS_FLAG_ACK ( 1 << 0 )

#define TRANS_ERR_CRC -1
#define TRANS_ERR_DUP -2

typedef enum { TRANS_PASS, TRANS_CRC, TRANS_STOP_N_WAIT, TRANS_INVALID } trans_t;

typedef struct
{
   unsigned bytes_sent;
   unsigned bytes_recv;
   unsigned crc_fails;
   unsigned duplicates;
   unsigned retries;
   unsigned max_retries_per_block;
} trans_stats_t;

extern trans_stats_t trans_stats;

int set_trans( trans_t fec );

int (*send_trans)( unsigned char *buff, const int length );
int (*recv_trans)( unsigned char *buff );

void trans_done( void );

#endif /* TRANS_LAYER_H_ */
