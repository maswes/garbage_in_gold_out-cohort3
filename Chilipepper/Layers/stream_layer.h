/*
 * stream_layer.h
 */

#ifndef STREAM_LAYER_H_
#define STREAM_LAYER_H_

#include "ecc.h"

#define STREAM_BUFF_SIZE (TRIPLE_HAMMING + RS_N)

typedef enum { STREAM_PASS, STREAM_CHUNK, STREAM_INVALID } stream_t;

int set_stream( stream_t fec );

int (*send_stream)( unsigned char *buff, const int length );
int (*recv_stream)( unsigned char *buff );

#endif /* STREAM_LAYER_H_ */
