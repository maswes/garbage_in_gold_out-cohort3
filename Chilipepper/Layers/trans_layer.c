/*
 * trans_layer.c
 */

#include "trans_layer.h"
#include "link_layer.h"
#include "debug.h"
#include <string.h>

#ifdef XILINX
#include "sleep.h"
#endif

trans_stats_t trans_stats;

unsigned char trans_buff[TRANS_BUFF_SIZE];
static unsigned short trans_seq;

static int send_trans_pass( unsigned char *buff, const int length )
{
   DEBUG( DEBUG_FUNC, INDENT_TRANS, "%s\r\n", __func__);

   if ( length > TRANS_BUFF_SIZE )
      return -1;

   trans_stats.bytes_sent += length;

   return send_link( buff, length );
}

static int recv_trans_pass( unsigned char *buff )
{
   DEBUG( DEBUG_FUNC, INDENT_TRANS, "%s\r\n", __func__);

   return recv_link( buff );
}

static int send_trans_crc( unsigned char *buff, const int length )
{
   DEBUG( DEBUG_FUNC, INDENT_TRANS, "%s\r\n", __func__);

   int len = length;
   memcpy( trans_buff, buff, len );
   Chilipepper_AppendCrc( trans_buff, len );
   len += 2;

   trans_stats.bytes_sent += length;

   return send_link( trans_buff, len );
}

static int recv_trans_crc( unsigned char *buff )
{
   DEBUG( DEBUG_FUNC, INDENT_TRANS, "%s\r\n", __func__);
   int len;
   len = recv_link( trans_buff );

   if ( Chilipepper_CheckCrc( trans_buff, len ) != 0 )
   {
      DEBUG( DEBUG_WORDY, INDENT_TRANS, "%s: CRC check failed\r\n", __func__ );
      trans_stats.crc_fails++;
      return TRANS_ERR_CRC;
   }

   len -= 2;
   memcpy( buff, trans_buff, len );

   trans_stats.bytes_recv += len;

   return len;
}

static int send_trans_stop_n_wait( unsigned char *buff, const int length )
{
   DEBUG( DEBUG_WORDY, INDENT_TRANS, "%s: Length = %d\r\n", __func__, length );
   int len = 0, err, pos;
   int retries = -1;
   unsigned short ack;
   unsigned char flags = 0;

   if ( length > TRANS_BUFF_SIZE )
      return -1;

   /* Increase seq to stay ahead of the receiver */
   ++trans_seq;

   /* Clear flags, add seq, and append crc */
 retry:
   if ( ++retries > 0 )
   {
      DEBUG( DEBUG_BASIC, INDENT_TRANS, "%s: Retrying seq %d\r\n", __func__, trans_seq );
      trans_stats.retries++;
      usleep( 5000 );
   }

   len = 0;
   flags = 0;
   memcpy( &trans_buff[len], &flags, sizeof( flags ) );
   len +=  sizeof( flags );
   memcpy( &trans_buff[len], &trans_seq, sizeof( trans_seq ) );
   len += sizeof( trans_seq );
   memcpy( &trans_buff[len], buff, length );
   len += length;
   Chilipepper_AppendCrc( trans_buff, len );
   len += 2;

   DEBUG( DEBUG_BASIC, INDENT_TRANS, "%s: Sent [seq %d, length %d, crc 0x%02x%02x]\r\n", __func__, trans_seq, len, trans_buff[len - 2], trans_buff[len - 1] );

   err = send_link( trans_buff, len );

   DEBUG( DEBUG_VERBOSE, INDENT_TRANS, "%s: waiting for ACK\r\n", __func__ );
   /* Wait for the ACK */
   /*
   ack_tries = 3000000;
   while ( ack_tries-- > 0 )
   {
      len = recv_link( trans_buff );
      if ( len >= 0 )
         break;
   }*/
   usleep( 10000 );
   len = recv_link( trans_buff );

   if ( len == -1 )
   {
      DEBUG( DEBUG_BASIC, INDENT_TRANS, "%s: recv_link failed\r\n", __func__ );
      goto retry;
   }

   pos = 0;
   memcpy( &flags, &trans_buff[pos], sizeof( flags ) );
   pos += sizeof( flags );
   memcpy( &ack, &trans_buff[pos], sizeof( ack ) );
   pos += sizeof( ack );

   DEBUG( DEBUG_BASIC, INDENT_TRANS, "%s: Recv [ack %d, crc 0x%02x%02x]\r\n", __func__, ack, trans_buff[len - 2], trans_buff[len - 1] );

   /* Extract flags and seq */
   if ( Chilipepper_CheckCrc( trans_buff, len ) != 0 )
   {
      DEBUG( DEBUG_BASIC, INDENT_TRANS, "%s: CRC check failed\r\n", __func__ );
      trans_stats.crc_fails++;
      goto retry;
   }
   len -= 2;
   
   if ( ( flags && TRANS_FLAG_ACK ) == 0 )
   {
      DEBUG( DEBUG_WORDY, INDENT_TRANS, "%s: ACK not set in flag 0x%01x\r\n", __func__, flags );
      goto retry;
   }

   if ( ack != trans_seq )
   {
      DEBUG( DEBUG_WORDY, INDENT_TRANS, "%s: ACK %d != %d SEQ\r\n", __func__, ack, trans_seq );
      goto retry;
   }      

   trans_stats.bytes_sent += length;

   return length;
}

static int recv_trans_stop_n_wait( unsigned char *buff )
{
   DEBUG( DEBUG_FUNC, INDENT_TRANS, "%s\r\n", __func__);
   int len, recv_len, pos;
   unsigned char flags = 0;
   unsigned short seq = 0;

   flags = 0;
   seq = 0;
   len = recv_link( trans_buff );
   
   if ( len < 0 )
   {
      DEBUG( DEBUG_WORDY, INDENT_TRANS, "%s: %d = len < 0\r\n", __func__, len );
      return len;
   }

   /* Extract flags and seq */
   pos = 0;
   memcpy( &flags, &trans_buff[pos], sizeof( flags ) );
   pos += sizeof( flags );
   memcpy( &seq, &trans_buff[pos], sizeof( seq ) );
   pos += sizeof( seq );

   DEBUG( DEBUG_BASIC, INDENT_TRANS, "%s: Recv [seq %d, length %d, crc 0x%02x%02x]\r\n", __func__, seq, len, trans_buff[len - 2], trans_buff[len - 1] );

   if ( Chilipepper_CheckCrc( trans_buff, len ) != 0 )
   {
      DEBUG( DEBUG_WORDY, INDENT_TRANS, "%s: CRC check failed\r\n", __func__ );
      trans_stats.crc_fails++;
      return TRANS_ERR_CRC;
   }
   len -= 2;
   print_buff( DEBUG_VERBOSE, INDENT_TRANS, "Received Data w/o CRC", trans_buff, len );

   if ( seq == trans_seq )
   {
      DEBUG( DEBUG_WORDY, INDENT_TRANS, "%s: Received duplicate for seq %d\r\n", __func__, trans_seq );
      trans_stats.duplicates++;
      recv_len = TRANS_ERR_DUP;
   }
   else
   {
      /* Save the data */
      recv_len = len - pos;
      trans_stats.bytes_recv += recv_len;
      memcpy( buff, &trans_buff[pos], recv_len );
      trans_seq = seq;
   }

   /* Send ACK */
   len = 0;
   flags = TRANS_FLAG_ACK;
   memcpy( &trans_buff[len], &flags, sizeof( flags ) );
   len +=  sizeof( flags );
   memcpy( &trans_buff[len], &seq, sizeof( seq ) );
   len += sizeof( seq );
   Chilipepper_AppendCrc( trans_buff, len );
   len += 2;

   usleep( 5000 );

   DEBUG( DEBUG_BASIC, INDENT_TRANS, "%s: Sent [ack %d, crc 0x%02x%02x]\r\n", __func__, seq, trans_buff[len - 2], trans_buff[len - 1] );

   print_buff( DEBUG_VERBOSE, INDENT_TRANS, "Sending ACK Data + CRC", trans_buff, len );

   len = send_link( trans_buff, len );

   trans_stats.bytes_sent += len;

   if ( len < 0 )
   {
      DEBUG( DEBUG_WORDY, INDENT_TRANS, "%s: send_link error %d", __func__, len );
   }

   return recv_len;
}

void trans_done( void )
{
   int len;
   unsigned char flags = 0;

   /* Send ACK */
   len = 0;
   flags = TRANS_FLAG_ACK;
   memcpy( &trans_buff[len], &flags, sizeof( flags ) );
   len +=  sizeof( flags );
   memcpy( &trans_buff[len], &trans_seq, sizeof( trans_seq ) );
   len += sizeof( trans_seq );
   Chilipepper_AppendCrc( trans_buff, len );
   len += 2;

 resend:
   usleep( 5000 );

   send_link( trans_buff, len );

   goto resend;
}


/*
 * TRANS Layer Setup
 */
int set_trans( trans_t trans )
{
   DEBUG( DEBUG_FUNC, INDENT_TRANS, "%s\r\n", __func__);

   trans_seq = 0;

   trans_stats.bytes_recv = 0;
   trans_stats.bytes_sent = 0;
   trans_stats.crc_fails = 0;
   trans_stats.duplicates = 0;
   trans_stats.retries = 0;
   trans_stats.max_retries_per_block = 0;

   switch ( trans )
   {
      case TRANS_PASS:
         DEBUG( DEBUG_VERBOSE, INDENT_TRANS, "%s: Pass All\r\n", __func__ );
         send_trans = send_trans_pass;
         recv_trans = recv_trans_pass;
         break;
      case TRANS_CRC:
         DEBUG( DEBUG_VERBOSE, INDENT_TRANS, "%s: CRC\r\n", __func__ );
         send_trans = send_trans_crc;
         recv_trans = recv_trans_crc;
         break;
      case TRANS_STOP_N_WAIT:
         DEBUG( DEBUG_VERBOSE, INDENT_TRANS, "%s: Stop-and-Wait\r\n", __func__);
         send_trans = send_trans_stop_n_wait;
         recv_trans = recv_trans_stop_n_wait;
         break;
      default:
         break;
   }

   return 0;
}

