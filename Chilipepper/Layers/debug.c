#include "debug.h"

#ifdef XILINX
#include "xil_printf.h"
#else
#include <stdio.h>
#endif

#include <math.h>
#include <stdarg.h>

debug_level_t debug_levels = DEBUG_BASIC;

void populate_buff( unsigned char *buff, const int len )
{
   unsigned int i;
   for ( i = 0; i < len; ++i )
      buff[i] = ( i ) % 256;
}

void print_buff( const debug_level_t level, const debug_indent_t indent, const char *title, const unsigned char *buff, const int length )
{
   int i;
   if ( title )
      DEBUG( level, indent, "%s", title );

   for ( i = 0; i < length; ++i )
   {
      if ( i % 16 == 0 )
      {
         DEBUG( level, indent, "\r\n" );
         //DEBUG( level, indent, "" );
      }
      DEBUG( level, 0, "0x%02x ", buff[i] );
   }
   DEBUG( level, 0, "\r\n" );
}

void set_debug_level( const debug_level_t level )
{
   debug_levels = level;
}

#ifndef XILINX
int Chilipepper_CheckCrc( unsigned char *rxBuf, int numBytes )
{
    unsigned short valueCRC, genPoly; // 16-bits
    unsigned short p, b, top;
    unsigned long valueCRCsh1, valueCRCadd1; // 32-bits
    unsigned char cv, d;
    int i1, i2;

    valueCRC = 65535;
    genPoly = 4129;
    for (i1=0; i1<numBytes; i1++) // add header and CRC
    {
    	cv = rxBuf[i1];
        for (i2=0; i2<8; i2++)
        {
            p = pow(2,7-i2);
            d = cv/p;
            b = d%2; // mod of message
            valueCRCsh1 = valueCRC<<1; // shift left
            valueCRCadd1 = valueCRCsh1|b; // bit or
            top = valueCRCadd1/pow(2,16);
            if ( top== 1)
            {
                valueCRC = valueCRCadd1^genPoly; // bit xor
            }
            else
            {
                valueCRC = valueCRCadd1;
            }
            valueCRC = valueCRC%65535;
        }
    }
    return valueCRC;
}

void Chilipepper_AppendCrc( unsigned char *buf, int msgLength )
{
    unsigned short valueCRC, genPoly; // 16-bits
    unsigned short p, b, top;
    unsigned long valueCRCsh1, valueCRCadd1; // 32-bits
    int i1, i2;
    unsigned char msb, lsb;
    unsigned char cv, d;

    valueCRC = 65535; //64*2014 = 64K
    genPoly = 4129;
    for (i1=0; i1<(msgLength+2); i1++) // add header and CRC
    {
        cv = buf[i1];
        for (i2=0; i2<8; i2++)
        {
            p = pow(2,7-i2);
            if (i1<msgLength)
            {
            	d = cv/p;
            	b = d%2; // mod of message
            }
            else
            	b = 0;
            valueCRCsh1 = valueCRC<<1; // shift left
            valueCRCadd1 = valueCRCsh1|b; // bit or
            top = valueCRCadd1/pow(2,16);
            if ( top == 1)
            {
                valueCRC = valueCRCadd1^genPoly; // bit xor
            }
            else
            {
                valueCRC = valueCRCadd1;
            }
            valueCRC = valueCRC%65535;
        }
    }
    msb = valueCRC>>8;
    lsb = valueCRC%256;
    buf[msgLength] = msb;
    buf[msgLength+1] = lsb;
}
#endif
