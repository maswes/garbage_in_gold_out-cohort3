/*
 * phy_layer.c
 */

#include <string.h>
#include <stdio.h>
#include "phy_layer.h"
#include "debug.h"

#ifdef XILINX
#include "chilipepper.h"
#endif

unsigned char phy_buff[PHY_BUFF_SIZE];

/*
 * Pass Through PHY
 */

#define LOOPBACK_BUFF_SIZE 2048
static unsigned char loop_buff[LOOPBACK_BUFF_SIZE];
static int loop_length;

int send_phy_loopback( unsigned char *buff, const int length )
{
   DEBUG( DEBUG_FUNC, INDENT_PHY, "%s\r\n", __func__);
   int len = length;
   if ( len + loop_length > LOOPBACK_BUFF_SIZE )
      len = LOOPBACK_BUFF_SIZE - loop_length;

   memcpy( &loop_buff[loop_length], buff, len );
   loop_length = loop_length + len;

   return len;
}

int recv_phy_loopback( unsigned char *buff )
{
   DEBUG( DEBUG_FUNC, INDENT_PHY, "%s\r\n", __func__);
   int len = loop_length;
   memcpy( buff, loop_buff, len );
   loop_length = 0;
   return len;
}

#ifdef XILINX
int send_phy_chili( unsigned char *buff, const int length )
{
   phy_buff[0] = 0xAA;
   phy_buff[1] = length;
   memcpy( &phy_buff[2], buff, length );
   print_buff( DEBUG_VERBOSE, INDENT_PHY, "Sent", phy_buff, length + 2 );
   Chilipepper_WriteRawPacket( phy_buff, length + 2 );
   return length;
}

int recv_phy_chili( unsigned char *buff )
{
   int bytesRead, i;
   bytesRead = Chilipepper_ReadRawPacket( phy_buff );

   if ( bytesRead == -1 )
      return bytesRead;

   switch ( phy_buff[0] )
   {
      case 0xaa:
         break;
      case 0x55:
         for ( i = 0; i < bytesRead; ++i )
            phy_buff[i] = ~phy_buff[i];
         break;
      default:
         DEBUG( DEBUG_BASIC, INDENT_PHY, "%s: Bad Sentinel 0x%02x\r\n", __func__, phy_buff[0] );
         return -3;
   }

   bytesRead = phy_buff[1];
   if ( bytesRead > FEC_BUFF_SIZE )
   {
      DEBUG( DEBUG_BASIC, INDENT_PHY, "%s: Bad Size %d\r\n", __func__, bytesRead );
      return -2;
   }

   memcpy( buff, &phy_buff[2], bytesRead );

   print_buff( DEBUG_VERBOSE, INDENT_PHY, "Recv", buff, bytesRead );

   return bytesRead;
}
#endif

/*
 * PHY Layer Setup
 */
int set_phy( phy_t phy )
{
   switch ( phy )
   {
      case PHY_LOOPBACK:
         send_phy = send_phy_loopback;
         recv_phy = recv_phy_loopback;
         break;
#ifdef XILINX
      case PHY_CHILI:
          send_phy = send_phy_chili;
          recv_phy = recv_phy_chili;
          break;
#endif
      default:
         break;
   }

   return 0;
}
