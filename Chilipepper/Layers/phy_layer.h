/*
 * phy_layer.h
 */

#ifndef PHY_LAYER_H_
#define PHY_LAYER_H_

#include "fec_layer.h"

typedef enum { PHY_LOOPBACK, PHY_LOCAL, PHY_CHILI, PHY_INVALID } phy_t;

#define PHY_ADDITION 2
#define PHY_BUFF_SIZE ( FEC_BUFF_SIZE + PHY_ADDITION )

int set_phy( phy_t phy );

int (*send_phy)( unsigned char *buff, const int length );
int (*recv_phy)( unsigned char *buff );

#endif /* PHY_LAYER_H_ */
