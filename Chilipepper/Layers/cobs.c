/* 
 * Original based on: https://github.com/jacquesf/COBS-Consistent-Overhead-Byte-Stuffing
 *
 * Copyright 2011, Jacques Fortier. All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted, with or without modification.
 */

/* Stuffs "length" bytes of data at the location pointed to by
 * "input", writing the output to the location pointed to by
 * "output". Returns the number of bytes written to "output".
 *
 * Remove the "restrict" qualifiers if compiling with a
 * pre-C99 C dialect.
 */
int cobs_encode(const unsigned char *input, int length, unsigned char *output)
{
   int read_index = 0;
   int write_index = 1;
   int code_index = 0;
   unsigned char code = 1;

   while(read_index < length)
   {
      if(input[read_index] == 0)
      {
         output[code_index] = code;
         code = 1;
         code_index = write_index++;
         read_index++;
      }
      else
      {
         output[write_index++] = input[read_index++];
         code++;
         if(code == 0xFF)
         {
            output[code_index] = code;
            code = 1;
            code_index = write_index++;
         }
      }
   }

   output[code_index] = code;

   return write_index;
}

/* Unstuffs "length" bytes of data at the location pointed to by
 * "input", writing the output * to the location pointed to by
 * "output". Returns the number of bytes written to "output" if
 * "input" was successfully unstuffed, and 0 if there was an
 * error unstuffing "input".
 *
 * Remove the "restrict" qualifiers if compiling with a
 * pre-C99 C dialect.
 */
int cobs_decode(const unsigned char *input, int length, unsigned char *output)
{
   int read_index = 0;
   int write_index = 0;
   unsigned char code;
   unsigned char i;

   while(read_index < length)
   {
      code = input[read_index];

      if(read_index + code > length && code != 1)
      {
         return 0;
      }

      read_index++;

      for(i = 1; i < code; i++)
      {
         output[write_index++] = input[read_index++];
      }
      if(code != 0xFF && read_index != length)
      {
         output[write_index++] = '\0';
      }
   }

   return write_index;
}
