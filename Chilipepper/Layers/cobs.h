/* 
 * Original based on: https://github.com/jacquesf/COBS-Consistent-Overhead-Byte-Stuffing
 *
 * Copyright 2011, Jacques Fortier. All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted, with or without modification.
 */
#ifndef _COBS_H
#define _COBS_H


int cobs_encode(const unsigned char *input, int length, unsigned char *output);
int cobs_decode(const unsigned char *input, int length, unsigned char *output);

#endif
