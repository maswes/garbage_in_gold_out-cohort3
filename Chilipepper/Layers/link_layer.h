/*
 * link_layer.h
 */

#ifndef LINK_LAYER_H_
#define LINK_LAYER_H_

#include "trans_layer.h"

#define LINK_COBS_ADDITION 1
#define LINK_BUFF_SIZE ( TRANS_BUFF_SIZE + LINK_COBS_ADDITION )

typedef enum { LINK_PASS, LINK_COBS, LINK_INVALID } link_t;

int set_link( link_t link );

int (*send_link)( unsigned char *buff, const int length );
int (*recv_link)( unsigned char *buff );

#endif /* LINK_LAYER_H_ */
