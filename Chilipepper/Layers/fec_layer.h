/*
 * fec_layer.h
 */

#ifndef FEC_LAYER_H_
#define FEC_LAYER_H_

#include "link_layer.h"

typedef enum { FEC_PASS, FEC_TRIPLE, FEC_HAMMING, FEC_RS, FEC_INVALID } fec_t;

#define FEC_BUFF_SIZE ( LINK_BUFF_SIZE * 2 )

int set_fec( fec_t fec );

int (*send_fec)( unsigned char *buff, const int length );
int (*recv_fec)( unsigned char *buff );

#endif /* FEC_LAYER_H_ */
