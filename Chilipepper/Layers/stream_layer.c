/*
 * stream_layer.c
 */

#include <string.h>
#include "stream_layer.h"
#include "trans_layer.h"
#include "hamming.h"
#include "debug.h"

unsigned char stream_buff[STREAM_BUFF_SIZE];


int send_stream_pass( unsigned char *buff, const int length )
{
   DEBUG( DEBUG_FUNC, INDENT_STREAM, "%s\r\n", __func__);

   return send_trans( buff, length );
}

int recv_stream_pass( unsigned char *buff )
{
   DEBUG( DEBUG_FUNC, INDENT_STREAM, "%s\r\n", __func__);

   return recv_trans( buff );
}

int send_stream_chunk( unsigned char *buff, const int length )
{
   DEBUG( DEBUG_FUNC, INDENT_STREAM, "%s\r\n", __func__);
   int i, chunks, sent, err, len;
   unsigned char hamm;

   if      ( g_rssi >= 120 ) g_npar_idx = 0;
   else if ( g_rssi >= 100 ) g_npar_idx = 1;
   else if ( g_rssi >=  75 ) g_npar_idx = 2;
   else if ( g_rssi >=  50 ) g_npar_idx = 3;
   else                      g_npar_idx = 4;

   g_npar = npar_table[g_npar_idx];

   DEBUG( DEBUG_FUNC, INDENT_STREAM, "%s: Using %d parity for RSSI of %d\r\n",
      __func__, g_npar, g_rssi);

   hamm = HammingMatrixEncode(g_npar_idx);
   stream_buff[0] = hamm;
   stream_buff[1] = hamm;
   stream_buff[2] = hamm;

   DEBUG( DEBUG_FUNC, INDENT_STREAM, "%s: hamming(%d) = %d = 0x%02x\r\n",
      __func__, g_npar_idx, hamm, hamm );

   g_msg_size = RS_N - g_npar;
   chunks = length / g_msg_size;
   if ( chunks == 0 ) chunks = 1;

   DEBUG( DEBUG_FUNC, INDENT_STREAM, "%s: Sending %d chunks of size %d\r\n",
      __func__, chunks, g_msg_size );

   len = g_msg_size;
   for ( i = 0; i < chunks; ++i )
   {
      if ( length < ( ( i + 1 ) * g_msg_size ) )
         len = length % g_msg_size;
      memcpy( &stream_buff[TRIPLE_HAMMING], &buff[i * g_msg_size], len );
      print_buff( DEBUG_BASIC, INDENT_STREAM, "Send Chunk", stream_buff, STREAM_BUFF_SIZE );
      err = send_trans(stream_buff, STREAM_BUFF_SIZE );
      if ( err < 0 )
      {
         DEBUG( DEBUG_BASIC, INDENT_STREAM, "%s: Error %d sending buff[%d]\r\n", __func__, err, i * STREAM_BUFF_SIZE );
         return err;
      }
      sent += err;
   }
   
   return sent;
}

int recv_stream_chunk( unsigned char *buff )
{
   int len;
   DEBUG( DEBUG_FUNC, INDENT_STREAM, "%s\r\n", __func__);
   while ( 1 )
   {
      len = recv_trans( buff );
      if ( len >= 0 )
         return len;
   }
}

/*
 * Sream Layer Setup
 */
int set_stream( stream_t stream )
{
   DEBUG( DEBUG_FUNC, INDENT_STREAM, "%s\r\n", __func__);
   switch ( stream )
   {
      case STREAM_PASS:
         DEBUG( DEBUG_VERBOSE, INDENT_STREAM, "%s: Passall\r\n", __func__ );
         send_stream = send_stream_pass;
         recv_stream = recv_stream_pass;
         break;
      case STREAM_CHUNK:
         DEBUG( DEBUG_VERBOSE, INDENT_STREAM, "%s: Chunk\r\n", __func__ );
         send_stream = send_stream_chunk;
         recv_stream = recv_stream_chunk;
         break;
      default:
         break;
   }

   return 0;
}
