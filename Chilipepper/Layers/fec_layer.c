#include <string.h>
#include <stdio.h>
#include "phy_layer.h"
#include "fec_layer.h"
#include "hamming.h"
#include "debug.h"
#include "ecc.h"

unsigned char fec_buff[FEC_BUFF_SIZE];

static int send_fec_pass( unsigned char *buff, const int length )
{
   DEBUG( DEBUG_FUNC, INDENT_FEC, "%s\r\n", __func__);
   return send_phy( buff, length );
}

static int recv_fec_pass( unsigned char *buff )
{
   DEBUG( DEBUG_FUNC, INDENT_FEC, "%s\r\n", __func__);
   return recv_phy( buff );
}

static int send_fec_hamming( unsigned char *buff, const int length )
{
   DEBUG( DEBUG_FUNC, INDENT_FEC, "%s\r\n", __func__);
   int i;

   if ( length > FEC_BUFF_SIZE )
      return -1;
   
   for ( i = 0; i < length; ++i )
   {
      fec_buff[i * 2] = HammingMatrixEncode( buff[i] & 0x0f );
      fec_buff[i * 2 + 1] = HammingMatrixEncode( ( buff[i] >> 4 ) & 0xf );
   }

   print_buff( DEBUG_VERBOSE, INDENT_FEC, "Encoded Hamming", fec_buff, length * 2 );

   return send_phy( fec_buff, length * 2 );
}

static int recv_fec_hamming( unsigned char *buff )
{
   DEBUG( DEBUG_FUNC, INDENT_FEC, "%s\r\n", __func__);
   int i, len;
   unsigned int syndrome;
   unsigned char data_out_f0, data_out_0f;

   len = recv_phy( fec_buff );

   if ( len < 0 )
      return len;

   for ( i = 0; i < len; i += 2 )
   {
      data_out_0f = HammingMatrixDecode( fec_buff[i], &syndrome );
      //printf( "%s: dec(0x%02x) = 0x%02x, errors %hd\n", __func__, fec_buff[i], data_out_0f, syndrome );
      data_out_f0 = HammingMatrixDecode( fec_buff[i+1], &syndrome );
      //printf( "%s: dec(0x%02x) = 0x%02x, errors %hd\n", __func__, fec_buff[i], data_out_f0, syndrome );
      buff[i/2] = ( data_out_f0 << 4 ) | data_out_0f;
   }

   len = len / 2;

   print_buff( DEBUG_VERBOSE, INDENT_FEC, "Decoded Hamming", buff, len );

   return len;
}

static int send_fec_rs( unsigned char *buff, const int length )
{
   DEBUG( DEBUG_FUNC, INDENT_FEC, "%s: length = %d\r\n", __func__, length );

   static int run_once = 1;
   static unsigned char codeword[256];

   if ( run_once )
   {
      DEBUG( DEBUG_FUNC, INDENT_FEC, "%s: Initialize ECC\r\n", __func__ );
      run_once = 0;
      initialize_ecc();
   }

   if ( length > FEC_BUFF_SIZE )
      return -1;

   encode_data( g_npar_idx, &buff[TRIPLE_HAMMING], g_msg_size, codeword );
   memcpy( fec_buff, buff, length );
   memcpy( &fec_buff[TRIPLE_HAMMING + g_msg_size], &codeword[g_msg_size], g_npar );

   print_buff( DEBUG_VERBOSE, INDENT_FEC, "Encoded RS", fec_buff, length );

   return send_phy( fec_buff, length );
}

static int recv_fec_rs( unsigned char *buff )
{
   DEBUG( DEBUG_FUNC, INDENT_FEC, "%s\r\n", __func__);

   int len, npar_idx, npar, msg_len;
   unsigned int syndrome;
   unsigned char hamm;
   static unsigned char codeword[256];

   len = recv_phy( fec_buff );

   DEBUG( DEBUG_FUNC, INDENT_FEC, "%s: Received %d bytes\r\n", __func__, len );

   if ( len < 0 )
      return len;

   hamm = fec_buff[0] & fec_buff[1] | fec_buff[0] & fec_buff[2] | fec_buff[1] & fec_buff[2];
   //hamm = fec_buff[0];
   npar_idx = HammingMatrixDecode( hamm, &syndrome );
   npar = npar_table[npar_idx];
   msg_len = RS_N - npar;
   DEBUG( DEBUG_FUNC, INDENT_FEC, "%s: dec_hamming(%d) = %d --> npar = %d, msg_len = %d\r\n",
      __func__, hamm, npar_idx, npar, msg_len );

   fec_buff[4] = 0xff;

   memcpy( codeword, &fec_buff[TRIPLE_HAMMING], RS_N );

   print_buff( DEBUG_VERBOSE, INDENT_FEC, "Corrupted codeword", codeword, RS_N );

   decode_data( npar, codeword, RS_N );
   if (check_syndrome (npar) != 0) {
      correct_errors_erasures (npar,
         codeword, 
         RS_N,
         0, 
         0);
   }

   memcpy( buff, codeword, msg_len );

   print_buff( DEBUG_VERBOSE, INDENT_FEC, "Decoded RS", buff, msg_len );

   return msg_len;
}

/*
 * FEC Layer Setup
 */
int set_fec( fec_t fec )
{
   switch ( fec )
   {
      case FEC_PASS:
         send_fec = send_fec_pass;
         recv_fec = recv_fec_pass;
         break;
      case FEC_HAMMING:
         send_fec = send_fec_hamming;
         recv_fec = recv_fec_hamming;
         break;
      case FEC_RS:
         send_fec = send_fec_rs;
         recv_fec = recv_fec_rs;
         break;
      default:
         break;
   }

   return 0;
}

