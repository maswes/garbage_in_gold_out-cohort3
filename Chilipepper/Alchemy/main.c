#include <stdio.h>
#include "debug.h"
#include "alchemy.h"

int main (int argc, char **argv)
{
   unsigned char buff[2048];

   int size = 400;

   alch_init(1, TOYON);

   populate_buff( buff, size );

   printf("%s: Running\n", __func__);

   alch_send(buff, size);

   g_mode = ALCHEMY;
   alch_send(buff, size);


   return 0;
}
