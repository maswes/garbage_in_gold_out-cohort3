#include <stdio.h>
#include <string.h>
#include "alchemy.h"
#include "hamming.h"
#include "debug.h"

#ifndef XILINX
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#else
#include "chilipepper.h"
#endif


int g_rssi;
int g_use_crc;
Mode g_mode;
Retry g_retry;
int g_packet_size;

char ACK = 0x01;

//#define TOYON_SHORTEN 228
#define TOYON_SHORTEN 188

#define SHORTEN_RS_N 127

#ifndef XILINX
struct sockaddr_in servaddr, cliaddr;
int g_fd;
int g_client;
#endif

void phy_setup(int client)
{
#ifndef XILINX
   g_client = client;
   g_fd=socket(AF_INET,SOCK_DGRAM,0);
   int flags = fcntl(g_fd, F_GETFL, 0);
   fcntl(g_fd, F_SETFL, flags | O_NONBLOCK);

   bzero(&servaddr,sizeof(servaddr));
   servaddr.sin_family = AF_INET;
   servaddr.sin_port=htons(32000);

   if (g_client)
   {
      printf("%s: Sender\r\n", __func__);
      servaddr.sin_addr.s_addr=inet_addr("127.0.0.1");
   }
   else
   {
      printf("%s: Receiver\r\n", __func__);
      servaddr.sin_addr.s_addr=htonl(INADDR_ANY);
      bind(g_fd,(struct sockaddr *)&servaddr,sizeof(servaddr));
   }
#endif
}

int phy_send(unsigned char *buff, int len)
{
   int err = 0;
#ifndef XILINX
   int send_len;
   struct sockaddr *saddr;
   if (g_client)
   {
      saddr = (struct sockaddr *)&servaddr;
      send_len = sizeof(servaddr);
   }
   else
   {
      saddr = (struct sockaddr *)&cliaddr;
      send_len = sizeof(cliaddr);
   }
   err = sendto(g_fd, buff, len, 0, saddr, send_len);
#else
   Chilipepper_WriteRawPacket(buff, len);
#endif
   return err;
}

int phy_recv(unsigned char *buff, int len)
{
   int err = -1;
   int retries = 2000;
   int clilen;
   while (err < 0 && retries-- > 1)
   {
#ifndef XILINX
      clilen = sizeof(cliaddr);
      err = recvfrom(g_fd, buff, len, 0, (struct sockaddr *)&cliaddr, (socklen_t *)&clilen);
      sleep(1);
#else
      if (g_mode == ALCHEMY)
         len = g_packet_size + 1;
      else
         len = 0;
      err = Chilipepper_ReadRawPacket(buff, len);
#endif
   }

   /*
   if (!retries)
   {
      DEBUG(DEBUG_VERBOSE, INDENT_MAIN, "Timed out waiting for data\r\n");
   }
   */
   
   return err;
}

void alch_init_local (int use_crc, Mode mode, Retry retry, int client)
{
   initialize_ecc();
   g_use_crc = use_crc;
   g_mode = mode;
   g_retry = retry;
   g_rssi = 9;
   g_npar_idx = 4; // 4 = 64B, 5 = 128B

   if (g_mode == TOYON)
   {
      /* Stay under 255 due to FIFO limit */
      g_packet_size = sizeof(alch_packet) - TOYON_SHORTEN;
   }
   else
   {
      g_packet_size = sizeof(alch_packet);
   }

   phy_setup(client);
}

int alch_send (unsigned char *buff, const int length)
{
   int chunk_size, num_chunks;
   int payload_len, i;
   int crc;
   int recv_packet_size = 259;
   int retries = -1;

   alch_packet alchemy_packet, recv_packet;

   /* ALCHEMY Mode variables */
   unsigned char hamm;
   unsigned int syndrome;
   unsigned char *fec_buff;
   int npar, msg_len, size;
   static unsigned char codeword[256];
   static unsigned char rxBuff[512];

   static int sequence_num = 0;

   /* Setup the chuck size */
   switch (g_mode)
   {
      case TOYON:
         chunk_size = sizeof(alchemy_packet.payload.data) - TOYON_SHORTEN;
         break;
      case ALCHEMY:
         /*
         if      ( g_rssi >= 120 ) g_npar_idx = 0;
         else if ( g_rssi >= 100 ) g_npar_idx = 1;
         else if ( g_rssi >=  75 ) g_npar_idx = 2;
         else if ( g_rssi >=  50 ) g_npar_idx = 3;
         else                      g_npar_idx = 4;
         */
         g_npar = npar_table[g_npar_idx];
         chunk_size = sizeof(alchemy_packet.payload) - sizeof(alchemy_packet.payload.header) - g_npar - SHORTEN_RS_N;
         break;
      default:
         break;
   }

   num_chunks = length / chunk_size + 1;

   DEBUG_VA(DEBUG_BASIC, INDENT_STREAM, "Sending %d bytes in %d chunks of size %d\r\n",
         length, num_chunks, chunk_size);

   for (i = 0; i < num_chunks; ++i)
   {
      /* Clear the packet */
      memset(&alchemy_packet, 0, sizeof(alchemy_packet));

      DEBUG_VA(DEBUG_BASIC, INDENT_STREAM, "Sending %d bytes in %d chunks of size %d\r\n",
         length, num_chunks, chunk_size);

      /* See if payload fills packet */
      if (length < (i + 1) * chunk_size)
      {
         payload_len = length % chunk_size;
      }
      else
      {
         payload_len = chunk_size;
      }
      
      /* Set the sequence number */
      alchemy_packet.payload.header.seq = sequence_num++;        

      /* Copy in the data */
      memcpy(alchemy_packet.payload.data, &buff[i * chunk_size], payload_len);

      /* Setup the payload header */
      alchemy_packet.payload.header.length = payload_len;
      alchemy_packet.payload.header.flags = 0;
      switch (g_mode)
      {
         case TOYON:
            alchemy_packet.th_size[0] = g_packet_size % 256;
            alchemy_packet.th_size[1] = (g_packet_size >> 8) % 256;
            alchemy_packet.th_size[2] = (g_packet_size >> 16) % 256;
            break;
         case ALCHEMY:
            hamm = HammingMatrixEncode(g_npar_idx);
            alchemy_packet.th_size[0] = hamm;
            alchemy_packet.th_size[1] = hamm;
            alchemy_packet.th_size[2] = hamm;
            break;
         default:
            break;
      }

      /* Calculate the CRC */
      if (g_use_crc)
      {
         Alchemy_PrependCrc((unsigned char *)&alchemy_packet.payload, sizeof(alchemy_packet.payload));
      }

      DEBUG_VA(DEBUG_BASIC, INDENT_TRANS, "Sending [seq %d, packet len %d, payload len %d, crc 0x%04x]\r\n",
            alchemy_packet.payload.header.seq, g_packet_size, 
            alchemy_packet.payload.header.length, alchemy_packet.payload.header.crc);

      print_buff(DEBUG_BUFFERS, INDENT_TRANS, "Unencoded Packet",
                 (unsigned char *)&alchemy_packet, g_packet_size);

      if (g_mode == ALCHEMY)
      {
         memset(codeword, 0, 256);
         
         encode_data(g_npar_idx, 
            (unsigned char *)&alchemy_packet.payload, 
            payload_len + sizeof(alchemy_packet.payload.header), 
            codeword);
#ifndef XILINX
         codeword[10] = codeword[20] = codeword[30] = 0xff;
#endif
         memcpy((unsigned char *)&alchemy_packet.payload, codeword, RS_N);
         
         print_buff(DEBUG_BUFFERS, INDENT_TRANS, "Encoded Packet",
                    (unsigned char *)&alchemy_packet, g_packet_size);
      }

  retry:
      if (retries > 0)
      {
         DEBUG_VA(DEBUG_BASIC, INDENT_TRANS, "Retrying(%d) seq %d\r\n", retries, sequence_num - 1);
      }

      size = phy_send((unsigned char *)&alchemy_packet, g_packet_size);

      if (g_retry)
      {
         DEBUG(DEBUG_BASIC, INDENT_TRANS, "Waiting for ACK\r\n");
      }
      else
      {
         DEBUG(DEBUG_BASIC, INDENT_TRANS, "Not waiting for ACK\r\n");
         continue;
      }

      bzero(rxBuff, sizeof(rxBuff));
      bzero((unsigned char *)&recv_packet, sizeof(recv_packet));

      size = phy_recv(rxBuff, sizeof(rxBuff));

      if (size < 0)
      {
         DEBUG(DEBUG_BASIC, INDENT_LINK, "Timed out waiting for ACK\r\n");
         retries++;
         goto retry;
      }

#ifdef XILINX
      if (g_mode == ALCHEMY)
      {
         recv_packet_size = g_packet_size + 1;
      }
#endif

      if (size != recv_packet_size)
      {
         DEBUG_VA(DEBUG_BASIC, INDENT_LINK, "Received the incorrect length %d\r\n", size);
         retries++;
         goto retry;
      }

      memcpy((unsigned char *)&recv_packet, rxBuff, size);

      print_buff(DEBUG_BUFFERS, INDENT_LINK, "Received Packet",
         (unsigned char *)&recv_packet, g_packet_size);

      if (g_mode == ALCHEMY)
      {
         /* Determine the parity and msg length based on triple Haming */
         fec_buff = (unsigned char *)&recv_packet;
         hamm = (fec_buff[0] & fec_buff[1]) | (fec_buff[0] & fec_buff[2]) | (fec_buff[1] & fec_buff[2]);
         //g_npar_idx = HammingMatrixDecode(hamm, &syndrome);
         npar = npar_table[g_npar_idx];
         msg_len = RS_N - SHORTEN_RS_N - npar;
         DEBUG_VA(DEBUG_BASIC, INDENT_LINK, "Hamming indicates message lenghth of %d\r\n", msg_len);
         print_buff( DEBUG_BUFFERS, INDENT_LINK, "Encoded Chunk",
            (unsigned char *)&recv_packet.payload, RS_N);


         bzero(codeword, 256);
         memcpy(codeword, (unsigned char *)&recv_packet.payload, RS_N - SHORTEN_RS_N );

         decode_data( npar, codeword, RS_N );
         if (check_syndrome (npar) != 0) 
            correct_errors_erasures(npar, codeword, RS_N, 0, 0);

         memset(&codeword[RS_N - npar], 0, npar);
         print_buff(DEBUG_BUFFERS, INDENT_LINK, "Decoded Chunk", codeword, RS_N);

         bzero((unsigned char *)&recv_packet.payload, sizeof(recv_packet.payload));
         memcpy((unsigned char *)&recv_packet.payload, codeword, RS_N);

         int recv_payload_len = recv_packet.payload.header.length;

         if (recv_payload_len < msg_len)
         {
            DEBUG_VA(DEBUG_VERBOSE, INDENT_LINK, "Zeroing out %d extra bytes starting at [%d]\r\n",
               msg_len - recv_payload_len + SHORTEN_RS_N, recv_payload_len);
            bzero((unsigned char *)&recv_packet.payload.data[recv_payload_len],
               msg_len - recv_payload_len + SHORTEN_RS_N);
            print_buff(DEBUG_BUFFERS, INDENT_LINK, "Shortend Packet",
               (unsigned char *)&recv_packet, g_packet_size);
         }
      }

      if (g_use_crc)
      {
         crc = Alchemy_CheckCrc((unsigned char *)&recv_packet.payload, sizeof(recv_packet.payload));
         if (crc != 0)
         {
            DEBUG_VA(DEBUG_BASIC, INDENT_LINK, "Bad CRC 0x%04x\r\n", crc);
            retries++;
            goto retry;
         }
      }

      if (recv_packet.payload.header.flags != ACK)
      {
         DEBUG(DEBUG_BASIC, INDENT_LINK, "Not an ACK\r\n");
         retries++;
         goto retry;
      }

      if (recv_packet.payload.header.seq != alchemy_packet.payload.header.seq)
      {
         DEBUG_VA(DEBUG_BASIC, INDENT_LINK, "Not the current sequence num of %d\r\n", alchemy_packet.payload.header.seq);
         retries++;
         goto retry;
      }

      DEBUG_VA(DEBUG_BASIC, INDENT_LINK, "Received ACK [seq %d, packet len %d, payload len %d, crc 0x%04x]\r\n",
         recv_packet.payload.header.seq, g_packet_size, 
         recv_packet.payload.header.length, recv_packet.payload.header.crc);

      //sleep(1);
   }

   return 0;
}

int alch_recv (unsigned char *buff)
{
   int size, npar, msg_len, crc;
   int payload_len;
   int recv_packet_size = 259;
   alch_packet alchemy_packet;

   unsigned short last_seq = -1;
   unsigned int syndrome;
   unsigned char hamm;
   unsigned char *fec_buff;
   static unsigned char codeword[256];
   static unsigned char rxBuff[512];
   int done = 0, total_len = 0, max_len = 512;
   int acks;

   while (!done)
   {
      bzero(rxBuff, sizeof(rxBuff));
      bzero((unsigned char *)&alchemy_packet, sizeof(alchemy_packet));

      size = phy_recv(rxBuff, sizeof(rxBuff));
   
      if (size < 0)
      {
         //DEBUG_VA(DEBUG_VERBOSE, INDENT_LINK, "Failed phy_recv %d\r\n", size);
         break;
      }

#ifdef XILINX
      if (g_mode == ALCHEMY)
      {
         recv_packet_size = g_packet_size + 1;
      }
#endif

      if (size != recv_packet_size)
      {
         DEBUG_VA(DEBUG_BASIC, INDENT_LINK, "Received the incorrect length %d %d %d\r\n", size, g_packet_size, recv_packet_size);
         return BAD_LEN;
      }

      memcpy((unsigned char *)&alchemy_packet, rxBuff, g_packet_size);

      print_buff(DEBUG_BUFFERS, INDENT_TRANS, "Received Packet",
                 (unsigned char *)&alchemy_packet, g_packet_size);

      if (g_mode == ALCHEMY)
      {
         /* Determine the parity and msg length based on triple Haming */
         fec_buff = (unsigned char *)&alchemy_packet;
         hamm = (fec_buff[0] & fec_buff[1]) | (fec_buff[0] & fec_buff[2]) | (fec_buff[1] & fec_buff[2]);
         //g_npar_idx = HammingMatrixDecode( hamm, &syndrome );
         npar = npar_table[g_npar_idx];
         msg_len = RS_N - SHORTEN_RS_N - npar;

         DEBUG_VA(DEBUG_VERBOSE, INDENT_TRANS, "msg_len = %d for parity %d\r\n", msg_len, npar);

         print_buff( DEBUG_BUFFERS, INDENT_TRANS, "Encoded Chunk",
                     (unsigned char *)&alchemy_packet.payload, RS_N);

         bzero(codeword, 256);
         memcpy(codeword, (unsigned char *)&alchemy_packet.payload, RS_N - SHORTEN_RS_N );

         decode_data( npar, codeword, RS_N );
         if (check_syndrome (npar) != 0) 
            correct_errors_erasures(npar, codeword, RS_N, 0, 0);

         memset(&codeword[RS_N - npar], 0, npar);
         print_buff(DEBUG_BUFFERS, INDENT_TRANS, "Decoded Chunk", codeword, RS_N);

         bzero((unsigned char *)&alchemy_packet.payload, sizeof(alchemy_packet.payload));
         memcpy((unsigned char *)&alchemy_packet.payload, codeword, RS_N);

         payload_len = alchemy_packet.payload.header.length;
         
         if (payload_len < msg_len)
         {
            DEBUG_VA(DEBUG_VERBOSE, INDENT_TRANS, "Zeroing out %d extra bytes\r\n",
                  msg_len - payload_len);
            bzero((unsigned char *)&alchemy_packet.payload.data[payload_len], 
               msg_len - payload_len + SHORTEN_RS_N);
            print_buff(DEBUG_BUFFERS, INDENT_TRANS, "Shortened Packet",
                       (unsigned char *)&alchemy_packet, g_packet_size);
         }
      }

      if (g_use_crc)
      {
         crc = Alchemy_CheckCrc((unsigned char *)&alchemy_packet.payload, sizeof(alchemy_packet.payload));
         if (crc != 0)
         {
            DEBUG_VA(DEBUG_BASIC, INDENT_TRANS, "Bad CRC 0x%04x\r\n", crc);
            continue;
         }
      }

      DEBUG_VA(DEBUG_BASIC, INDENT_TRANS, "Received [seq %d, packet len %d, payload len %d, crc 0x%04x]\r\n",
            alchemy_packet.payload.header.seq, g_packet_size, 
            alchemy_packet.payload.header.length, alchemy_packet.payload.header.crc);

      payload_len = alchemy_packet.payload.header.length;
      if (last_seq == alchemy_packet.payload.header.seq)
      {
         DEBUG_VA(DEBUG_BASIC, INDENT_TRANS, "Received duplicate seq %d\r\n", last_seq);
      }
      else
      {
         if (total_len + payload_len <= max_len)
         {
            memcpy(&buff[total_len], (unsigned char *)&alchemy_packet.payload.data, payload_len);
            last_seq = alchemy_packet.payload.header.seq;
            total_len += payload_len;
         }
         else
         {
            DEBUG_VA(DEBUG_BASIC, INDENT_TRANS, "Done! Exceeds buffer with size %d\r\n",
                  total_len + payload_len);
            done = 1;
         }

         if (g_mode == TOYON)
         {
            if (payload_len < sizeof(alchemy_packet.payload.data) - TOYON_SHORTEN)
            {
               DEBUG_VA(DEBUG_BASIC, INDENT_TRANS, "Done! %d is less than %d\r\n", payload_len, sizeof(alchemy_packet.payload.data) - TOYON_SHORTEN);
               done = 1;
            }
         }
         else
         {
            msg_len -= sizeof(alchemy_packet.payload.header);
            if (payload_len < msg_len)
            {
               DEBUG_VA(DEBUG_BASIC, INDENT_TRANS, "Done! %d is less than %d\r\n", payload_len, msg_len);
               done = 1;
            }
         }
      }

      if (g_retry)
      {
         DEBUG(DEBUG_BASIC, INDENT_TRANS, "Sending ACK\r\n");
      }
      else
      {
         DEBUG(DEBUG_BASIC, INDENT_TRANS, "Not sending ACK\r\n");
         continue;
      }

      if (done)
         acks = 5;
      else
         acks = 2;
      while (acks-- > 0)
      {
         /* ACK good data */
         bzero((unsigned char *)&alchemy_packet.payload.data, sizeof(alchemy_packet.payload.data));
         alchemy_packet.payload.header.flags = ACK;
         alchemy_packet.payload.header.length = 0;
         alchemy_packet.payload.header.crc = 0;

         if (g_use_crc)
         {
            Alchemy_PrependCrc((unsigned char *)&alchemy_packet.payload, sizeof(alchemy_packet.payload));
         }
   
         DEBUG_VA(DEBUG_BASIC, INDENT_LINK, "ACKing [seq %d, packet len %d, payload len %d, crc 0x%04x]\r\n",
               alchemy_packet.payload.header.seq, g_packet_size, 
               alchemy_packet.payload.header.length, alchemy_packet.payload.header.crc);
   
         print_buff(DEBUG_BUFFERS, INDENT_LINK, "Unencoded Packet",
                    (unsigned char *)&alchemy_packet, g_packet_size);

         if (g_mode == ALCHEMY)
         {
            /* Use worst case parity */
            //g_npar_idx = 4;
            g_npar = npar_table[g_npar_idx];
            hamm = HammingMatrixEncode(g_npar_idx);
            alchemy_packet.th_size[0] = hamm;
            alchemy_packet.th_size[1] = hamm;
            alchemy_packet.th_size[2] = hamm;

            bzero(codeword, 256);

            encode_data(g_npar_idx, 
                        (unsigned char *)&alchemy_packet.payload, 
                        sizeof(alchemy_packet.payload.header), 
                        codeword);
            codeword[10] = codeword[20] = codeword[30] = 0xff;
            memcpy((unsigned char *)&alchemy_packet.payload, codeword, RS_N);
         
            print_buff(DEBUG_BUFFERS, INDENT_LINK, "Encoded Packet",
                       (unsigned char *)&alchemy_packet, g_packet_size);
         }

         phy_send((unsigned char *)&alchemy_packet, g_packet_size);

         //sleep(1);
      }
   }

   return total_len;
}
