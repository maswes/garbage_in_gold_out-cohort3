/* Sample UDP server */

#include <stdio.h>
#include "alchemy.h"
#include "debug.h"

int main(int argc, char**argv)
{
   int size, test_size = 248, count = 0, err, crc;
   // Alchemy
   unsigned char buff[256], msg[2048], test_msg[2048];

   /*
   populate_buff(&test_msg[2], test_size-2);
   print_buff(DEBUG_BASIC, INDENT_MAIN, "Orig", test_msg, test_size);
   Alchemy_PrependCrc(test_msg, test_size);
   print_buff(DEBUG_BASIC, INDENT_MAIN, "CRC", test_msg, test_size);
   crc = Alchemy_CheckCrc(test_msg, test_size);
   printf("%s: crc = 0x%04x\n", __func__, crc);
   */
   alch_init_local(USE_CRC, ALCHEMY, RETRY, SERVER);
   populate_buff(test_msg, test_size);
   test_msg[test_size-1] = '!';

   size = alch_recv(msg);
   printf("%s: Received %d bytes\n", __func__, size);

   print_buff( DEBUG_BASIC, INDENT_MAIN, "Recv", msg, size );

   //test_msg[102] = 0xff;
   err = memcmp(test_msg, msg, test_size);
   printf("%s: err = %d\n", __func__, err);

   return 0;
}
