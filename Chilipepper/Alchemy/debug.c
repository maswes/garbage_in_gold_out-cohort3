#include "debug.h"

#ifdef XILINX
#include <stdio.h>
#include <stdarg.h>
XUartPs *chili_uart;
void set_chili_uart(XUartPs *uart)
{
   chili_uart = uart;
}

void Chilipepper_printf_va( const char* format, ... )
{
   char dest[1024 * 16];
   va_list argptr;
   va_start(argptr, format);
   vsprintf(dest, format, argptr);
   va_end(argptr);
   Chilipepper_printf(chili_uart, dest);
}
#else
#include <stdio.h>
#endif

#include <math.h>
#include <stdarg.h>

debug_level_t debug_levels = DEBUG_BASIC;

void populate_buff( unsigned char *buff, const int len )
{
   unsigned int i;
   for ( i = 0; i < len; ++i )
      buff[i] = 'a';//( i ) % 256;
}

void print_buff
(  const debug_level_t level, 
   const debug_indent_t indent, 
   const char *title, 
   const unsigned char *buff, 
   const int length 
)
{
   char dest[1024 * 16];
   int i;

   bzero(dest, sizeof(dest));

   if ( title )
      SDEBUG_VA( level, indent, dest, "%s", title );

   for ( i = 0; i < length; ++i )
   {
      if ( i % 16 == 0 )
      {
         SDEBUG( level, indent, dest, "\r\n" );
         SDEBUG( level, indent, dest, "" );
      }
      SDEBUG_VA( level, 0, dest, "0x%02x ", buff[i] );
   }

   SDEBUG( level, 0, dest, "\r\n" );
   DEBUG(level, 0, dest);
}

void set_debug_level( const debug_level_t level )
{
   debug_levels = level;
}

int Alchemy_CheckCrc( unsigned char *rxBuf, int numBytes )
{
    unsigned short valueCRC, genPoly; // 16-bits
    unsigned short p, b, top;
    unsigned long valueCRCsh1, valueCRCadd1; // 32-bits
    unsigned char cv, d;
    int i1, i2;

    valueCRC = 65535;
    genPoly = 4129;
    for (i1=2; i1<numBytes+2; i1++) // add header and CRC
    {
        if (i1 >= numBytes)
           cv = rxBuf[i1 - numBytes];
        else
           cv = rxBuf[i1];

        for (i2=0; i2<8; i2++)
        {
            p = pow(2,7-i2);
            d = cv/p;
            b = d%2; // mod of message
            valueCRCsh1 = valueCRC<<1; // shift left
            valueCRCadd1 = valueCRCsh1|b; // bit or
            top = valueCRCadd1/pow(2,16);
            if ( top== 1)
            {
                valueCRC = valueCRCadd1^genPoly; // bit xor
            }
            else
            {
                valueCRC = valueCRCadd1;
            }
            valueCRC = valueCRC%65535;
        }
    }
    return valueCRC;
}

void Alchemy_PrependCrc( unsigned char *buf, int msgLength )
{
    unsigned short valueCRC, genPoly; // 16-bits
    unsigned short p, b, top;
    unsigned long valueCRCsh1, valueCRCadd1; // 32-bits
    int i1, i2;
    unsigned char msb, lsb;
    unsigned char cv, d;

    valueCRC = 65535; //64*2014 = 64K
    genPoly = 4129;
    for (i1=2; i1<msgLength+2; i1++) // add header and CRC
    {
        if (i1 < msgLength)
           cv = buf[i1];
        else
           cv = 0;

        for (i2=0; i2<8; i2++)
        {
            p = pow(2,7-i2);
            if (i1<msgLength)
            {
            	d = cv/p;
            	b = d%2; // mod of message
            }
            else
            	b = 0;
            valueCRCsh1 = valueCRC<<1; // shift left
            valueCRCadd1 = valueCRCsh1|b; // bit or
            top = valueCRCadd1/pow(2,16);
            if ( top == 1)
            {
                valueCRC = valueCRCadd1^genPoly; // bit xor
            }
            else
            {
                valueCRC = valueCRCadd1;
            }
            valueCRC = valueCRC%65535;
        }
    }

    msb = valueCRC>>8;
    lsb = valueCRC%256;
    buf[0] = msb;
    buf[1] = lsb;
}
