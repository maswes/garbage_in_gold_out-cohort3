/* Sample UDP client */

#include <stdio.h>
#include "alchemy.h"
#include "debug.h"

int main(int argc, char**argv)
{
   // Alchemy
   alch_init_local(USE_CRC, ALCHEMY, RETRY, CLIENT);
   unsigned char buff[2048];
   int size = 248;
   populate_buff(buff, size);
   buff[size-1] = '!';
   alch_send(buff, size);
   sleep(size/150 + 1);

   return 0;
}
