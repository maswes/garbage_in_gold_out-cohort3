#ifndef __ALCHEMY_H__
#define __ALCHEMY_H__

#include "ecc.h"

typedef enum Mode_t {TOYON, ALCHEMY} Mode;
typedef enum Use_CRC_t {NO_CRC, USE_CRC} Use_CRC;
typedef enum Retry_t {NO_RETRY, RETRY} Retry;
typedef enum Client_t {SERVER, CLIENT} Client;
typedef enum Error_t {NONE = 0, BAD_LEN = -1, BAD_CRC = -2} Error;

typedef struct alch_payload_hdr_t
{
   unsigned short crc;
   unsigned short length;
   unsigned short seq;
   unsigned char flags;
} __attribute__((__packed__)) alch_payload_hdr;

typedef struct alch_payload_t
{
   alch_payload_hdr header;
   unsigned char data[RS_N - sizeof(alch_payload_hdr)];
} __attribute__((__packed__)) alch_payload;

typedef struct alch_packet_t
{
   unsigned char th_size[3];
   alch_payload payload;
} __attribute__((__packed__)) alch_packet;

extern int g_rssi;
extern int g_use_crc;
extern Mode g_mode;

void alch_init_local (int use_crc, Mode mode, Retry retry, int sender);
int alch_send (unsigned char *buff, const int length);
int alch_recv (unsigned char *buff);

#endif /* __ALCHEMY_H__ */
