#ChipScope Core Generator Project File Version 3.0
#Sat May 30 19:50:12 Pacific Daylight Time 2015
SignalExport.bus<0000>.channelList=0 1 2 3 4 5 6 7 8 9
SignalExport.bus<0000>.name=TRIG0
SignalExport.bus<0000>.offset=0.0
SignalExport.bus<0000>.precision=0
SignalExport.bus<0000>.radix=Bin
SignalExport.bus<0000>.scaleFactor=1.0
SignalExport.bus<0001>.channelList= 9 8 7 6 5 4 3 2
SignalExport.bus<0001>.name=d_1
SignalExport.bus<0001>.offset=0.0
SignalExport.bus<0001>.precision=0
SignalExport.bus<0001>.radix=Hex
SignalExport.bus<0001>.scaleFactor=1.0
SignalExport.bus<0002>.channelList= 9 8 7 6 5 4 3 2
SignalExport.bus<0002>.name=byte
SignalExport.bus<0002>.offset=0.0
SignalExport.bus<0002>.precision=0
SignalExport.bus<0002>.radix=Hex
SignalExport.bus<0002>.scaleFactor=1.0
SignalExport.bus<0003>.channelList= 23 22 21 20 19 18 17 16 15 14 13 12
SignalExport.bus<0003>.name=rx_i
SignalExport.bus<0003>.offset=0.0
SignalExport.bus<0003>.precision=0
SignalExport.bus<0003>.radix=Hex
SignalExport.bus<0003>.scaleFactor=1.0
SignalExport.bus<0004>.channelList= 11 10 9 8 7 6 5 4 3 2 1 0
SignalExport.bus<0004>.name=rx_q
SignalExport.bus<0004>.offset=0.0
SignalExport.bus<0004>.precision=0
SignalExport.bus<0004>.radix=Hex
SignalExport.bus<0004>.scaleFactor=1.0
SignalExport.bus<0005>.channelList= 23 22 21 20 19 18 17 16 15 14 13 12
SignalExport.bus<0005>.name=i_out
SignalExport.bus<0005>.offset=0.0
SignalExport.bus<0005>.precision=0
SignalExport.bus<0005>.radix=Hex
SignalExport.bus<0005>.scaleFactor=1.0
SignalExport.bus<0006>.channelList= 11 10 9 8 7 6 5 4 3 2 1 0
SignalExport.bus<0006>.name=q_out
SignalExport.bus<0006>.offset=0.0
SignalExport.bus<0006>.precision=0
SignalExport.bus<0006>.radix=Hex
SignalExport.bus<0006>.scaleFactor=1.0
SignalExport.bus<0001>.channelList=10 11 12 13 14 15 16 17 18 19
SignalExport.bus<0001>.name=TRIG1
SignalExport.bus<0001>.offset=0.0
SignalExport.bus<0001>.precision=0
SignalExport.bus<0001>.radix=Bin
SignalExport.bus<0001>.scaleFactor=1.0
SignalExport.bus<0002>.channelList=20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43
SignalExport.bus<0002>.name=TRIG2
SignalExport.bus<0002>.offset=0.0
SignalExport.bus<0002>.precision=0
SignalExport.bus<0002>.radix=Bin
SignalExport.bus<0002>.scaleFactor=1.0
SignalExport.bus<0003>.channelList=44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67
SignalExport.bus<0003>.name=TRIG3
SignalExport.bus<0003>.offset=0.0
SignalExport.bus<0003>.precision=0
SignalExport.bus<0003>.radix=Bin
SignalExport.bus<0003>.scaleFactor=1.0
SignalExport.clockChannel=CLK
SignalExport.dataEqualsTrigger=true
SignalExport.triggerChannel<0000><0000>=full
SignalExport.triggerChannel<0000><0001>=empty
SignalExport.triggerChannel<0000><0002>=d_1[0]
SignalExport.triggerChannel<0000><0003>=d_1[1]
SignalExport.triggerChannel<0000><0004>=d_1[2]
SignalExport.triggerChannel<0000><0005>=d_1[3]
SignalExport.triggerChannel<0000><0006>=d_1[4]
SignalExport.triggerChannel<0000><0007>=d_1[5]
SignalExport.triggerChannel<0000><0008>=d_1[6]
SignalExport.triggerChannel<0000><0009>=d_1[7]
SignalExport.triggerChannel<0001><0000>=store_byte
SignalExport.triggerChannel<0001><0001>=clear_fifo_out
SignalExport.triggerChannel<0001><0002>=byte[0]
SignalExport.triggerChannel<0001><0003>=byte[1]
SignalExport.triggerChannel<0001><0004>=byte[2]
SignalExport.triggerChannel<0001><0005>=byte[3]
SignalExport.triggerChannel<0001><0006>=byte[4]
SignalExport.triggerChannel<0001><0007>=byte[5]
SignalExport.triggerChannel<0001><0008>=byte[6]
SignalExport.triggerChannel<0001><0009>=byte[7]
SignalExport.triggerChannel<0002><0000>=rx_q[0]
SignalExport.triggerChannel<0002><0001>=rx_q[1]
SignalExport.triggerChannel<0002><0002>=rx_q[2]
SignalExport.triggerChannel<0002><0003>=rx_q[3]
SignalExport.triggerChannel<0002><0004>=rx_q[4]
SignalExport.triggerChannel<0002><0005>=rx_q[5]
SignalExport.triggerChannel<0002><0006>=rx_q[6]
SignalExport.triggerChannel<0002><0007>=rx_q[7]
SignalExport.triggerChannel<0002><0008>=rx_q[8]
SignalExport.triggerChannel<0002><0009>=rx_q[9]
SignalExport.triggerChannel<0002><0010>=rx_q[10]
SignalExport.triggerChannel<0002><0011>=rx_q[11]
SignalExport.triggerChannel<0002><0012>=rx_i[0]
SignalExport.triggerChannel<0002><0013>=rx_i[1]
SignalExport.triggerChannel<0002><0014>=rx_i[2]
SignalExport.triggerChannel<0002><0015>=rx_i[3]
SignalExport.triggerChannel<0002><0016>=rx_i[4]
SignalExport.triggerChannel<0002><0017>=rx_i[5]
SignalExport.triggerChannel<0002><0018>=rx_i[6]
SignalExport.triggerChannel<0002><0019>=rx_i[7]
SignalExport.triggerChannel<0002><0020>=rx_i[8]
SignalExport.triggerChannel<0002><0021>=rx_i[9]
SignalExport.triggerChannel<0002><0022>=rx_i[10]
SignalExport.triggerChannel<0002><0023>=rx_i[11]
SignalExport.triggerChannel<0003><0000>=q_out[0]
SignalExport.triggerChannel<0003><0001>=q_out[1]
SignalExport.triggerChannel<0003><0002>=q_out[2]
SignalExport.triggerChannel<0003><0003>=q_out[3]
SignalExport.triggerChannel<0003><0004>=q_out[4]
SignalExport.triggerChannel<0003><0005>=q_out[5]
SignalExport.triggerChannel<0003><0006>=q_out[6]
SignalExport.triggerChannel<0003><0007>=q_out[7]
SignalExport.triggerChannel<0003><0008>=q_out[8]
SignalExport.triggerChannel<0003><0009>=q_out[9]
SignalExport.triggerChannel<0003><0010>=q_out[10]
SignalExport.triggerChannel<0003><0011>=q_out[11]
SignalExport.triggerChannel<0003><0012>=i_out[0]
SignalExport.triggerChannel<0003><0013>=i_out[1]
SignalExport.triggerChannel<0003><0014>=i_out[2]
SignalExport.triggerChannel<0003><0015>=i_out[3]
SignalExport.triggerChannel<0003><0016>=i_out[4]
SignalExport.triggerChannel<0003><0017>=i_out[5]
SignalExport.triggerChannel<0003><0018>=i_out[6]
SignalExport.triggerChannel<0003><0019>=i_out[7]
SignalExport.triggerChannel<0003><0020>=i_out[8]
SignalExport.triggerChannel<0003><0021>=i_out[9]
SignalExport.triggerChannel<0003><0022>=i_out[10]
SignalExport.triggerChannel<0003><0023>=i_out[11]
SignalExport.triggerPort<0000>.name=TRIG0
SignalExport.triggerPort<0001>.name=TRIG1
SignalExport.triggerPort<0002>.name=TRIG2
SignalExport.triggerPort<0003>.name=TRIG3
SignalExport.triggerPortCount=4
SignalExport.triggerPortIsData<0000>=true
SignalExport.triggerPortIsData<0001>=true
SignalExport.triggerPortIsData<0002>=true
SignalExport.triggerPortIsData<0003>=true
SignalExport.triggerPortWidth<0000>=10
SignalExport.triggerPortWidth<0001>=10
SignalExport.triggerPortWidth<0002>=24
SignalExport.triggerPortWidth<0003>=24
SignalExport.type=ila


