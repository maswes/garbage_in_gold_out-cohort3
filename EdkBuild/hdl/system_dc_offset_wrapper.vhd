-------------------------------------------------------------------------------
-- system_dc_offset_wrapper.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

library dc_offset_pcore_v1_00_a;
use dc_offset_pcore_v1_00_a.all;

entity system_dc_offset_wrapper is
  port (
    IPCORE_CLK : in std_logic;
    IPCORE_RESETN : in std_logic;
    AXI_Lite_ACLK : in std_logic;
    AXI_Lite_ARESETN : in std_logic;
    AXI_Lite_AWADDR : in std_logic_vector(31 downto 0);
    AXI_Lite_AWVALID : in std_logic;
    AXI_Lite_AWREADY : out std_logic;
    AXI_Lite_WDATA : in std_logic_vector(31 downto 0);
    AXI_Lite_WSTRB : in std_logic_vector(3 downto 0);
    AXI_Lite_WVALID : in std_logic;
    AXI_Lite_WREADY : out std_logic;
    AXI_Lite_BRESP : out std_logic_vector(1 downto 0);
    AXI_Lite_BVALID : out std_logic;
    AXI_Lite_BREADY : in std_logic;
    AXI_Lite_ARADDR : in std_logic_vector(31 downto 0);
    AXI_Lite_ARVALID : in std_logic;
    AXI_Lite_ARREADY : out std_logic;
    AXI_Lite_RDATA : out std_logic_vector(31 downto 0);
    AXI_Lite_RRESP : out std_logic_vector(1 downto 0);
    AXI_Lite_RVALID : out std_logic;
    AXI_Lite_RREADY : in std_logic;
    i_in : in std_logic_vector(11 downto 0);
    q_in : in std_logic_vector(11 downto 0);
    i_out : out std_logic_vector(11 downto 0);
    q_out : out std_logic_vector(11 downto 0);
    blinky : out std_logic;
    d1 : out std_logic_vector(23 downto 0);
    d2 : out std_logic_vector(23 downto 0);
    d3 : out std_logic_vector(23 downto 0);
    d4 : out std_logic_vector(23 downto 0);
    d5 : out std_logic_vector(1 downto 0);
    d6 : out std_logic_vector(29 downto 0);
    d7 : out std_logic;
    d8 : out std_logic;
    gain_out : out std_logic_vector(5 downto 0)
  );
end system_dc_offset_wrapper;

architecture STRUCTURE of system_dc_offset_wrapper is

  component dc_offset_pcore is
    port (
      IPCORE_CLK : in std_logic;
      IPCORE_RESETN : in std_logic;
      AXI_Lite_ACLK : in std_logic;
      AXI_Lite_ARESETN : in std_logic;
      AXI_Lite_AWADDR : in std_logic_vector(31 downto 0);
      AXI_Lite_AWVALID : in std_logic;
      AXI_Lite_AWREADY : out std_logic;
      AXI_Lite_WDATA : in std_logic_vector(31 downto 0);
      AXI_Lite_WSTRB : in std_logic_vector(3 downto 0);
      AXI_Lite_WVALID : in std_logic;
      AXI_Lite_WREADY : out std_logic;
      AXI_Lite_BRESP : out std_logic_vector(1 downto 0);
      AXI_Lite_BVALID : out std_logic;
      AXI_Lite_BREADY : in std_logic;
      AXI_Lite_ARADDR : in std_logic_vector(31 downto 0);
      AXI_Lite_ARVALID : in std_logic;
      AXI_Lite_ARREADY : out std_logic;
      AXI_Lite_RDATA : out std_logic_vector(31 downto 0);
      AXI_Lite_RRESP : out std_logic_vector(1 downto 0);
      AXI_Lite_RVALID : out std_logic;
      AXI_Lite_RREADY : in std_logic;
      i_in : in std_logic_vector(11 downto 0);
      q_in : in std_logic_vector(11 downto 0);
      i_out : out std_logic_vector(11 downto 0);
      q_out : out std_logic_vector(11 downto 0);
      blinky : out std_logic;
      d1 : out std_logic_vector(23 downto 0);
      d2 : out std_logic_vector(23 downto 0);
      d3 : out std_logic_vector(23 downto 0);
      d4 : out std_logic_vector(23 downto 0);
      d5 : out std_logic_vector(1 downto 0);
      d6 : out std_logic_vector(29 downto 0);
      d7 : out std_logic;
      d8 : out std_logic;
      gain_out : out std_logic_vector(5 downto 0)
    );
  end component;

begin

  dc_offset : dc_offset_pcore
    port map (
      IPCORE_CLK => IPCORE_CLK,
      IPCORE_RESETN => IPCORE_RESETN,
      AXI_Lite_ACLK => AXI_Lite_ACLK,
      AXI_Lite_ARESETN => AXI_Lite_ARESETN,
      AXI_Lite_AWADDR => AXI_Lite_AWADDR,
      AXI_Lite_AWVALID => AXI_Lite_AWVALID,
      AXI_Lite_AWREADY => AXI_Lite_AWREADY,
      AXI_Lite_WDATA => AXI_Lite_WDATA,
      AXI_Lite_WSTRB => AXI_Lite_WSTRB,
      AXI_Lite_WVALID => AXI_Lite_WVALID,
      AXI_Lite_WREADY => AXI_Lite_WREADY,
      AXI_Lite_BRESP => AXI_Lite_BRESP,
      AXI_Lite_BVALID => AXI_Lite_BVALID,
      AXI_Lite_BREADY => AXI_Lite_BREADY,
      AXI_Lite_ARADDR => AXI_Lite_ARADDR,
      AXI_Lite_ARVALID => AXI_Lite_ARVALID,
      AXI_Lite_ARREADY => AXI_Lite_ARREADY,
      AXI_Lite_RDATA => AXI_Lite_RDATA,
      AXI_Lite_RRESP => AXI_Lite_RRESP,
      AXI_Lite_RVALID => AXI_Lite_RVALID,
      AXI_Lite_RREADY => AXI_Lite_RREADY,
      i_in => i_in,
      q_in => q_in,
      i_out => i_out,
      q_out => q_out,
      blinky => blinky,
      d1 => d1,
      d2 => d2,
      d3 => d3,
      d4 => d4,
      d5 => d5,
      d6 => d6,
      d7 => d7,
      d8 => d8,
      gain_out => gain_out
    );

end architecture STRUCTURE;

