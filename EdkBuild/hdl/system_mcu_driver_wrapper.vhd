-------------------------------------------------------------------------------
-- system_mcu_driver_wrapper.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

library mcu_driver_pcore_v1_00_a;
use mcu_driver_pcore_v1_00_a.all;

entity system_mcu_driver_wrapper is
  port (
    IPCORE_CLK : in std_logic;
    IPCORE_RESETN : in std_logic;
    AXI_Lite_ACLK : in std_logic;
    AXI_Lite_ARESETN : in std_logic;
    AXI_Lite_AWADDR : in std_logic_vector(31 downto 0);
    AXI_Lite_AWVALID : in std_logic;
    AXI_Lite_AWREADY : out std_logic;
    AXI_Lite_WDATA : in std_logic_vector(31 downto 0);
    AXI_Lite_WSTRB : in std_logic_vector(3 downto 0);
    AXI_Lite_WVALID : in std_logic;
    AXI_Lite_WREADY : out std_logic;
    AXI_Lite_BRESP : out std_logic_vector(1 downto 0);
    AXI_Lite_BVALID : out std_logic;
    AXI_Lite_BREADY : in std_logic;
    AXI_Lite_ARADDR : in std_logic_vector(31 downto 0);
    AXI_Lite_ARVALID : in std_logic;
    AXI_Lite_ARREADY : out std_logic;
    AXI_Lite_RDATA : out std_logic_vector(31 downto 0);
    AXI_Lite_RRESP : out std_logic_vector(1 downto 0);
    AXI_Lite_RVALID : out std_logic;
    AXI_Lite_RREADY : in std_logic;
    init_done : in std_logic;
    mcu_reset_out : out std_logic;
    tr_sw : out std_logic;
    pa_en : out std_logic;
    tx_en : out std_logic;
    rx_en : out std_logic;
    blinky : out std_logic
  );
end system_mcu_driver_wrapper;

architecture STRUCTURE of system_mcu_driver_wrapper is

  component mcu_driver_pcore is
    port (
      IPCORE_CLK : in std_logic;
      IPCORE_RESETN : in std_logic;
      AXI_Lite_ACLK : in std_logic;
      AXI_Lite_ARESETN : in std_logic;
      AXI_Lite_AWADDR : in std_logic_vector(31 downto 0);
      AXI_Lite_AWVALID : in std_logic;
      AXI_Lite_AWREADY : out std_logic;
      AXI_Lite_WDATA : in std_logic_vector(31 downto 0);
      AXI_Lite_WSTRB : in std_logic_vector(3 downto 0);
      AXI_Lite_WVALID : in std_logic;
      AXI_Lite_WREADY : out std_logic;
      AXI_Lite_BRESP : out std_logic_vector(1 downto 0);
      AXI_Lite_BVALID : out std_logic;
      AXI_Lite_BREADY : in std_logic;
      AXI_Lite_ARADDR : in std_logic_vector(31 downto 0);
      AXI_Lite_ARVALID : in std_logic;
      AXI_Lite_ARREADY : out std_logic;
      AXI_Lite_RDATA : out std_logic_vector(31 downto 0);
      AXI_Lite_RRESP : out std_logic_vector(1 downto 0);
      AXI_Lite_RVALID : out std_logic;
      AXI_Lite_RREADY : in std_logic;
      init_done : in std_logic;
      mcu_reset_out : out std_logic;
      tr_sw : out std_logic;
      pa_en : out std_logic;
      tx_en : out std_logic;
      rx_en : out std_logic;
      blinky : out std_logic
    );
  end component;

begin

  mcu_driver : mcu_driver_pcore
    port map (
      IPCORE_CLK => IPCORE_CLK,
      IPCORE_RESETN => IPCORE_RESETN,
      AXI_Lite_ACLK => AXI_Lite_ACLK,
      AXI_Lite_ARESETN => AXI_Lite_ARESETN,
      AXI_Lite_AWADDR => AXI_Lite_AWADDR,
      AXI_Lite_AWVALID => AXI_Lite_AWVALID,
      AXI_Lite_AWREADY => AXI_Lite_AWREADY,
      AXI_Lite_WDATA => AXI_Lite_WDATA,
      AXI_Lite_WSTRB => AXI_Lite_WSTRB,
      AXI_Lite_WVALID => AXI_Lite_WVALID,
      AXI_Lite_WREADY => AXI_Lite_WREADY,
      AXI_Lite_BRESP => AXI_Lite_BRESP,
      AXI_Lite_BVALID => AXI_Lite_BVALID,
      AXI_Lite_BREADY => AXI_Lite_BREADY,
      AXI_Lite_ARADDR => AXI_Lite_ARADDR,
      AXI_Lite_ARVALID => AXI_Lite_ARVALID,
      AXI_Lite_ARREADY => AXI_Lite_ARREADY,
      AXI_Lite_RDATA => AXI_Lite_RDATA,
      AXI_Lite_RRESP => AXI_Lite_RRESP,
      AXI_Lite_RVALID => AXI_Lite_RVALID,
      AXI_Lite_RREADY => AXI_Lite_RREADY,
      init_done => init_done,
      mcu_reset_out => mcu_reset_out,
      tr_sw => tr_sw,
      pa_en => pa_en,
      tx_en => tx_en,
      rx_en => rx_en,
      blinky => blinky
    );

end architecture STRUCTURE;

