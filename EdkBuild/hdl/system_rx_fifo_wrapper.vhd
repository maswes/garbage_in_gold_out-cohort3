-------------------------------------------------------------------------------
-- system_rx_fifo_wrapper.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

library rx_fifo_pcore_v1_00_a;
use rx_fifo_pcore_v1_00_a.all;

entity system_rx_fifo_wrapper is
  port (
    IPCORE_CLK : in std_logic;
    IPCORE_RESETN : in std_logic;
    AXI_Lite_ACLK : in std_logic;
    AXI_Lite_ARESETN : in std_logic;
    AXI_Lite_AWADDR : in std_logic_vector(31 downto 0);
    AXI_Lite_AWVALID : in std_logic;
    AXI_Lite_AWREADY : out std_logic;
    AXI_Lite_WDATA : in std_logic_vector(31 downto 0);
    AXI_Lite_WSTRB : in std_logic_vector(3 downto 0);
    AXI_Lite_WVALID : in std_logic;
    AXI_Lite_WREADY : out std_logic;
    AXI_Lite_BRESP : out std_logic_vector(1 downto 0);
    AXI_Lite_BVALID : out std_logic;
    AXI_Lite_BREADY : in std_logic;
    AXI_Lite_ARADDR : in std_logic_vector(31 downto 0);
    AXI_Lite_ARVALID : in std_logic;
    AXI_Lite_ARREADY : out std_logic;
    AXI_Lite_RDATA : out std_logic_vector(31 downto 0);
    AXI_Lite_RRESP : out std_logic_vector(1 downto 0);
    AXI_Lite_RVALID : out std_logic;
    AXI_Lite_RREADY : in std_logic;
    reset_fifo : in std_logic;
    store_byte : in std_logic;
    byte_in : in std_logic_vector(7 downto 0);
    empty : out std_logic;
    full : out std_logic;
    d_1 : out std_logic_vector(7 downto 0)
  );
end system_rx_fifo_wrapper;

architecture STRUCTURE of system_rx_fifo_wrapper is

  component rx_fifo_pcore is
    port (
      IPCORE_CLK : in std_logic;
      IPCORE_RESETN : in std_logic;
      AXI_Lite_ACLK : in std_logic;
      AXI_Lite_ARESETN : in std_logic;
      AXI_Lite_AWADDR : in std_logic_vector(31 downto 0);
      AXI_Lite_AWVALID : in std_logic;
      AXI_Lite_AWREADY : out std_logic;
      AXI_Lite_WDATA : in std_logic_vector(31 downto 0);
      AXI_Lite_WSTRB : in std_logic_vector(3 downto 0);
      AXI_Lite_WVALID : in std_logic;
      AXI_Lite_WREADY : out std_logic;
      AXI_Lite_BRESP : out std_logic_vector(1 downto 0);
      AXI_Lite_BVALID : out std_logic;
      AXI_Lite_BREADY : in std_logic;
      AXI_Lite_ARADDR : in std_logic_vector(31 downto 0);
      AXI_Lite_ARVALID : in std_logic;
      AXI_Lite_ARREADY : out std_logic;
      AXI_Lite_RDATA : out std_logic_vector(31 downto 0);
      AXI_Lite_RRESP : out std_logic_vector(1 downto 0);
      AXI_Lite_RVALID : out std_logic;
      AXI_Lite_RREADY : in std_logic;
      reset_fifo : in std_logic;
      store_byte : in std_logic;
      byte_in : in std_logic_vector(7 downto 0);
      empty : out std_logic;
      full : out std_logic;
      d_1 : out std_logic_vector(7 downto 0)
    );
  end component;

begin

  rx_fifo : rx_fifo_pcore
    port map (
      IPCORE_CLK => IPCORE_CLK,
      IPCORE_RESETN => IPCORE_RESETN,
      AXI_Lite_ACLK => AXI_Lite_ACLK,
      AXI_Lite_ARESETN => AXI_Lite_ARESETN,
      AXI_Lite_AWADDR => AXI_Lite_AWADDR,
      AXI_Lite_AWVALID => AXI_Lite_AWVALID,
      AXI_Lite_AWREADY => AXI_Lite_AWREADY,
      AXI_Lite_WDATA => AXI_Lite_WDATA,
      AXI_Lite_WSTRB => AXI_Lite_WSTRB,
      AXI_Lite_WVALID => AXI_Lite_WVALID,
      AXI_Lite_WREADY => AXI_Lite_WREADY,
      AXI_Lite_BRESP => AXI_Lite_BRESP,
      AXI_Lite_BVALID => AXI_Lite_BVALID,
      AXI_Lite_BREADY => AXI_Lite_BREADY,
      AXI_Lite_ARADDR => AXI_Lite_ARADDR,
      AXI_Lite_ARVALID => AXI_Lite_ARVALID,
      AXI_Lite_ARREADY => AXI_Lite_ARREADY,
      AXI_Lite_RDATA => AXI_Lite_RDATA,
      AXI_Lite_RRESP => AXI_Lite_RRESP,
      AXI_Lite_RVALID => AXI_Lite_RVALID,
      AXI_Lite_RREADY => AXI_Lite_RREADY,
      reset_fifo => reset_fifo,
      store_byte => store_byte,
      byte_in => byte_in,
      empty => empty,
      full => full,
      d_1 => d_1
    );

end architecture STRUCTURE;

