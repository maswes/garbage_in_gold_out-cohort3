XSIZE = 25;
YSIZE = 25;
serialPort = serial('COM3','BAUD', 115200);
fopen(serialPort);
[A,m] = imread('gold.gif');
ImageSend = round(ind2rgb(A(:,:,:,1),m).*256);

for x = 1:XSIZE
    for y = 1:YSIZE
        pixel = [x y ImageSend(x,y,1) ImageSend(x,y,2) ImageSend(x,y,3)];
        fwrite(serialPort,pixel);
    end
end
fclose(serialPort);