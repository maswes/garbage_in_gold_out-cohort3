XSIZE = 25
YSIZE = 25
serialPort = serial('COM3','BAUD', 115200);
fopen(serialPort);
receiving = 1;
recvImage = zeros(XSIZE,YSIZE,3);
while receiving
    recd = 0;
    while recd < 5
        pixel(recd+1:5) = fread(serialPort,5-recd);
    end
    if(pixel(1) > 0 & pixel(1) <= XSIZE & pixel(2) > 0 & pixel(2) > 0 & pixel(2) <= YSIZE)
        recvImage(pixel(1),pixel(2),1) = pixel(3);
        recvImage(pixel(1),pixel(2),2) = pixel(4);
        recvImage(pixel(1),pixel(2),3) = pixel(5);
        if(pixel(1) == XSIZE && pixel(2) == YSIZE)
            receiving = 0;
        end
    end
end
image(recvImage);
fclose(serialPort);
