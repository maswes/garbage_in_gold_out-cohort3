XSIZE = 59;
YSIZE = 64;
%{
serialPort = serial('COM3','BAUD', 115200);
serialPort.StopBits=1;
serialPort.Terminator='';
serialPort.Parity='none';
serialPort.FlowControl='none';
fopen(serialPort);
%}
[A,m] = imread('dragon.gif');
ImageSend = round(ind2rgb(A(:,:,:,1),m).*255);
f = fopen('dragon.rgb', 'w');
for x = 1:XSIZE
    for y = 1:YSIZE
        pixel = [x y ImageSend(x,y,1) ImageSend(x,y,2) ImageSend(x,y,3)];
        fprintf(f, '  %d, %d, %d, %d, %d,\n', x, y, ImageSend(x,y,1), ImageSend(x,y,2), ImageSend(x,y,3))
    end
end
fclose(f);

%{
for x = 1:XSIZE
    for y = 1:YSIZE
        pixel = [x y ImageSend(x,y,1) ImageSend(x,y,2) ImageSend(x,y,3)]
        try
            fprintf(serialPort, pixel);
        end
        pause(.05);
    end
end

fclose(serialPort);
delete(serialPort);
%}