# README #

This is the repository for Kyle Smith and Charles Laubach's capstone project. The goal for our capstone is to improve the Toyon Chillipepper, primarily by implementing the missing FEC and equalization functions. We are also fixing issues with Toyon's code when they present themselves.

## Repository Layout ##
**Chilipepper/** - This directory contains Toyon's QPSK Modem code and Labs. Currently, most of our work has been focused in Labs/Lab_9/MATLAB because that is the code with which we are most familiar. Occasionally, we will diff the Labs/Lab_9/MATLAB and QPSK_Radio/MATLAB directories and port in our changes. Eventually, we would like to make QPSK_Radio/MATLAB our primary directory for work because it appears to have a better implementation in some areas. Unfortunately, Lab_9/MATLAB also has a better implementation in parts, so an extensive porting job may be required before we can start using QPSK_Radio/MATLAB in earnest.

**Milestones/** - Repository of the milestones we are targeting for the following class. Contains files with filename format of 'MilestonesFor<date>.txt', which is the date by which we plan to finish and present the milestones.

**Presentations/** - Copies of presentations made in class. Since we started the capstone under Professor Eldon, the second class of the capstone course was actually the fourth progress report. We did not create slides for the layman introduction to our capstone presented at the first class.

**SimulinkModel/** - A copy of the files needed to simulate the Toyon Chilipepper in Simulink. The model itself is contained in SimModel.slx, and, so long as one has the correct toolboxes and the SimulinkModel/ folder is added to the path, no other setup is required to run the model.


# Getting Started #

To get up and running you can use the provided bitstream with the Alchemy application by following these steps.

## Requirements ##

You will need to have the following software installed on your computer.

1. Xilinx ISE 14.7
1. Windows 7 (Windows 8 requires [workarounds](http://www.eevblog.com/forum/microcontrollers/guide-getting-xilinx-ise-to-work-with-windows-8-64-bit/))
1. MATLAB 2014a
1. Two Zedboards
1. Two Chilipepper radios

## Building the Demo ##

1. Open `EdkBuild/system.xmp` in Xilinx Platform Studio
1. In the left Navigator pane, click `Export Design` and `Export & Launch SDK`. This will launch Xilinx Software Development Kit
1. In the SDK, select `File --> New --> Application Project`
1. Name the project `alchemy` and select the `Hello World` Application Template
1. Remove `helloworld.c` from the `src` folder
1. Copy in all of the `.c` and `.h` files from the `Chilipepper/Alchemy` folder. This will build the application.

## Preparing the Zedboards ##

1. Connect the Chilipeppers to the Zedboards and power them on
1. Connect the UART and JTAG USB cables to the first Zedboard
1. Make sure all Zedboard switches are disabled (down)
1. In Xilinx SDK, select `Xilinx Tools --> Program FPGA`. If not already specified, use `EdkBuild/implementation/system.bit`. A blue LED on the Zedboard indicates success
1. In Xilinx SDK, right click on the `alchemy` folder and select `Run As --> Launch on Hardware (GDB)`
1. Repeat steps 2 - 5 on the second Zedboard

## Running the Demo ##

The demo will display an image sent from one Zedboard to the other. Use MATLAB to display this image.

1. Run MATLAB on the computer connected to the second, receiving Zedboard
1. Edit `Demo\ReceiveImage.m` to use the correct COM port
1. Run the `Demo\ReceiveImage.m` script
1. On the second, receiving Zedboard, enable Switch 2
1. On the first, sending Zedboard, enable Switch 1
1. The image will now be transmitted between the Zedboards and displayed in MATLAB